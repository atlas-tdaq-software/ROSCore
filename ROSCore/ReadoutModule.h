// -*- c++ -*-

/*
  ATLAS ROS Software

  Class: ReadoutModule
  Authors: ATLAS ROS group 	
*/


#ifndef READOUTMODULE_H
#define READOUTMODULE_H

#include <vector>
#include "ROSCore/IOMPlugin.h"
#include "ROSCore/DataChannel.h"
#include "DFSubSystemItem/Config.h"
#include "DFThreads/DFCountedPointer.h"

namespace ROS {
  /**
   * \brief Interface to the H/W and S/W components controlled by 
   * the IOManager.
   *
   * Abstract base class describing an H/W component (for example the 
   * ROBIN or a ROD) or a S/W component (for example the SWROBIN)
   * that is controlled by the
   * IOM application. It extends the Controllable class, that has a 
   * virtual method for each FSM state transition. If any action has 
   * to be taken to react to a specific state transition, the ReadoutModule 
   * sub class will overload the relevant method.
   * If the component is an input module, it will create and own a number 
   * of DataChannels.
   */
  class ReadoutModule : public IOMPlugin {
  public:

    virtual ~ReadoutModule() noexcept { }

    /**
     * Set/update the values of the class internal variables.
     */
    virtual void setup(DFCountedPointer<Config> configuration);


    /** 
     * Get the list of channels connected to this ReadoutModule
     */
    virtual const std::vector<DataChannel *> * channels() = 0;

    /** Reset internal statistics. */
    virtual void clearInfo() = 0;


     /** Get statistics or other data in response to a user command
         from Run Control */
     virtual int getUserCommandStatistics(std::string command,
                                          unsigned char* buffer,
                                          unsigned int maxSize,
                                          unsigned long physicalAddress);
  };

   inline void ReadoutModule::setup(DFCountedPointer<Config>){
   }

   /** Default implementation reads nothing */
   inline int  ReadoutModule::getUserCommandStatistics(std::string /*command*/,
                                                       unsigned char* /*buffer*/,
                                                       unsigned int /*maxSize*/,
                                                       unsigned long /*physicalAddress*/)
  {

     return 0;
  }
}
#endif //READOUTMODULE_H
