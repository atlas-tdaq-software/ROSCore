// $Id$
/*
  ATLAS TDAQ ROS/RCD

  Class: SingleFragmentDataChannel
  Authors: RCD 
*/

#ifndef SINGLEFRAGMENTDATACHANNEL_H
#define SINGLEFRAGMENTDATACHANNEL_H

#include "DFSubSystemItem/Config.h"
#include "ROSCore/DataChannel.h"
#include "ROSEventFragment/EventFragment.h"
#include "ROSMemoryPool/MemoryPool.h"
#include "DFThreads/DFFastMutex.h"
#include "ROSInfo/SingleFragmentDataChannelInfo.h"

namespace ROS {


  class SingleFragmentDataChannel : public DataChannel {
  public:
    
    SingleFragmentDataChannel(unsigned int id, unsigned int configId,
                              unsigned long rolPhysicalAddress,
			  DFCountedPointer<Config> configuration,
			  SingleFragmentDataChannelInfo* info = new SingleFragmentDataChannelInfo()); 
    
    virtual ~SingleFragmentDataChannel();

    virtual int getNextFragment(unsigned int* buffer, int max_size,
			        unsigned int* status,  unsigned long pciAddress = 0) = 0;

    virtual ROS::EventFragment *getFragment(int level1Id);
    virtual int requestFragment(int /*level1Id*/) {return 0;};                           //Dummy method. Will never be called
    virtual void releaseFragment(const std::vector < unsigned int > * /*level1Ids*/) {}; //Dummy method. Will never be called
    virtual void releaseFragment(int /*level1Id*/) {};                                   //Dummy method. Will never be called
    virtual void clearInfo();
    virtual ISInfo *getISInfo(void);

  private:    
    MemoryPool *m_memoryPool;        //The Memory Pool for the Hardware Fragments
    DFFastMutex *m_mutex;            //"REQUESTMUTEX" to protect access to the Memory Pool (via HWFragment)
    SingleFragmentDataChannelInfo *m_statistics;
  };
}
#endif //SINGLEFRAGMENTDATACHANNEL_H
