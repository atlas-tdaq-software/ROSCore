// -*- c++ -*-
//$Id$

/*
  ATLAS ROS Software

  Class: SequentialInputHandler
  Author: B.Gorini, M.Joos, J.Petersen, CERN

*/

#ifndef SEQUENTIALINPUTHANDLER_H
#define SEQUENTIALINPUTHANDLER_H

#include <sys/types.h>
#include <map>

#include "ROSCore/ROSHandler.h"
#include "ROSCore/TriggerInputQueue.h"

#include "DFThreads/DFFastMutex.h"

#include "ROSCore/SequentialDataChannel.h"
#include "ROSInfo/SequentialInputHandlerInfo.h"

namespace ROS { 

/**
 * Class SequentialInputHandler
 * 
 */
  class SequentialInputHandler : public ROSHandler {

  public:

    static SequentialInputHandler *Instance();
 
    virtual void setup(DFCountedPointer<Config> configuration);

      virtual void configure(const daq::rc::TransitionCmd& s);
      virtual void prepareForRun(const daq::rc::TransitionCmd& s);
      virtual void stopGathering(const daq::rc::TransitionCmd& s);
      virtual void unconfigure(const daq::rc::TransitionCmd& s);


    virtual void clearInfo();
    virtual ISInfo *getISInfo();

    virtual ~SequentialInputHandler () noexcept;

    const std::vector<SequentialDataChannel *> *channels(void);
    void registerChannel(SequentialDataChannel *channel);

    void  unregisterChannel (SequentialDataChannel *channel);
    
    bool areFragmentsThere(unsigned int L1id);
    void pushL1id(unsigned int L1id, unsigned int channel);

  protected: 

    virtual void run();
    virtual void cleanup();

  private:
    static DFFastMutex *s_mutex;                       //Needed to protect the creation of the singleton
    static SequentialInputHandler *s_uniqueInstance;   //Make sure ony one instance of SequentialInputHandler gets created
    std::vector<SequentialDataChannel *> m_dataChannels;    //All the sequential channels in the IOM: poll & interrupt
    int m_numberOfDataChannels;                        //The number of registered data channels  //MJ: = m_dataChannels.size()?
    bool m_singleFragmentMode;	                       //Do not wait for complete events but send the fragments out immediately 
    std::map <u_int, int> m_outStandingEvents;              //u_int = L1ID, int = Number of fragments received for this L1ID
                                                       //Once each channel has sent a fragment for a given L1ID the event can be processed
    std::vector <u_int> m_rod_L1id_prev;                    //L1ID of the current event (bad name)
    SequentialInputHandlerInfo m_statistics;           //For the communication with IS
    bool m_triggerQueueFlag;                           //Parameter of the config DB  
    u_int m_triggerQueueSize;                          //Parameter of the config DB

    TriggerInputQueue* m_inputToTriggerQueue;        //Input end of the Q (output end in DataDrivenTrigger) 
    bool m_threadStarted;                              //The thread will only be started if at least one data channel has registered itself.  
    static const int c_mapSizeBins = 100;              //Histogram (see below) 
    std::vector<int> m_mapSizeHist;                         //The histogram tells you how many events (in terms of L1IDs) the system was
                                                       //working on in parallel at any moment in time
     bool m_runActive;

    SequentialInputHandler(void);                      //Empty Constructor: it is private as part of the singleton pattern
  };
} 
#endif //SEQUENTIALINPUTHANDLER_H

