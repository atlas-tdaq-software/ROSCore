/* -*- c++ -*-  $Id$
  ATLAS ROS Software

  Class: TRIGGERIN
  Authors: ATLAS ROS group 	

  $Log$
  Revision 1.7  2008/02/05 08:48:34  gcrone
  Add triggerCount method

  Revision 1.6  2006/08/18 11:09:13  gcrone
  Namespace changes for Config

  Revision 1.5  2005/12/09 11:43:02  gcrone
  Removed more spurious ;s to make gcc344 happy

  Revision 1.4  2005/11/24 13:02:32  gcrone
  Remove spurious ;s

  Revision 1.3  2005/07/26 21:15:26  gorini
  Added IOMPlugin interface that replace Controllable as base class for the Plugins

  Revision 1.2  2005/04/29 09:15:41  gorini
  Removed include of obsolete SelectionCriteria class

  Revision 1.1  2005/04/28 20:57:01  gorini
  Adapted IOManager to new emon package. Moved Request implementations from here to ROSIO. Moved abstract classes from ROSIO to here to avoid cross dependencies between packages.

  Revision 1.2  2004/04/13 10:20:35  gcrone
  Make TriggerIn and DataOut inherit from Controllable.  The
  Configuration is now passed in a new method called setup.

  Revision 1.1.1.1  2004/02/05 18:31:17  akazarov
  imported from nightly 05.02.2004

  Revision 1.5  2004/01/08 16:43:45  gcrone
  Add virtual destructor

  Revision 1.4  2003/11/11 10:56:03  gcrone
  Use DFCountedPointer<Config> in place of Config*.

  Revision 1.3  2003/07/21 09:53:32  glehmann
  minor change in TriggerIn to account for new SubSystemItem

  Revision 1.2  2002/11/14 16:23:41  gcrone
  Add m_ioManager and inline initialise method to set it.

  Revision 1.1.1.1  2002/10/07 09:50:07  pyannis
  Initial version of DataFlow ROS packages tree...

  Revision 1.2  2002/09/17 21:44:46  gcrone
  Document code only - No functional changes


*/

#ifndef TRIGGERIN_H
#define TRIGGERIN_H

#include <string>

#include "DFThreads/DFThread.h"
#include "DFThreads/DFCountedPointer.h"
#include "ROSCore/IOManager.h"
#include "ROSCore/IOMPlugin.h"


namespace ROS {
   class Config;

  /** @interface
    Abstract Trigger Input class.
   */
   class TriggerIn : public DFThread, public IOMPlugin {
   public:  
      virtual ~TriggerIn() noexcept;

      virtual void setup(IOManager* iomanager,
                         DFCountedPointer<Config>) = 0;

      // placeholder for possible implementation of trigger
      // that may need to be re-enabled after an event
      // e.g. clearing a busy
      virtual void clear();

      virtual unsigned int triggerCount();
   protected:
      void initialise(IOManager* iomanager);

      IOManager* m_ioManager;
      std::string m_ioManagerName;
   };

   inline TriggerIn::~TriggerIn() noexcept {
   }

   inline void TriggerIn::clear() {
   }

   inline unsigned int TriggerIn::triggerCount() {
      return 0;
   }

   inline void TriggerIn::initialise(IOManager* iomanager) {
      m_ioManager=iomanager;
      m_ioManagerName = m_ioManager->getName();
   }

}
#endif //TRIGGERIN_H
