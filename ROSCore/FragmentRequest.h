// -*- c++ -*-
// $Id$ 
//
// $Log$
// Revision 1.2  2008/06/04 16:32:54  gcrone
// Move checkAge and s_maxAge into Request base class and add s_delay and timeToExecute so that we can delay executionof a request
//
// Revision 1.1  2008/02/04 15:19:29  gcrone
// Add new FragmentRequest/Builder classes
//
//
//

#ifndef FRAGMENTREQUEST_H
#define FRAGMENTREQUEST_H

#include "ROSCore/Request.h"



typedef unsigned int NodeID;
class DFFastMutex;
template <class type> class DFOutputQueue;

namespace ROS {
   class MemoryPool;
   class EventFragment;
   class FragmentBuilder;
   class FragmentRequest : public Request
   {
   public: 
      FragmentRequest(const unsigned int level1Id,
                      std::vector<DataChannel*>* dataChannel,
                      bool deleteChannelVector,
                      bool doMonitoring,
                      bool doRelease,
                      FragmentBuilder* builder,
                      const NodeID destination=0,
                      const unsigned int transactionId=0,
                      DFOutputQueue<unsigned int>* releaseQueue=0);
      virtual ~FragmentRequest();
      virtual int execute(void);
      virtual std::ostream& put(std::ostream& stream) const;
      virtual std::string what();
      static void configure(
                            DFFastMutex *mutex,
			    unsigned int maxAge);

   private:
      const bool m_deleteChannelVector;
      const bool m_doMonitoring;
      const bool m_doRelease;
      EventFragment* m_eventFragment;
      FragmentBuilder* m_builder;
      const unsigned int m_level1Id;
      const unsigned int m_transactionId;
      const NodeID m_destination;
      DFOutputQueue<unsigned int>* m_releaseQueue;
      static DFFastMutex* s_mutex;

      int m_ticket[Request::MAX_NUMBER_OF_FRAGMENT_MANAGERS] ;
   };

}
#endif
