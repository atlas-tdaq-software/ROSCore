// -*- c++ -*-
// $Id$ 
//
// $Log$
// Revision 1.13  2008/07/18 11:10:03  gcrone
// Specify namespace std:: where necessary
//
// Revision 1.12  2008/06/04 16:32:54  gcrone
// Move checkAge and s_maxAge into Request base class and add s_delay and timeToExecute so that we can delay executionof a request
//
// Revision 1.11  2005/11/24 13:02:32  gcrone
// Remove spurious ;s
//
// Revision 1.10  2005/04/28 20:57:00  gorini
// Adapted IOManager to new emon package. Moved Request implementations from here to ROSIO. Moved abstract classes from ROSIO to here to avoid cross dependencies between packages.
//
// Revision 1.9  2004/12/02 13:38:43  joos
// modifications and additions for EB-ROS
//
// Revision 1.8  2004/10/18 14:07:45  glehmann
// changed signature of start/stopMonitoring; introduced interface class IOManagerConfig for Configuration plugins; Configuration plugin loaded at load command in IOManager now.
//
// Revision 1.7  2004/07/15 17:33:16  gorini
// After receiving a stop command the effective request timeout is limited to communicationTimeout. This should guarantee that all request handlers will properly stop.
//
// Revision 1.6  2004/03/24 14:07:24  gorini
// Merged NewArchitecture branch.
//
// Revision 1.5.2.2  2004/03/19 10:11:53  gorini
// Made DataRequest more generic so that also RCDDataRequests can inherit from it.
// buildROSFragment method has become purely virtual and is now called buildFragment.
// Added ROSDataRequest that provides a specific implementation of buildFragment for the ROS requests.
//
// Revision 1.5.2.1  2004/03/16 23:54:05  gorini
// Adapted IOManager and Request classes to new ReadouModule/DataChannel architecture
//
// Revision 1.5  2004/01/28 16:06:44  gorini
// Cleanup of data requests. No fucntional change
//
// Revision 1.4  2004/01/28 15:39:12  gorini
// Added monitoring to TestBeamRequest. Major cleanup of Request classes.
//
// Revision 1.3  2004/01/07 17:07:10  gcrone
// Added virtual destructor
//
// Revision 1.2  2003/04/02 09:57:55  joos
// Time-out added for Data Requests
//
// Revision 1.1  2003/02/05 09:25:59  gcrone
// Rearranged Request class hierarchy.  All data requests
// L2,TestBeam... now subclassed from DataRequest which has a
// buildROSFragment method.
// Request::execute now returns bool for success/(temporary) failure. Now
// retry data requests where Request::execute returns false;
//
//

#ifndef DATAREQUEST_H
#define DATAREQUEST_H

#include "ROSCore/Request.h"
#include "ROSObjectAllocation/FastObjectPool.h"
#include "ROSObjectAllocation/DoubleEndedObjectPool.h"
#include "ROSObjectAllocation/ThreadSafeObjectPool.h"

#include "DFThreads/DFFastMutex.h"
#include "rcc_time_stamp/tstamp.h"

typedef unsigned int NodeID;

namespace ROS {
   class MemoryPool;
   class EventFragment;
   //! Request class that collects ROBFragments from DataChannel
   //! objects and builds a ROSFragment.
   class DataRequest : public Request
   {
   public: 
      virtual ~DataRequest();
      virtual int execute(void)=0;
      virtual std::ostream& put(std::ostream& stream) const=0;
      virtual std::string what()=0;
      static void configure(MemoryPool* pool,DFFastMutex *mutex,
			    unsigned int runNumber,unsigned int rosId,
			    unsigned int maxAge);
   protected:
      DataRequest(const unsigned int level1Id,
                  std::vector<DataChannel*> *dataChannel,
                  const NodeID destination=0);
      virtual bool buildFragment() = 0;
      EventFragment* m_eventFragment;
      const unsigned int m_level1Id;
      const NodeID m_destination;

      int m_ticket[Request::MAX_NUMBER_OF_FRAGMENT_MANAGERS] ;
      static MemoryPool* s_pool;
      static DFFastMutex *s_mutex;
      static unsigned int s_runNumber;
      static unsigned int s_rosId;
      static unsigned int s_sdId;
   private:
   };

   inline DataRequest::~DataRequest() {
   }
}
#endif
