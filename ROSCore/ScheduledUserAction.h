// $Id$

/*
  ATLAS TDAQ ROS/RCD

  Class: ScheduledUserAction
  Authors: B.Gorini, M.Joos, J.Petersen
*/

#ifndef SCHEDULEDUSERACTION_H
#define SCHEDULEDUSERACTION_H

#include <sys/types.h>
#include "rcc_time_stamp/tstamp.h"

namespace ROS 
{
  class ScheduledUserAction 
  {
  public:
    ScheduledUserAction(u_int deltaTimeMs);
    virtual ~ScheduledUserAction();
    virtual void reactTo(void) = 0; 
    bool checkAge(void);
    u_int getAge();
    void startTimer(void);
    u_int getLastTimeSlice(void);

  private:
    u_int m_deltaTime;		// frequency in ms at which the user code gets executed
    tstamp m_tsStart;		// start of new time slice
    u_int m_lastTimeSlice;	// last actual time slice value
  };
}
#endif //SCHEDULEDUSERACTION_H
