// -*- c++ -*-
// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone
//
//  $Log$
//  Revision 1.61  2009/02/20 12:22:02  gcrone
//  Bring back user action scheduler from ROSInterruptScheduler package
//
//  Revision 1.60  2008/11/11 17:52:20  gcrone
//  Use new ROSHandler interface
//
//  Revision 1.59  2008/06/04 16:32:54  gcrone
//  Move checkAge and s_maxAge into Request base class and add s_delay and timeToExecute so that we can delay executionof a request
//
//  Revision 1.58  2008/04/01 15:41:18  gcrone
//  Add pause and resume methods.
//
//  Revision 1.57  2008/03/10 18:46:28  gcrone
//  Check to see if we're configured in probe and don't do anything if we're not
//
//  Revision 1.56  2008/02/26 16:12:05  gcrone
//  Update to new RunController
//
//  Revision 1.55  2008/02/25 15:14:11  gcrone
//  Cleanup, remove declarations of unused classes and make sure std namespace used on string and vector
//
//  Revision 1.54  2008/02/12 09:25:10  jorgen
//  include files from InterruptScheduler instead of ROSCore
//
//  Revision 1.53  2008/02/05 08:43:38  gcrone
//  provisional support for Robin interrupt catcher
//
//  Revision 1.52  2008/01/30 13:41:54  jorgen
//  move Core functionality from ROSModules to ROSCore
//
//  Revision 1.51  2007/08/21 15:33:23  gcrone
//  Direct publishing of ISInfo objects from plugins
//
//  Revision 1.50  2007/05/11 10:11:09  gcrone
//  Added enable and disable methods (from Controllable)
//
//  Revision 1.49  2007/04/27 13:06:55  gcrone
//  Calculate ROSload using delta values since last probe
//
//  Revision 1.48  2007/02/14 15:56:52  gcrone
//  Add support for publishing directly to IS without an info handler thread.
//
//  Revision 1.47  2006/10/31 18:01:44  gcrone
//  Remove redundant forward defs of DFErrorCatcher and DFInfoCatcher and fix a comment
//
//  Revision 1.46  2006/10/26 15:31:04  jorgen
//   dynamic debugging via m_trace
//
//  Revision 1.45  2006/10/13 08:35:11  jorgen
//   interrupt catcher at configure/unconfigure; dynamic debugging via user command
//
//  Revision 1.44  2006/06/29 14:58:41  gcrone
//  Add scheduled monitoring action.
//
//  Revision 1.43  2006/03/09 12:11:20  gcrone
//  Add preliminary support for getUserCommandStatistics()
//
//  Revision 1.42  2006/01/26 14:51:52  gcrone
//  Update for new run control FSM
//
//  Revision 1.41  2005/11/24 13:02:32  gcrone
//  Remove spurious ;s
//
//  Revision 1.40  2005/07/27 17:56:06  gcrone
//  Use forward references rather than include headers in IOManager.h, include the headers in IOManager.cpp
//
//  Revision 1.39  2005/04/29 11:52:39  jorgen
//   added userActionScheduler
//
//  Revision 1.38  2005/04/29 09:15:41  gorini
//  Removed include of obsolete SelectionCriteria class
//
//  Revision 1.37  2005/04/28 20:57:01  gorini
//  Adapted IOManager to new emon package. Moved Request implementations from here to ROSIO. Moved abstract classes from ROSIO to here to avoid cross dependencies between packages.
//
//  Revision 1.36  2005/04/12 16:48:01  glehmann
//  keep also empty load() method
//
//  Revision 1.35  2005/04/12 16:38:44  glehmann
//  Overload Controllable method load(string &)
//
//  Revision 1.34  2005/04/11 12:28:54  glehmann
//  moving info publishing to IOManager
//
//  Revision 1.33  2005/01/23 21:41:12  gorini
//  General cleanup in the area of SequentialInpuHandler and InterruptCatcher
//
//  Revision 1.32  2005/01/21 20:04:58  gcrone
//  Make m_triggerIn a vector of TriggerIn* so we can have multiple
//  TriggerIns.
//  Calculate Request rate since last probe.
//
//  Revision 1.31  2005/01/20 20:21:22  gcrone
//  New Request handling:  Remove AllRequest plugin and l2Request, ebRequest &
//  releaseRequest methods.  Provide queueRequest method instead.
//
//  Revision 1.30  2005/01/20 16:26:08  gorini
//   add InterruptCatcher & SequentialInputHandler
//
//  Revision 1.29  2004/10/18 14:07:45  glehmann
//  changed signature of start/stopMonitoring; introduced interface class IOManagerConfig for Configuration plugins; Configuration plugin loaded at load command in IOManager now.
//
//  Revision 1.28  2004/07/29 08:46:40  gorini
//  Removed obsolete monitor-related variable and method from IOManager
//
//  Revision 1.27  2004/07/15 17:33:16  gorini
//  After receiving a stop command the effective request timeout is limited to communicationTimeout. This should guarantee that all request handlers will properly stop.
//
//  Revision 1.26  2004/06/02 16:08:19  glehmann
//  added startTrigger and stopTrigger in IOManager: it calls startTrigger and stopTrigger on TriggerIn and ReadoutModules
//
//  Revision 1.25  2004/03/24 14:07:24  gorini
//  Merged NewArchitecture branch.
//
//  Revision 1.24.2.1  2004/03/16 23:54:05  gorini
//  Adapted IOManager and Request classes to new ReadouModule/DataChannel architecture
//
//  Revision 1.24  2004/03/03 17:30:17  gcrone
//  Count L1 IDs in releaseRequest rather than calculate based on delete
//  grouping from triggerIn Config.
//
//  Revision 1.23  2004/02/11 11:30:01  glehmann
//  adapt to online-21
//
//  Revision 1.22  2004/02/05 17:05:07  jorgen
//   improve Level1 rate statistics
//
//  Revision 1.21  2004/02/02 08:32:12  glehmann
//  search for lib+name.so and libROS+name.so when loading dynamic libraries
//
//  Revision 1.20  2004/01/28 15:39:12  gorini
//  Added monitoring to TestBeamRequest. Major cleanup of Request classes.
//
//  Revision 1.19  2004/01/20 19:58:57  gcrone
//  Remove redundant methods getNextRequest() and requeueRequest()
//
//  Revision 1.18  2003/11/21 11:36:13  akazarov
//  back to v1r5p0
//
//  Revision 1.16  2003/11/11 11:00:45  gcrone
//  Use DFCountedPointer<Config> in place of Config*.
//
//  Revision 1.15  2003/07/21 09:51:38  glehmann
//  change of API of SubSystemItem
//
//  Revision 1.14  2003/07/10 17:19:30  gorini
//  Implemented automatic exception notification to te ErrorCatcher, through a specific BaseException::NotificationHandler.
//
//  Revision 1.13  2003/06/30 15:39:24  gorini
//  Fixed problem happening when stopping monitoring. Requests queued when monitoring was enabled would try to send monitoring data even if scheduled for run after monitoring had been stopped. This was causing crashes.
//
//  Revision 1.12  2003/06/23 14:48:29  gcrone
//  Make AllRequest a plugin
//
//  Revision 1.11  2003/04/25 16:25:58  gorini
//  Added monitoring to the ROS
//
//  Revision 1.10  2003/04/02 09:57:55  joos
//  Time-out added for Data Requests
//
//  Revision 1.9  2003/01/30 14:41:10  gcrone
//  Catch ConfigExceptions and report via DFErrorCatcher
//
//  Revision 1.8  2002/12/20 14:00:58  jorgen
//   added TestBeamRequest.h
//
//  Revision 1.7  2002/11/27 09:25:01  gcrone
//  Pass transaction ID from original DC request message through L2 request
//
//  Revision 1.6  2002/11/24 15:17:48  jorgen
//   add timing & statistics for system tests
//
//  Revision 1.5  2002/11/03 11:46:42  glehmann
//  IOManager is a friend of DFCreator
//
//  Revision 1.4  2002/11/02 14:41:59  glehmann
//  IOMAnager implements the new interface with the Local Controller.
//
//  Revision 1.3  2002/10/27 14:43:59  jorgen
//   added a trace flag
//
//  Revision 1.2  2002/10/21 15:24:13  gcrone
//  New run number and ROS ID fields.
//
//  Revision 1.1.1.1  2002/10/07 09:50:07  pyannis
//  Initial version of DataFlow ROS packages tree...
//
//  Revision 1.3  2002/09/17 21:39:52  gcrone
//  Document code only - No functional changes
//
//  Revision 1.2  2002/09/13 10:23:07  gcrone
//  Use RolMap defined in Utilities/RolMap.h instead of
//  map<int,DataChannel*>.  Add count of EB requests.
//
//  Revision 1.1.1.1  2002/08/23 13:36:54  gorini
//  First import
//
//  Revision 1.2  2002/08/01 14:15:52  gcrone
//  First 'working' version using ROS namespace and dynamically loaded libraries
//
//  Revision 1.1.1.1  2002/06/21 16:18:28  pyannis
//  Creation of the cmtros directory that will hold the new ROS software
//  managed by CMT.
//  Various packages are already installed, e.g. Core, BufferManagement, DFThreads.
//
//
// //////////////////////////////////////////////////////////////////////
#ifndef IOMANAGER_H
#define IOMANAGER_H

#include <vector>
#include <map>
#include <set>
#include <string>
#include <memory>
#include <atomic>
#include <chrono>

#include "tbb/concurrent_queue.h"

#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/RunControlCommands.h"

#include "DFSubSystemItem/SSIConfiguration.h"
#include "ROSUtilities/PluginFactory.h"

#include "ROSCore/IOManagerConfig.h"
#include "DFThreads/DFCountedPointer.h"

#include "rcc_time_stamp/tstamp.h"

#include "DFThreads/DFFastMutex.h"
#include "ROSCore/ScheduledUserAction.h"

#include "is/infodictionary.h"

#include "monsvc/PublishingController.h"

typedef unsigned int NodeID;

class ISInfo;

namespace ROS {
   class Request;
   class RequestHandler;
   class DataChannel;
   class ReadoutModule;
   class TriggerIn;
   class DataOut;
   class MemoryPool;

  /** IOManager the core intelligence of the ROS.
      The IOManager receives commands from run control via the
      daq::rc::Controllable interface. IOManager instantiates
      (and deletes) appropriate instances of TriggerIn, DataOut, RequestHandler and
      ReadoutModule classes according to the configuration given by run
      control.
      
      The IOManager provides a method by which a TriggerIn object may input
      requests and queues them for later execution
      by RequestHandlers.
  */
   class IOManager : public daq::rc::Controllable
   {
   public:
      IOManager(std::string& name);
      virtual ~IOManager(void) noexcept;

      void queueRequest(Request* request);

      void setConfigPlugin(std::string& pluginName);
      void setInteractive(bool interactive);


      // Inherited from Controllable
      virtual void configure(const daq::rc::TransitionCmd& s) override;
      virtual void connect(const daq::rc::TransitionCmd& s) override;
      virtual void prepareForRun(const daq::rc::TransitionCmd& s) override;
      virtual void stopROIB(const daq::rc::TransitionCmd&) override;
      virtual void stopDC(const daq::rc::TransitionCmd&) override;
      virtual void stopHLT(const daq::rc::TransitionCmd&) override;
      virtual void stopRecording(const daq::rc::TransitionCmd&) override;
      virtual void stopGathering(const daq::rc::TransitionCmd&) override;
      virtual void stopArchiving(const daq::rc::TransitionCmd&) override;

      virtual void subTransition(const daq::rc::SubTransitionCmd&) override;

      virtual void publish() override;
      virtual void publishFullStats() override;
      virtual void disconnect(const daq::rc::TransitionCmd& s) override;
      virtual void unconfigure(const daq::rc::TransitionCmd& s) override;

      virtual void user(const daq::rc::UserCmd& command) override;

      virtual void disable(const std::vector<std::string>&) override;
      virtual void enable(const std::vector<std::string>&) override;

      virtual void resynch(const daq::rc::ResynchCmd& cmd) override;

      virtual void onExit(daq::rc::FSM_STATE state) noexcept override;

      std::vector<ReadoutModule*>* readoutModules();
      std::string getName();
   private:
      void coreMonitor();
      void publishSpecificInfo(std::string setName,
                               ISInfo* info,
                               int index=-1);
      void monitoringAction(const std::string& name);
      void preconfigure(); 
     class ScheduledMonitoringAction : public ScheduledUserAction {
      public:
         ScheduledMonitoringAction(IOManager* iom, std::string name, unsigned int interval);
         void reactTo() override;
      private:
         IOManager* m_iom;
         std::string m_name;
      };

      std::vector<ScheduledMonitoringAction*> m_scheduledMonitoringActions;
      // Factories for creating new interfaces
      PluginFactory<TriggerIn>       m_triggerInFactory;
      PluginFactory<DataOut>         m_dataOutFactory;
      PluginFactory<ReadoutModule>   m_readoutModuleFactory;
      PluginFactory<IOManagerConfig> m_configFactory;

      std::string m_name;
      // The Queue of pending requests
      tbb::concurrent_bounded_queue<Request*>* m_requestQueue;

      /// List of instantiated Request Handlers
      std::vector<RequestHandler*> m_requestHandlers;

      /// List of instantiated Readout Modules
      std::vector<ReadoutModule*> m_readoutModules;

      std::vector<DataOut*>  m_dataOut;

      std::vector<TriggerIn*> m_triggerIn;

 
      // Configuration
      std::string m_configPluginName;
      IOManagerConfig * m_config;
      DFCountedPointer<Config> m_coreParameters;
 
      /** The current run number from IS */
      unsigned int m_runNumber;
 
      /** Identifier of this ROS element.  This is used as the
          sourceId field in ROSFragments that we send to HLT */
      unsigned int m_rosId;

      // Memory pool for buffers
      MemoryPool* m_pool;

      // Memory pool for userCommand statistics buffers
      MemoryPool* m_userCommandPool;
      unsigned int m_userCommandPageSize;
      unsigned int m_userCommandDestination;

      // statistics
      std::atomic<int> m_requestsQueued;
      int m_requestsDequeued;
      int m_requestsRequeued;
      int m_numberOfRequestsLast;

      int m_lastHandled;
      int m_lastPopEmpty;
      int m_lastPushEmpty;

      // time stamps
      tstamp m_tsStart;
      tstamp m_tsStop;
      tstamp m_tsStopLast;

      /// Mutexes to be passed to the different classes for synchronisation
      DFFastMutex * m_requestMutex ;

      // trace flag
      bool m_trace;

      // time outs
      unsigned int m_requestTimeOut;
      int m_communicationTimeout;
      unsigned int m_releaseDelay;

      bool m_interactiveMode;
      std::string m_isInfoName;
      IPCPartition m_ipcPartition;
      std::set<std::string> m_isItems;

      bool m_configured;

      int m_maxBusyCounts;
      int m_busyIndex;
      int m_busyPerSecond;
      int* m_busyCount;
      int* m_queueCount;
      float* m_busyInterval;
      std::chrono::time_point<std::chrono::system_clock> m_busyTimeLast;
      std::chrono::time_point<std::chrono::system_clock> m_lastPublish;
      bool m_runActive;
      bool m_preConfigured;
      std::vector<std::string> m_moduleIds;
      std::vector<DFCountedPointer<Config> > m_triggerInConfig;

      monsvc::PublishingController* m_publishingController;
  };

}
#endif
