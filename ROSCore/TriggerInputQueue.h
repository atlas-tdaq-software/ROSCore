// -*- c++ -*-
// 
//  Encapsulate tbb concurrent queue in a singleton to be accessed by both
// ReadoutModules and DataDrivenTrigger
//

#ifndef TRIGGERINPUTQUEUE_H
#define TRIGGERINPUTQUEUE_H

#include <mutex>
#include <iostream>
#include <utility>                        // for move

#include "tbb/concurrent_queue.h"

namespace ROS {
   class TriggerInputQueue {

   public:
      static TriggerInputQueue* instance();
      void destroy();

      unsigned int pop();
      void push(unsigned int);
      void reset();
      void abort();
      void setSize(unsigned int);
   private:
      TriggerInputQueue();
      static TriggerInputQueue* s_instance;
      static std::mutex s_mutex;
      static unsigned int s_numInstance;
      tbb::concurrent_bounded_queue<unsigned int> m_queue;
   };

   inline unsigned int TriggerInputQueue::pop() {
      unsigned int value;
      m_queue.pop(value);
      return value;
   }
   inline void TriggerInputQueue::push(unsigned int value) {
      try {
         m_queue.push(value);
      }
      catch (tbb::user_abort& abortException) {
         std::cout << "TriggerInputQueue push() aborting...\n";
         return;
      }
   }
}

#endif
