// -*- c++ -*-

/*
  ATLAS ROS Software

  Class: IOMPlugin
  Authors: ATLAS ROS group 	
*/


#ifndef IOMPLUGIN_H
#define IOMPLUGIN_H

#include <string>
#include "DFSubSystemItem/Config.h"
#include "DFThreads/DFCountedPointer.h"

#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/RunControlCommands.h"

#include "dal/Partition.h"

class ISInfo;

namespace ROS {
   /** Generic base class for all objects IOM run-time plugins 
       BEWARE: the configuration plugin is a special case
   */
   class IOMPlugin : public daq::rc::Controllable {
   public:
      virtual ~IOMPlugin() noexcept {};


      void setConfiguration(Configuration* confDB,
                            const daq::core::Partition* partition,
                            std::string uid);

      /** 
       * Set the Run Parameters
       */
      virtual void setRunConfiguration(DFCountedPointer<Config>
                                       runConfiguration);

      /**
       * Get values of statistical counters
       */
      virtual DFCountedPointer<Config> getInfo();

      /**
       * Get access to statistics stored in ISInfo class
       */
      virtual ISInfo* getISInfo();
   protected:
      IOMPlugin() ;
      DFCountedPointer<Config> m_runParams;


      Configuration* m_configurationDB;
      const daq::core::Partition* m_partition;
      std::string m_uid;
      //      ISInfo* m_isInfo;
   };

   inline IOMPlugin::IOMPlugin() :
      m_configurationDB(0), m_partition(0) {
   //      m_isInfo=0;
   }

   inline ISInfo* IOMPlugin::getISInfo() {
      //      return m_isInfo;
      return 0;
   }
   inline DFCountedPointer<Config> IOMPlugin::getInfo() {
      return Config::New();
   }

   inline void IOMPlugin::setConfiguration(Configuration* confDB,
                                           const daq::core::Partition* partition,
                                           std::string uid){
      m_configurationDB=confDB;
      m_partition=partition;
      m_uid=uid;
   }

   inline void IOMPlugin::setRunConfiguration(DFCountedPointer<Config> runParams)
   {
      m_runParams=runParams;
   }
}
#endif //IOMPLUGIN_H
