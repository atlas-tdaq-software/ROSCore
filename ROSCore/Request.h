// -*- c++ -*-
// $Id$
// ///////////////////////////////////////////////////////////////////////////
//
// $Log$
// Revision 1.13  2008/06/04 16:32:54  gcrone
// Move checkAge and s_maxAge into Request base class and add s_delay and timeToExecute so that we can delay executionof a request
//
// Revision 1.12  2007/05/02 13:47:58  gcrone
// Increase max number of channels to 1500 for HLT preloaded data files.
//
// Revision 1.11  2005/11/29 16:38:21  gcrone
// Removed another spurious ; to make gcc344 happy
//
// Revision 1.10  2005/11/24 13:02:32  gcrone
// Remove spurious ;s
//
// Revision 1.9  2004/03/24 14:07:24  gorini
// Merged NewArchitecture branch.
//
// Revision 1.8.2.1  2004/03/16 23:54:05  gorini
// Adapted IOManager and Request classes to new ReadouModule/DataChannel architecture
//
// Revision 1.8  2004/01/28 16:06:44  gorini
// Cleanup of data requests. No fucntional change
//
// Revision 1.7  2004/01/28 15:39:12  gorini
// Added monitoring to TestBeamRequest. Major cleanup of Request classes.
//
// Revision 1.6  2003/04/02 09:57:56  joos
// Time-out added for Data Requests
//
// Revision 1.5  2003/03/11 16:36:00  pyannis
// Changes to compile with gcc 3.2 too...
//
// Revision 1.4  2003/02/05 09:25:59  gcrone
// Rearranged Request class hierarchy.  All data requests
// L2,TestBeam... now subclassed from DataRequest which has a
// buildROSFragment method.
// Request::execute now returns bool for success/(temporary) failure. Now
// retry data requests where Request::execute returns false;
//
// Revision 1.3  2002/11/08 15:30:58  gcrone
// New error/exception handling
//
// Revision 1.2  2002/11/06 10:23:30  gorini
// Increased max number of fragment managers to 64.
//
// Revision 1.1.1.1  2002/10/07 09:50:07  pyannis
// Initial version of DataFlow ROS packages tree...
//
// Revision 1.2  2002/09/17 21:39:52  gcrone
// Document code only - No functional changes
//
//
// ///////////////////////////////////////////////////////////////////////////
#ifndef REQUEST_H
#define REQUEST_H

#include <vector>
#include <iostream>
#include <string>
#include <chrono>


class DFFastMutex;

namespace ROS {
   class DataChannel;
   //! Abstract Request class.
   class Request
   {
   public:
      enum
      {
        REQUEST_OK = 0,
        REQUEST_NOTFOUND,
        REQUEST_TIMEOUT
      };
      virtual ~Request() { };
      /** The action to perform to fulfill the request. */
      virtual int execute(void)=0;
      virtual std::ostream& put(std::ostream& stream)const=0;
      virtual std::string what()=0;
      bool checkAge(unsigned int ageLimit);
      bool checkAge();
      static void setDelay(unsigned int delay);
      static void setMaxAge(unsigned int maxAge);
   protected:
      std::vector<DataChannel*> *m_dataChannels;
      Request(std::vector<DataChannel*> *dataChannels);
      bool timeToExecute();

      enum {MAX_NUMBER_OF_FRAGMENT_MANAGERS=2000};

      std::chrono::time_point<std::chrono::high_resolution_clock> m_birthTime;
      static unsigned int s_delay;
      static unsigned int s_maxAge; // in us
   };
   
}
std::ostream& operator <<(std::ostream& stream, const ROS::Request& request);
#endif

