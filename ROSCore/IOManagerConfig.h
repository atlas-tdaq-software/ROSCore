/* -*- c++ -*-
Configuration plugin interface
     Authors: B. Gorini, G. Lehmann
     Date: %Date%
*/

#ifndef IOMANAGERCONFIG_H
#define IOMANAGERCONFIG_H

#include <string>


class Configuration;
namespace daq {
   namespace core {
      class Partition;
   }
}

namespace ROS{
   class SSIConfiguration;
   class IOManagerConfig {
   public:
      virtual ~IOManagerConfig() {};
      virtual SSIConfiguration * update() = 0;
      virtual SSIConfiguration * updateRun() = 0;

      virtual Configuration* getConfiguration() {return 0;};
      virtual const daq::core::Partition* getPartition() {return 0;};

      static void globalOptions(std::string options);
      static const std::string& globalOptions();
   protected:
      static std::string s_globalOptions;
   };
}
#endif //IOMANAGERCONFIG_H

