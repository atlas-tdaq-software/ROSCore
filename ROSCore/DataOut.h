// -*- c++ -*-
/*
  ATLAS ROS Software

  Class: DATAOUT
  Authors: ATLAS ROS group 	

  $Log$
  Revision 1.10  2008/03/06 13:37:26  gcrone
  Handle SelectionCriteria

  Revision 1.9  2008/02/25 15:11:12  gcrone
  New static methods allow for Monitoring and Debug DataOuts

  Revision 1.8  2008/02/05 08:52:00  gcrone
  Add status argument to sendData method

  Revision 1.7  2006/04/28 13:43:32  gcrone
  New flag/mutex handling for sampler

  Revision 1.6  2006/04/27 16:57:37  gcrone
  New protected method clearSampledDataOut

  Revision 1.5  2006/04/25 18:01:48  gcrone
  Add static member mutex for locking of monitoring data out

  Revision 1.4  2006/03/08 16:20:32  gcrone
  sendData() now takes a Buffer* rather than a Fragment*

  Revision 1.3  2005/11/24 13:02:32  gcrone
  Remove spurious ;s

  Revision 1.2  2005/07/26 21:15:26  gorini
  Added IOMPlugin interface that replace Controllable as base class for the Plugins

  Revision 1.1  2005/04/28 20:57:00  gorini
  Adapted IOManager to new emon package. Moved Request implementations from here to ROSIO. Moved abstract classes from ROSIO to here to avoid cross dependencies between packages.

  Revision 1.2  2004/04/13 10:20:35  gcrone
  Make TriggerIn and DataOut inherit from Controllable.  The
  Configuration is now passed in a new method called setup.

  Revision 1.1.1.1  2004/02/05 18:31:17  akazarov
  imported from nightly 05.02.2004

  Revision 1.7  2004/01/20 19:50:04  gcrone
  Added start() and stop() with default empty implementation

  Revision 1.6  2004/01/13 15:44:31  gcrone
  Added virtual destructor.

  Revision 1.5  2003/11/11 10:56:03  gcrone
  Use DFCountedPointer<Config> in place of Config*.

  Revision 1.4  2003/02/19 18:38:47  gcrone
  Use EventFragment* instead of ROSFragment*

  Revision 1.3  2002/11/27 09:03:52  gcrone
  Initial check in of IOException class

  Revision 1.2  2002/11/03 18:24:12  gcrone
  sendData() now takes ROSFragment* instead of EventFragment*

  Revision 1.1.1.1  2002/10/07 09:50:07  pyannis
  Initial version of DataFlow ROS packages tree...

  Revision 1.3  2002/09/17 21:44:46  gcrone
  Document code only - No functional changes
*/

#ifndef DATAOUT_H
#define DATAOUT_H


#include "DFThreads/DFCountedPointer.h"

#include "ROSCore/IOMPlugin.h"

typedef unsigned int NodeID;
namespace emon {
   class SelectionCriteria;
}
/** @interface */
namespace ROS {
   class Config;
   class Buffer;
   //! Abstract Data Out class
   class DataOut : public IOMPlugin
   {
   public:  
      enum Type { MAIN , SAMPLED , DEBUGGING, OTHER };
      DataOut(Type type=MAIN);
      virtual ~DataOut() noexcept;

      //! Send Buffer (usually containing an EventFragment) to destination
      virtual void sendData(const Buffer*, const NodeID,
                            const unsigned int transactionId=1,
                            const unsigned int status=0) = 0;

      /**
       * Set/update the values of the class internal variables.
       */
      virtual void setup(DFCountedPointer<Config> configuration) = 0;

      //User methods to send data wihout knowing 
      //the specific subclass instances
      //Used in the Requests	
      static DataOut * main();
      static DataOut* debug();
      static DataOut* sampled();

      static void selectionCriteria(emon::SelectionCriteria* criteria);
      static emon::SelectionCriteria* selectionCriteria();

      void setScalingFactor(int scalingFactor);

   protected:
      int m_scalingFactor;
      Type m_type;


   private:
      static DataOut* s_mainDataOut;
      static DataOut* s_sampledDataOut;
      static DataOut* s_debugDataOut;

      static emon::SelectionCriteria* s_criteria;
   };

   inline DataOut* DataOut::main() {
     return s_mainDataOut;
   }
   inline DataOut* DataOut::sampled() {
     return s_sampledDataOut;
   }
   inline DataOut* DataOut::debug() {
     return s_debugDataOut;
   }
   inline void DataOut::setScalingFactor(int scalingFactor) {
     m_scalingFactor=scalingFactor;
   }

   inline void DataOut::selectionCriteria(emon::SelectionCriteria* criteria) {
      s_criteria=criteria;
   }
   inline emon::SelectionCriteria* DataOut::selectionCriteria() {
      return s_criteria;
   }

}
#endif //DATAOUT_H
