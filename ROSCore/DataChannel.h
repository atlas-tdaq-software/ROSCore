// -*- c++ -*-
// $Id$

/*
  ATLAS ROS Software

  Class: DataChannel
  Authors: ATLAS ROS group 	
*/


#ifndef DATACHANNEL_H
#define DATACHANNEL_H

#include <vector>
#include <map>
#include <sstream>
#include "DFThreads/DFCountedPointer.h"
#include "ROSEventFragment/EventFragment.h"
#include "DFSubSystemItem/Config.h"

class ISInfo;

namespace ROS {
  /**
   * \brief Interface with the individual input data channels.
   *
   * Provide an API for requesting, retrieving, and deleting data fragments 
   * from an individual channel of an input module, e.g. one of the
   * ROLs connected to a ROBIN or one of the FEB conected to a ROD emulator.
   */
  class DataChannel {
  public:
    DataChannel(unsigned int id, unsigned int configId, unsigned long physicalAddress) ;
    virtual ~DataChannel() ;

    /**
     * Get an event fragment
     * @param ticket the ticket returned by the preceding requestFragment call
     */
    virtual ROS::EventFragment * getFragment(int ticket) = 0;

    /**
     * Release all the event fragments listed in the vector.
     * @param level1Ids Pointer to a vector of 32bit level 1 event identifiers of fragments to be released.
     */
    virtual void releaseFragment(const std::vector < unsigned int > * level1Ids) = 0;

    /**
     * Request an individual event Fragment by Level1 Id.
     * @param level1Id 32bit Level 1 event identifier of fragment to release.
     */
    virtual void releaseFragment(int level1Id) = 0;

    /**
     * Request a Fragment.  It returns a ticket that has to be
     * passed to the getFragment method to obtain the requested fragment.
     * @param level1Id 32bit Level 1 event identifier of fragment to be retrieved.
     */
    virtual int requestFragment(int level1Id) = 0;

    /**
     * Tell the Robin to run the garbage collection algorithm on the respective ROL
     * For non-Robin data channels do nothing
     * @param oldestId 32bit Level 1 event identifier of the oldest valid fragment.
     */
    virtual int collectGarbage(unsigned int oldestId);
    
    /** 
     * Returns the logical id of the data channel (e.g. the ROB Id)
     */
    unsigned int id() ;

    /** 
     * Returns the Configuration id of the data channel (e.g. the sequential Id)
     */
    unsigned int configId() ;

    /** 
     * Returns the physical address of the data channel if any 
     * (e.g. the SLINK occurrence number)
     */
    unsigned long physicalAddress() ;

    /** Reset internal statistics */
    virtual void clearInfo() = 0;

    /** Get performance statistics */
    virtual DFCountedPointer < Config > getInfo();

    virtual void probe();

    /**
     * Get access to statistics stored in ISInfo class
     */
    virtual ISInfo* getISInfo();

    /** 
     * Static method to get the total number of Channels defined
     * in the system.
     */
    static int numberOfChannels();

    /** 
     * Static method to get the Channel with a certain id out of all channels defined
     * in the system.
     */
    static DataChannel * channel(unsigned int id);

    /** 
     * Static method that returns the complete list of channels defined in the system.
     */
    static std::vector<DataChannel * > * channels();

     /**
      * Inform all data channels of the oldest valid L1 id so that it can make up its own mind
      * about performing garbage collection.
      * @param oldestId 32bit Level 1 event identifier of the oldest valid fragment.
      */
     static void oldestValid(unsigned int oldestId);


  private:
    //list of channels 
    static std::vector < DataChannel * > s_dataChannels;
    
    //global map of all data channels vs IDs
    static std::map < unsigned int, DataChannel * > s_dataChannelsMap ;

    //Logical ID
    unsigned int m_id ;

    //configuration ID: sequential numbering 0,1,2 .. as defined when creating Channels in the Module
    unsigned int m_configId;

    //Physical address (e.g. S-LINK occurrence)
    unsigned long m_physicalAddress ;

  protected:
     static unsigned int s_oldestValidId;
  };
}
#endif //DATACHANNEL_H
