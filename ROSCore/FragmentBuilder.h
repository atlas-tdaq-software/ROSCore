// -*- c++ -*-
// $Id$ 
//
// $Log$
// Revision 1.2  2008/04/03 11:18:55  gcrone
// Add closeFragment method
//
// Revision 1.1  2008/02/04 15:19:29  gcrone
// Add new FragmentRequest/Builder classes
//
//
#ifndef FRAGMENTBUILDER_H
#define FRAGMENTBUILDER_H


typedef unsigned int NodeID;

namespace ROS {
   class EventFragment;
   class MemoryPool;
   class FragmentBuilder
   {
   public: 
      virtual ~FragmentBuilder();
      virtual EventFragment* createFragment(unsigned int level1Id, unsigned int nchannels)=0;
      virtual void appendFragment(EventFragment* parent, EventFragment* child)=0;
      virtual void closeFragment(EventFragment* fragment);
      static void configure(MemoryPool* pool,
			    unsigned int runNumber,
                            unsigned int rosId,
                            unsigned int subdetId);
   protected:
      static MemoryPool* s_pool;
      static unsigned int s_runNumber;
      static unsigned int s_rosId;
      static unsigned int s_sdId;
   };

   inline FragmentBuilder::~FragmentBuilder() {
   }
}
#endif
