// -*- c++ -*- $Id$
/*
  ATLAS ROS Software

  Class: REQUESTHANDLER
  Authors: ATLAS ROS group 	

  $Log$
  Revision 1.16  2007/04/19 14:14:06  gcrone
  Restructure run method with try/catch. New private deleteRequest() method.

  Revision 1.15  2007/01/12 14:01:00  gcrone
  Revert to previous version without thread cancellation blocking and flush() method

  Revision 1.13  2006/08/18 11:09:13  gcrone
  Namespace changes for Config

  Revision 1.12  2005/11/24 13:02:32  gcrone
  Remove spurious ;s

  Revision 1.11  2005/07/26 21:15:26  gorini
  Added IOMPlugin interface that replace Controllable as base class for the Plugins

  Revision 1.10  2004/02/20 14:31:41  gcrone
  Removed redundant RequestHandler:: from clearInfo() to avoid compiler
  warning.

  Revision 1.9  2004/02/04 12:24:24  gcrone
  Added clearInfo() method

  Revision 1.8  2004/01/07 17:07:10  gcrone
  Added virtual destructor

  Revision 1.7  2003/12/03 15:08:49  gcrone
  Config constructors now protected, use static method New to enforce use of DFCountedPointer

  Revision 1.6  2003/07/10 17:19:30  gorini
  Implemented automatic exception notification to te ErrorCatcher, through a specific BaseException::NotificationHandler.

  Revision 1.5  2003/04/02 09:57:56  joos
  Time-out added for Data Requests

  Revision 1.4  2002/11/14 16:26:47  gcrone
  Don't need pointer to IOManager anymore.

  Revision 1.3  2002/11/08 15:30:58  gcrone
  New error/exception handling

  Revision 1.2  2002/10/21 15:19:06  gcrone
  New interface with IOManager.  We get a queue and pop requests ourselves

  Revision 1.1.1.1  2002/10/07 09:50:07  pyannis
  Initial version of DataFlow ROS packages tree...

  Revision 1.2  2002/09/17 21:39:52  gcrone
  Document code only - No functional changes


*/

#ifndef REQUESTHANDLER_H
#define REQUESTHANDLER_H


#include <thread>
#include <mutex>

#include "ROSCore/Request.h"

#include "tbb/concurrent_queue.h"

/**
 * @stereotype thread */

namespace ROS {
   class Config;
   class RequestHandlerStatistics {
   public:
      int m_handlerId;
      int m_requestsHandled;
      int m_requestsTimedOut;
      int m_requestsFailed;
   };

   //! Independant thread to handle Requests
   class RequestHandler
   {
   private:     
      //! Total number of RequestHandlers instantiated (not necessarily
      //! still existing)
      static int s_numberOfHandlers;
      //! Handler ID to be assigned to next instantiated handler
      static int s_nextId;

      RequestHandlerStatistics m_statistics;

      //! The synchronisation mutex
      std::mutex m_mutex;

      //! The queue we take requests from (may or maynot be shared with other
      // RequestHandlers.
      tbb::concurrent_bounded_queue<Request*>* m_requestQueue;

      //! The Request we're currently working on
      Request* m_request;


      bool m_active;
      std::thread* m_thread;

      void deleteRequest();
      void run();
   public:
      /** The standard constructor.
          @param requestQueue The queue from which the handler
          will get its requests */
      RequestHandler(tbb::concurrent_bounded_queue<Request*>* requestQueue);

      ~RequestHandler();

      //! Get statistics of requests handled by this RequestHandler
      RequestHandlerStatistics* getStatistics();

      //! Clear statistics kept by this RequestHandler
      void clearStatistics();

      bool busy();

      void startExecution();
      void stop();
      void join();

   };
}
#endif //REQUESTHANDLER_H
