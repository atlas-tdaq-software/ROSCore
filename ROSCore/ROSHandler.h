// -*- c++ -*-
// $Id$ 
//
// Generic handler interface for the ReadoutApplication. A static list
// of handlers is maintained by the constuctor/destructor and
// iterators are provided to allow state transistion methods to be
// called on all registered handlers.
//
//  $Log$
//  Revision 1.1  2008/10/31 16:34:30  gcrone
//  New generic handler interface
//
//
#ifndef ROSHANDLER_H
#define ROSHANDLER_H

#include <list>
#include <string>

#include "ROSCore/IOMPlugin.h"
#include "DFThreads/DFThread.h"

namespace ROS {

   class ROSHandler : public IOMPlugin, public DFThread {
   public:
      typedef std::list<ROSHandler*>::iterator iterator;
      std::string name();
      static iterator begin();
      static iterator end();
      virtual void clearInfo();
   protected:
      ROSHandler(std::string name); // Constructor only available to derived classes 
      virtual ~ROSHandler() noexcept;

      std::string m_name;
   private:
      static std::list<ROSHandler*> s_handlers;
   };
}
#endif
