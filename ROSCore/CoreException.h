// -*- c++ -*-
/*
  Atlas Ros Software

  Class: COREEXCEPTION
  Authors: ATLAS ROS group 	
*/

#ifndef COREEXCEPTION_H
#define COREEXCEPTION_H

#include "DFExceptions/ROSException.h"

namespace ROS {
  class CoreException : public ROSException {    
  public:
    enum ErrorCode { UNCLASSIFIED, 
		     UNDEFINED_CONFIGURATION,
		     UNDEFINED_MONITORING_DESTINATION,
                     REQUESTED_UNKNOWN_ROL,
                     WRONG_NUMBER_OF_FM_CONFIGS,
                     THREAD_WAIT_TIMEOUT,
                     ILLEGAL_POOL_TYPE,
                     BAD_CONFIGURATION,
                     NO_POOL,
                     TIMEOUT,
                     STALE_REQUEST,
		     DUPLICATEID,
		     UNDEFINEDID,
                     INFOZERO,
		     PCMEMCHAN_LOST,
		     NODATA,
		     ALL_INVALIDPAGE,
		     ROD_MARKER,
		     FULL_MARKER,
		     ALL_ILLSIZE2,
		     SEQ_UNREGISTERNOTFOUND,
		     SGLFR_ILLPOOLTYPE,
                     POLL_TSCLOCK
    } ; 

    CoreException(ErrorCode error) ;
    CoreException(ErrorCode error, std::string description) ;
    CoreException(ErrorCode error, const ers::Context& context) ;
    CoreException(ErrorCode error, std::string description, const ers::Context& context) ;

     CoreException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);
    virtual ~CoreException() throw () {};

  protected:
    virtual ers::Issue * clone() const { return new CoreException( *this ); }
    static const char * get_uid() {return "ROS::CoreException";}
    virtual const char* get_class_name() const {return get_uid();}
    virtual std::string getErrorString(unsigned int errorId) const;
  };
  
  inline CoreException::CoreException(CoreException::ErrorCode error) 
     : ROSException("ROSCore", error, getErrorString(error)) { }
   
  inline CoreException::CoreException(CoreException::ErrorCode error, std::string description) 
     : ROSException("ROSCore", error, getErrorString(error),description) { }

  inline CoreException::CoreException(CoreException::ErrorCode error, const ers::Context& context)
     : ROSException("ROSCore", error, getErrorString(error), context) { }
   
  inline CoreException::CoreException(CoreException::ErrorCode error, std::string description, const ers::Context& context) 
     : ROSException("ROSCore", error, getErrorString(error), description, context) { }

  inline CoreException::CoreException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context) 
     : ROSException(cause, "ROSCore", error, getErrorString(error), description, context) { }

  inline std::string CoreException::getErrorString(unsigned int errorId) const {
	  std::string rc;    
    switch (errorId) {  
    case UNCLASSIFIED:
      rc = "Unclassified error" ;
      break;
    case UNDEFINED_CONFIGURATION:
      rc = "No Configuration provided";
      break;
    case UNDEFINED_MONITORING_DESTINATION:
      rc = "Trying to enable monitoring without having defined a destination (DataOut)" ;
      break;
    case REQUESTED_UNKNOWN_ROL:
      rc = "Requested unknown ROL: don't have DataChannel for it" ;
      break;
    case WRONG_NUMBER_OF_FM_CONFIGS:
      rc = "Wrong number of dataChannel Config objects: do not match the configured number of dataChannels" ;
      break;
    case THREAD_WAIT_TIMEOUT:
      rc = "Timeout from thread" ;
      break;
    case ILLEGAL_POOL_TYPE:
       rc="Illegal memory pool type";
       break;
    case BAD_CONFIGURATION:
       rc="Error during configuration";
       break;
    case NO_POOL:
       rc="No memory pool defined";
       break;
    case TIMEOUT:
       rc="Timeout";
       break;
    case STALE_REQUEST:
       rc="RequestHandler already has a Request at start";
       break;
    case DUPLICATEID:
      rc = "The IOM configuration contains two data channels with the same ID";
      break;
    case UNDEFINEDID:
      rc = "Non existing data channel ID specified";
      break;
    case INFOZERO:
      rc = "Pointer to IS Info object equal to zero";
      break;
    case PCMEMCHAN_LOST:
      rc = "An event fragment has been lost";
      break;
    case NODATA:
      rc = "Event fragment does not (yet) exist";
      break;
    case ALL_INVALIDPAGE:
      rc = "MemoryPool returned an already locked page or a page with wrong size";
      break;
    case ROD_MARKER:
      rc = "ROD header does not start with 0xee1234ee";
      break;
    case FULL_MARKER:
      rc = "Full header does not start with 0xaa1234aa";
      break;
    case ALL_ILLSIZE2:
      rc = "The size of the ROD fragment does not match the information in the ROD trailer";
      break;
    case SEQ_UNREGISTERNOTFOUND:
      rc = "Channel not found when unregister";
      break;
    case SGLFR_ILLPOOLTYPE:
      rc = "Illegal memory pool type";
      break;
    case POLL_TSCLOCK:
       rc = "Error on ts_clock"; 													break;
       break;
    default:
      rc = "" ;
      break;
    }
    return rc;
  }
}
#endif //COREEXCEPTION_H
