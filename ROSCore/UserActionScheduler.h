//$Id$

/*
  ATLAS ROS/RCD Software

  Class: UserActionScheduler
  Author: B.Gorini, M.Joos, J.Petersen, CERN
*/

#ifndef USERACTIONSCHEDULER_H
#define USERACTIONSCHEDULER_H
#include <mutex>

#include "ROSCore/ROSHandler.h"
#include "ROSInfo/UserActionSchedulerInfo.h"
#include "ROSCore/ScheduledUserAction.h"

namespace ROS { 

  class UserActionScheduler : public ROSHandler {

  public:

    static UserActionScheduler *Instance();
 
    virtual void prepareForRun(const daq::rc::TransitionCmd& cmd);
    virtual void stopGathering(const daq::rc::TransitionCmd& cmd);
    virtual void clearInfo();
    virtual ISInfo* getISInfo();

    virtual ~UserActionScheduler () noexcept;

//    const std::vector<ScheduledUserAction *> *scheduledUserActions();	// not yet implemented  //MJ ?

    void registerHandler(ScheduledUserAction *userActionHandler);
    void unregisterHandler(ScheduledUserAction *userActionHandler);

  protected: 

    virtual void run();
    virtual void cleanup();

  private:

    static std::mutex s_mutex;                                  // local mutex to protect the creation of the singleton
    static UserActionScheduler *s_uniqueInstance;               // flag for the creation of the singleton
    std::vector<ScheduledUserAction *> m_scheduledUserActions;  // this vector keeps pointers to the user code
    bool m_threadStarted;                                       // indicates if a thread has been started or not
    UserActionSchedulerInfo m_statistics;                       // statistics

    UserActionScheduler ();                                     //Empty Constructor: it is private as part of the singleton pattern
  };
} 
#endif //USERACTIONSCHEDULER_H

