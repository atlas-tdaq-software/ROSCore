// $Id$

/*
  ATLAS TDAQ ROS/RCD

  Class: SequentialDataChannel
  Authors: B.Gorini, M.Joos, J.Petersen

*/

#ifndef SEQUENTIALDATACHANNEL_H
#define SEQUENTIALDATACHANNEL_H

#include <sys/types.h>
#include "ROSCore/PCMemoryDataChannel.h"
#include "ROSInfo/SequentialDataChannelInfo.h"
#include "ROSEventInputManager/EventInputManager.h"

namespace ROS {

  class SequentialInputHandler;

  class SequentialDataChannel : public PCMemoryDataChannel {
  public:
    
    SequentialDataChannel(unsigned int id, unsigned int configId,
                          unsigned long rolPhysicalAddress,
			  bool usePolling,
			  DFCountedPointer<Config> configuration,
			  SequentialDataChannelInfo* info);
    
    virtual ~SequentialDataChannel();

    virtual int getNextFragment(unsigned int* buffer, int max_size, unsigned long pciAddress = 0) = 0;
    virtual int poll(void) = 0;

    // Reset internal statistics
    void clearInfo();

    virtual ISInfo *getISInfo(void);

    virtual void probe(void);

    // interface with Sequential Input Handler
    
    unsigned int inputFragment(void);
    bool readyToReadout(void);
    void clear(void);
    void signalFragmentAvailable(void);

    unsigned int get_rod_L1id_prev(void);
    void set_rod_L1id_prev(unsigned int);

    static const unsigned int c_initL1id = 0xffffffff;

  private:

    bool pollDataInterrupt(void);

    int m_fragType;                                    //Tells us if we are dealing with ROD (=0) or ROS (!=0) fragments
    u_long m_rolPhysicalAddress;                       //Only used in the text of exceptions to identify the culprit
    DFFastMutex *m_fastAllocateMutex;                  //Mutex "REQUESTMUTEX" (protects access to the event memory)
    DFFastMutex *m_dataInterruptMutex;                 //Unnamed mutex. Protects m_dataInterruptFlag
    EventInputManager *m_eventInputManager;            //Our Event Input Manager
    MemoryPool *m_memoryPool;                          //The Memory Pool that is managed my our Event Input Manager
    int m_robPageSize;                                 //Size of a page in the Memory Pool in bytes
    bool m_usePolling;                                 //Detect new data by polling or interupt
    u_int m_fragmentsScheduled;                        // # fragments ready to be read out, via polling or interrupts
    u_int m_dataInterruptFlag;                         // user signals interrupt ==> increment m_fragmentsScheduled
    u_int m_rod_L1id_prev;                             //Last L1ID input
    SequentialDataChannelInfo *m_statistics;           //For IS
    SequentialInputHandler *m_sequentialInputHandler;  //A pointer to the Sequential Input Handler that controls this Data Channel
  };

  inline unsigned int SequentialDataChannel::get_rod_L1id_prev() {
    return m_rod_L1id_prev;
  }

  inline void SequentialDataChannel::set_rod_L1id_prev(unsigned int l1id) {
    m_rod_L1id_prev = l1id;
  }

}
#endif //SEQUENTIALDATACHANNEL_H
