// -*- c++ -*- $Id$

/*
  ATLAS ROS Software

  Class: PCMemoryDataChannel
  Authors: Markus Joos, Jorgen Petersen

*/

#ifndef PCMEMORYDATACHANNEL_H
#define PCMEMORYDATACHANNEL_H

#include <sys/types.h>
#include "DFThreads/DFCountedPointer.h"
#include "DFThreads/DFFastMutex.h"
#include "DFSubSystemItem/Config.h"
#include "ROSEventInputManager/EventInputManager.h"
#include "ROSMemoryPool/MemoryPool.h"
#include "ROSInfo/PCMemoryDataChannelInfo.h"
#include "ROSCore/DataChannel.h"

namespace ROS {

  class PCMemoryDataChannel : public DataChannel {
  public:
    
    PCMemoryDataChannel(unsigned int id, unsigned int configId,
                        unsigned long rolPhysicalAddress,
                        DFCountedPointer<Config> configuration,
			PCMemoryDataChannelInfo* info = new PCMemoryDataChannelInfo());		// default parameter
    
    virtual ~PCMemoryDataChannel() ;
    /**
     * Get an event fragment
     * @param ticket the ticket returned by the preceding requestFragment call
     */
    ROS::EventFragment *getFragment(int ticket);
    
    /**
     * Release all the event fragments listed in the vector
     * @param level1Ids Pointer to a vector of 32bit level 1 event identifiers of fragments to be released.
     */
    void releaseFragment(const std::vector < unsigned int > * level1Ids);
    
    /**
     * Request an individual event Fragment by Level1 Id
     * @param level1Id 32bit Level 1 event identifier of fragment to release.
     */
    void releaseFragment(int level1Id);
    
    /**
     * Request a Fragment.  It returns a ticket that has to be
     * passed to the getFragment method to obtain the requested fragment.
     * @param level1Id 32bit Level 1 event identifier of fragment to be retrieved.
     */
    int requestFragment(int level1Id);
    
    /** Reset internal statistics */
    void clearInfo();
    
    void probe(void);

    virtual ISInfo* getISInfo(void);

    /** 
     * Returns (a pointer to) the Event Input Manager 
     */
    EventInputManager *getEIM() ;

    int getPageSize();
    int getNoPages();
    
  private:
      enum {
        EIM_NUMBEROFRESERVEDPAGES = 4   // # reserved pages in the memory pool associated with the EIM
      };

      DFFastMutex *m_mutex;                    // "REQUESTMUTEX". Protects the EventInputManager and its MemoryPool
      u_int m_sourceIdentifier;                // "channelId" from the conf. DB
      int m_pageSize;                          // "memoryPoolPageSize" from the conf. DB
      int m_noPages;                           // "memoryPoolNumPages" from the conf. DB
      int m_noHashLists;                       // Number of hash lists of the EventInputManager
      int m_poolType;                          // "poolType" from the conf. DB
      int m_fragType;                          // 0=ROB, 1=ROS
      MemoryPool *m_memoryPool;                // The MemoryPool of the EventInputManager
      EventInputManager *m_eventInputManager;  // A pointer to the EventInputManager
      PCMemoryDataChannelInfo *m_statistics;   // For IS
  };
  
  inline  EventInputManager *PCMemoryDataChannel::getEIM() {
    return m_eventInputManager;
  }

  inline int PCMemoryDataChannel::getPageSize() {
    return m_pageSize;
  }

  inline int PCMemoryDataChannel::getNoPages() {
    return m_noPages;
  }

}
#endif //PCMEMORYDATACHANNEL_H
