
tdaq_package()

add_definitions(-DnoTRACE_REQUEST_HANDLER -DnoTSTAMP)

tdaq_generate_isinfo(ROSCore_ISINFO schema/ROSCoreInfo.schema.xml
  NAMESPACE ROSInfo
  NO_NAMESPACE
  OUTPUT_DIRECTORY ROSInfo
  CPP_OUTPUT is_cpp_srcs)

tdaq_add_is_schema(schema/ROSCoreInfo.schema.xml)

add_custom_target(ROSInfoGen ALL DEPENDS ${is_cpp_srcs})

tdaq_add_library(ROSUserActionScheduler
  src/UserActionScheduler.cpp
  src/ScheduledUserAction.cpp
  ${is_cpp_srcs}
  INCLUDE_DIRECTORIES ROSUtilities
  LINK_LIBRARIES rc_ItemCtrl rcc_time_stamp DFDebug tdaq-common::ers SubSystemItem) 

tdaq_add_library(ROSCore
  src/IOManager.cpp     
  src/Request.cpp       
  src/DataRequest.cpp   
  src/FragmentRequest.cpp   
  src/FragmentBuilder.cpp   
  src/RequestHandler.cpp
  src/DataChannel.cpp
  src/DataOut.cpp
  src/PCMemoryDataChannel.cpp
  src/SequentialDataChannel.cpp
  src/SequentialInputHandler.cpp
  src/SingleFragmentDataChannel.cpp
  src/TriggerInputQueue.cpp
  src/ROSHandler.cpp
  src/IOManagerConfig.cpp
  ${is_cpp_srcs}
  LINK_LIBRARIES DFConfiguration_ISINFO rc_ItemCtrl ROSUserActionScheduler 
                 ROSEventFragment ROSEventInputManager ROSMemoryPool 
                 monsvcpub monsvc
                 DFThreads SubSystemItem  is ipc DFExceptions DFDebug)


# FIXME: install info files
