//$Id$
/************************************************************************/
/*									*/
/* File             : SingleFragmentDataChannel.cpp			*/
/*									*/
/* Authors          : G. Crone, B.Gorini, J.Petersen CERN		*/
/*									*/
/* Notes: 								*/
/* 1) we are using a mutex to protect memory allocation			*/
/* 2) allocating fragments dynamically event per event			*/
/* Since we are working in an event by event mode and with only one	*/
/* request handler active (at a time), this is probably not required	*/
/* but we keep it for compatibility with the standard modes of		*/
/* operation								*/
/*									*/
/********* C 2005 - ROS/RCD  ********************************************/
#include <iostream>                                 // for operator<<, basic...
#include <memory>                                   // for allocator

#include "DFDebug/DFDebug.h"
#include "ROSMemoryPool/MemoryPool_CMEM.h"
#include "ROSMemoryPool/MemoryPool_malloc.h"
#include "ROSCore/CoreException.h"
#include "ROSEventFragment/HWFragment.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSCore/SingleFragmentDataChannel.h"
#include "DFSubSystemItem/Config.h"                 // for Config
#include "DFThreads/DFCountedPointer.h"             // for DFCountedPointer
#include "DFThreads/DFFastMutex.h"                  // for DFFastMutex
#include "ROSBufferManagement/Buffer.h"             // for Buffer
#include "ROSCore/DataChannel.h"                    // for DataChannel
#include "ROSEventFragment/EventFragment.h"         // for EventFragment
#include "ROSInfo/SingleFragmentDataChannelInfo.h"  // for SingleFragmentDat...
#include "ROSMemoryPool/MemoryPage.h"               // for MemoryPage
#include "ROSMemoryPool/MemoryPool.h"               // for MemoryPool, Memor...
class ISInfo;

using namespace ROS;
using namespace RCD; 


//Called e.g. by RCDExampleModules::VMESingleFragmentDataChannel
/**************************************************************************************************/
SingleFragmentDataChannel::SingleFragmentDataChannel(unsigned int id,
					     unsigned int channelIndex,
					     unsigned long rolPhysicalAddress,
					     DFCountedPointer<Config> configuration,
					     SingleFragmentDataChannelInfo *info) :
  DataChannel(id, channelIndex, rolPhysicalAddress)
/**************************************************************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "SingleFragmentDataChannel::constructor: Entered");

  char mutexName[]="REQUESTMUTEX";
  m_mutex = DFFastMutex::Create(mutexName);

  m_statistics = info;
  DEBUG_TEXT(DFDB_ROSFM, 7, "SingeFragmentDataChannel constructor: m_statistics@ " << m_statistics);

  int npages               = configuration->getInt("memoryPoolNumPages", channelIndex);
  int pageSize             = configuration->getInt("memoryPoolPageSize", channelIndex);
  int pooltype             = configuration->getInt("poolType", channelIndex);

  DEBUG_TEXT(DFDB_ROSFM, 7, "SingleFragmentDataChannel:: # pages  = " << npages
                             << " page size = " << pageSize << " pool type = " << pooltype);

  std::cout << std::endl; 
  std::cout << " SingleFragmentDataChannel  Configuration parameters: " << std::endl;
  std::cout << " # pages in memory pool = " << npages << std::endl;
  std::cout << " page size              = " << pageSize << std::endl;
  std::cout << " pool type              = " << pooltype << std::endl;
  std::cout << " rolPhysicalAddress     = " << rolPhysicalAddress << std::endl;

  switch (pooltype)
  {
    case MemoryPool::ROSMEMORYPOOL_MALLOC:
      m_memoryPool = new MemoryPool_malloc(npages, pageSize);
      DEBUG_TEXT(DFDB_ROSFM, 7, " SingleFragmentDataChannel:: malloc memory pool @ " << HEX(m_memoryPool));
      break;
    case MemoryPool::ROSMEMORYPOOL_CMEM_BPA:
    case MemoryPool::ROSMEMORYPOOL_CMEM_GFP:
      m_memoryPool = new MemoryPool_CMEM(npages, pageSize, pooltype);
      DEBUG_TEXT(DFDB_ROSFM, 7, " SingleFragmentDataChannel:: CMEM memory pool @ " << HEX(m_memoryPool));
      break;
    default:
      CREATE_ROS_EXCEPTION(ex1, CoreException, SGLFR_ILLPOOLTYPE, "Type="<<pooltype);
      throw(ex1);
    
    break;
  }
  DEBUG_TEXT(DFDB_ROSFM, 15, "SingleFragmentDataChannel::constructor: Done");
}


//Called from a request handler
/*************************************************************************/
ROS::EventFragment *SingleFragmentDataChannel::getFragment(int /*dummy*/)  
/*************************************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "SingleFragmentDataChannel::getFragment: Entered");

  m_mutex->lock();
  HWFragment *hwFragment = new HWFragment(m_memoryPool);	// reserve the page
  m_mutex->unlock();
  DEBUG_TEXT(DFDB_ROSFM, 20, "SingleFragmentDataChannel::getFragment: HW fragment @ " << HEX(hwFragment));
  
  Buffer *hw_buffer = hwFragment->buffer();
  DEBUG_TEXT(DFDB_ROSFM, 20, "SingleFragmentDataChannel::getFragment: buffer @ " << HEX(hw_buffer));

  int pageSize = hw_buffer->pageSize();
  DEBUG_TEXT(DFDB_ROSFM, 20, "SingleFragmentDataChannel::getFragment: page size " << pageSize);

  unsigned int *buffer_ptr = (unsigned int *) hw_buffer->reserve(pageSize);
  DEBUG_TEXT(DFDB_ROSFM, 20, "SingleFragmentDataChannel::getFragment: page pointer " << HEX(buffer_ptr));

  // get PCI address of THE one and only page
  unsigned long pciAddress = (*(hw_buffer->begin()))->physicalAddress();

  // get HW fragment
  unsigned int status;
  int noBytes = getNextFragment(buffer_ptr, pageSize, &status, pciAddress);  //getNextFragment has to be contributed by the user
  DEBUG_TEXT(DFDB_ROSFM, 20, "SingleFragmentDataChannel::getFragment:  " << " # bytes = " << noBytes <<
			      ", status = " << HEX(status) << ", PCI address = " <<  HEX(pciAddress));

  if (noBytes == -1) {	// error on transfer: no data
    noBytes = 0;
  }

  hwFragment->putStatus(status);	// add status to the HW fragment

  hw_buffer->release(pageSize - noBytes);

  m_statistics->fragmentsServed++;

  DEBUG_TEXT(DFDB_ROSFM, 15, "SingleFragmentDataChannel::getFragment: Done");
  return hwFragment;
}


//Called from IOManager::probe
/****************************************************************/
ISInfo *SingleFragmentDataChannel::getISInfo()
/****************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SingleFragmentDataChannel::getIsInfo: called");
  return m_statistics;
  DEBUG_TEXT(DFDB_ROSFM, 15, "SingleFragmentDataChannel::getIsInfo: done");
}

//Called by IOManager::prepareForRun
/********************************************/
void SingleFragmentDataChannel::clearInfo()
/********************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SingleFragmentDataChannel::clearInfo: called");

  m_statistics->fragmentsServed = 0;

  DEBUG_TEXT(DFDB_ROSFM, 15, "SingleFragmentDataChannel::clearInfo: done");
}

/********************************************************************/
SingleFragmentDataChannel::~SingleFragmentDataChannel(void)
/********************************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "SingleFragmentDataChannel::destructor: Entered");

  m_mutex->destroy();

  delete m_memoryPool;
  DEBUG_TEXT(DFDB_ROSFM, 15, "SingleFragmentDataChannel::destructor: Done");

}
