//$Id$
/************************************************************************/
/*									*/
/* File             : ScheduledUserAction.cpp				*/
/*									*/
/* Authors          : B.Gorini, M.Joos, J.Petersen CERN			*/
/*									*/
/********* C 2008 - ROS/RCD *********************************************/
#include <sys/types.h>
#include <ostream>

#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSCore/UserActionScheduler.h"
#include "ROSCore/ScheduledUserAction.h"
#include "ROSCore/CoreException.h"
#include "rcc_time_stamp/tstamp.h"

using namespace ROS;

/*******************************************************/
ScheduledUserAction::ScheduledUserAction(u_int deltaTime) 
/*******************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "ScheduledUserAction::constructor: Entered");

  m_deltaTime = deltaTime;
  DEBUG_TEXT(DFDB_ROSFM, 7, "ScheduledUserAction::constructor: deltaTime = " << m_deltaTime );

  UserActionScheduler *pTC = UserActionScheduler::Instance();
  pTC->registerHandler(this);
  
  DEBUG_TEXT(DFDB_ROSFM, 15, "ScheduledUserAction::constructor: register handler " << HEX(this));
}


/*********************************************/
ScheduledUserAction::~ScheduledUserAction(void)
/*********************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "ScheduledUserAction::destructor: Entered");

  UserActionScheduler *pTC = UserActionScheduler::Instance();
  pTC->unregisterHandler(this);
  
  DEBUG_TEXT(DFDB_ROSFM, 15, "ScheduledUserAction::destructor: Done");
}


//Called by: UserActionScheduler::run
/**********************************/
bool ScheduledUserAction::checkAge()
/**********************************/
{
  tstamp tsCurrent;

  ts_clock(&tsCurrent);
  u_int myAge = (int)(1000.0 * ts_duration(m_tsStart, tsCurrent));  //Compute age and convert from seconds to milli seconds
  if (myAge >= m_deltaTime)
  {
    DEBUG_TEXT(DFDB_ROSFM, 20, "ScheduledUserAction::checkAge. Time slice expired, my_age = " << myAge << " ms");
    ts_clock(&m_tsStart);	// re-initialise time slice
    m_lastTimeSlice = myAge;
    return(true);
  }
  else
    return(false);
}


//Called by: UserActionScheduler::run
/*********************************/
u_int ScheduledUserAction::getAge()
/*********************************/
{
  tstamp tsCurrent;

  ts_clock(&tsCurrent);
  u_int myAge = (int)(1000.0 * ts_duration(m_tsStart, tsCurrent));  //Compute age and convert from seconds to milli seconds

  if (myAge >= m_deltaTime)
  {
    DEBUG_TEXT(DFDB_ROSFM, 20, "ScheduledUserAction::checkAge. Time slice expired, my_age = " << myAge << " ms");
    return(0);
  }
  else
    return(m_deltaTime - myAge);  //Time left until task is ready to run
}


//Called by: UserActionScheduler::prepareForRun
/****************************************/
void ScheduledUserAction::startTimer(void)
/****************************************/
{
  if (ts_clock(&m_tsStart) != 0) 
  {		// initialise m_tsStart
    CREATE_ROS_EXCEPTION(ex1, CoreException, POLL_TSCLOCK, "");
    throw(ex1);
  }
  DEBUG_TEXT(DFDB_ROSFM, 15, "ScheduledUserAction::startTimer: timer initialised");
}


//Called from user code. E.g.: DDTScheduledUserAction::reactTo
/***********************************************/
u_int ScheduledUserAction::getLastTimeSlice(void)
/***********************************************/
{
  return m_lastTimeSlice;
}
