// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone, University College London
//
//  $Log$
//  Revision 1.19  2008/06/04 16:32:55  gcrone
//  Move checkAge and s_maxAge into Request base class and add s_delay and timeToExecute so that we can delay executionof a request
//
//  Revision 1.18  2008/01/28 17:09:10  jorgen
//  move ReadoutModule.h and DataChannel.xx from ROSModules
//
//  Revision 1.17  2007/01/15 17:44:26  gcrone
//  Print timeout mesage with DEBUG_TEXT instead of cout.
//
//  Revision 1.16  2006/10/31 17:47:44  gcrone
//  Remove redundant include of ROSEventFragment/EventFragment.h
//
//  Revision 1.15  2005/04/29 09:15:41  gorini
//  Removed include of obsolete SelectionCriteria class
//
//  Revision 1.14  2005/04/28 20:57:01  gorini
//  Adapted IOManager to new emon package. Moved Request implementations from here to ROSIO. Moved abstract classes from ROSIO to here to avoid cross dependencies between packages.
//
//  Revision 1.13  2004/12/02 13:38:44  joos
//  modifications and additions for EB-ROS
//
//  Revision 1.12  2004/07/29 21:54:10  gorini
//  Bug fix: monitoring was always disabled at start
//
//  Revision 1.11  2004/07/15 17:33:17  gorini
//  After receiving a stop command the effective request timeout is limited to communicationTimeout. This should guarantee that all request handlers will properly stop.
//
//  Revision 1.10  2004/03/24 14:07:26  gorini
//  Merged NewArchitecture branch.
//
//  Revision 1.9.2.2  2004/03/19 10:11:55  gorini
//  Made DataRequest more generic so that also RCDDataRequests can inherit from it.
//  buildROSFragment method has become purely virtual and is now called buildFragment.
//  Added ROSDataRequest that provides a specific implementation of buildFragment for the ROS requests.
//
//  Revision 1.9.2.1  2004/03/16 23:54:08  gorini
//  Adapted IOManager and Request classes to new ReadouModule/DataChannel architecture
//
//  Revision 1.9  2004/01/29 10:44:08  gorini
//  Bug Fix: defined the DataRequest static symbols which were only declared.
//
//  Revision 1.8  2004/01/28 16:06:50  gorini
//  Cleanup of data requests. No fucntional change
//
//  Revision 1.7  2004/01/28 15:39:13  gorini
//  Added monitoring to TestBeamRequest. Major cleanup of Request classes.
//
//  Revision 1.6  2003/07/11 13:18:02  gorini
//  Added std:: for gcc32 compilation
//
//  Revision 1.5  2003/04/30 06:49:08  glehmann
//  ROSDataChannel::getFragment now returns a generic EventFragment, not a ROBFragment
//
//  Revision 1.4  2003/04/03 12:52:54  joos
//  request time-out can now be disabled
//
//  Revision 1.3  2003/04/02 09:57:59  joos
//  Time-out added for Data Requests
//
//  Revision 1.2  2003/02/19 17:59:58  jorgen
//   added yield when requeuing; improved time stamping
//
//  Revision 1.1  2003/02/05 09:26:04  gcrone
//  Rearranged Request class hierarchy.  All data requests
//  L2,TestBeam... now subclassed from DataRequest which has a
//  buildFragment method.
//  Request::execute now returns bool for success/(temporary) failure. Now
//  retry data requests where Request::execute returns false;
//
//
// //////////////////////////////////////////////////////////////////////

#include <vector>

#include "ROSCore/DataRequest.h"
#include "ROSCore/Request.h"
class DFFastMutex;
namespace ROS {
   class DataChannel;
   class MemoryPool;
}

using namespace ROS;

//Data Request static configuration variables 

MemoryPool* DataRequest::s_pool = 0;

DFFastMutex *DataRequest::s_mutex = 0;

unsigned int DataRequest::s_runNumber = 0;

unsigned int DataRequest::s_rosId = 0;

unsigned int DataRequest::s_sdId = 0;

//Methods

DataRequest::DataRequest(const unsigned int level1Id,
                         std::vector<DataChannel*> *dataChannel,
                         const NodeID destination) :
   Request(dataChannel), 
   m_level1Id(level1Id),
   m_destination(destination)
{
   m_eventFragment = 0;
}



void DataRequest::configure(MemoryPool* pool,
			    DFFastMutex *mutex,
			    unsigned int runNumber,
			    unsigned int rosId,
			    unsigned int maxAge) {
  s_pool=pool;
  s_mutex=mutex;
  s_runNumber=runNumber;
  s_rosId=rosId;
  s_sdId=0;    //MJ: do we need a proper value here? 
  s_maxAge=maxAge; 
}

