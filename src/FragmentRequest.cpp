// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone, University College London
//
//  $Log$
//  Revision 1.11  2008/07/18 11:10:03  gcrone
//  Specify namespace std:: where necessary
//
//  Revision 1.10  2008/06/04 16:32:55  gcrone
//  Move checkAge and s_maxAge into Request base class and add s_delay and timeToExecute so that we can delay executionof a request
//
//  Revision 1.9  2008/05/18 09:02:56  gcrone
//  Issue warning if request has timed out
//
//  Revision 1.8  2008/04/03 11:19:31  gcrone
//  Call FragmentBuilder's closeFragment method
//
//  Revision 1.7  2008/03/26 10:57:38  gcrone
//  Handle the case where nothing at all has been returned from
//  getFragment when timeout expires.
//
//  Revision 1.6  2008/03/19 08:46:18  gcrone
//  Reorganise with respect to timeout
//
//  Revision 1.5  2008/03/18 12:47:25  gcrone
//  Always append a subFragment as long as one was returned by the
//  getFragment call.  Check the fragmentReady status of Fragments that
//  are returned in case they are MayCome fragments.
//
//  Revision 1.4  2008/03/06 13:37:26  gcrone
//  Handle SelectionCriteria
//
//  Revision 1.3  2008/02/25 15:17:56  gcrone
//  New DataOut API call sampled instead of get/releaseSampled
//
//  Revision 1.2  2008/02/19 14:58:48  gcrone
//  Allow for no main DataOut being present.
//
//  Revision 1.1  2008/02/04 15:19:30  gcrone
//  Add new FragmentRequest/Builder classes
//
//
//
// //////////////////////////////////////////////////////////////////////
#include <string>
#include <vector>
#include <iostream>

#include "ROSCore/FragmentRequest.h"
#include "ROSCore/Request.h"
#include "ROSCore/FragmentBuilder.h"
#include "ROSEventFragment/EventFragment.h"
#include "ROSCore/DataChannel.h"
#include "ROSCore/CoreException.h"
#include "ROSCore/DataOut.h"
#include "ROSUtilities/ROSErrorReporting.h"

#include "DFThreads/DFOutputQueue.h"
#include "DFThreads/DFFastMutex.h"

#include "ers/ers.h"

#include "rcc_time_stamp/tstamp.h"

#include "DFDebug/DFDebug.h"

using namespace ROS;

// Static configuration variables 

DFFastMutex* FragmentRequest::s_mutex = 0;

//Methods
FragmentRequest::FragmentRequest(const unsigned int level1Id,
                                 std::vector<DataChannel*>* dataChannel,
                                 bool deleteChannelVector,
                                 bool doMonitoring,
                                 bool doRelease,
                                 FragmentBuilder* builder,
                                 const NodeID destination,
                                 const unsigned int transactionId,
                                 DFOutputQueue<unsigned int>* releaseQueue) :
   Request(dataChannel),
   m_deleteChannelVector(deleteChannelVector),
   m_doMonitoring(doMonitoring),
   m_doRelease(doRelease),
   m_builder(builder),
   m_level1Id(level1Id),
   m_transactionId(transactionId),
   m_destination(destination),
   m_releaseQueue(releaseQueue)
{
   m_eventFragment = 0;
}

FragmentRequest::~FragmentRequest() {
   if (m_deleteChannelVector) {
      delete m_dataChannels;
   }
}


void FragmentRequest::configure(
                                DFFastMutex *mutex,
                                unsigned int maxAge) {
   s_mutex=mutex;
   s_maxAge=maxAge;
}

int FragmentRequest::execute(void)
{
   DEBUG_TEXT(DFDB_ROSCORE, 20, "FragmentRequest::execute for L1Id "<< m_level1Id<< std::endl);

   //This code is to record the last runs through this function with 
   //the ring buffer mode of the rcc_time_stamp package
   //unsigned int tsdata = (m_level1Id * 100) + 2000000000;
   //TS_RECORD(TS_H5,(tsdata+1));

   // Ask the builder to create us an empty fragment.
   s_mutex->lock() ;
   DEBUG_TEXT(DFDB_ROSCORE, 20, "FragmentRequest::execute calling builder->createFragment for L1Id "<< m_level1Id<< std::endl);
   m_eventFragment=m_builder->createFragment(m_level1Id, m_dataChannels->size());
   s_mutex->unlock() ;


   std::vector<DataChannel*>::iterator chanStartIter=m_dataChannels->begin();
   std::vector<DataChannel*>::iterator chanEndIter=m_dataChannels->end();
   // Pre-request data from all DataChannels involved
   int index=0;
   for (std::vector<DataChannel*>::iterator dc=chanStartIter; dc!=chanEndIter; dc++) {
      m_ticket[index++] = (*dc)->requestFragment(m_level1Id);      
   }

   bool fragmentOk=true;
   index=0;
   int partsReceived=0;
   for (std::vector<DataChannel*>::iterator dc=chanStartIter; dc!=chanEndIter; dc++) {
      DEBUG_TEXT(DFDB_ROSCORE, 20, "FragmentRequest::execute calling dc->getFragment for L1Id "<< m_level1Id<< std::endl);
      EventFragment* subFragment = ((*dc)->getFragment(m_ticket[index])) ;
      if (subFragment != 0) {
         partsReceived++;
         DEBUG_TEXT(DFDB_ROSCORE, 20, "FragmentRequest::execute calling builder->appendFragment for L1Id "<< m_level1Id<< std::endl);
         m_builder->appendFragment(m_eventFragment,subFragment);
         TS_RECORD(TS_H1,2350);

         if (!subFragment->fragmentReady()) {
            fragmentOk=false;
         }

         s_mutex->lock() ;
         delete subFragment;
         s_mutex->unlock() ;
      }
      else {
         DEBUG_TEXT(DFDB_ROSCORE, 15, "FragmentRequest for L1Id "<< m_level1Id << " missing data aborting\n");
         fragmentOk=false;
         TS_RECORD(TS_H1,2360);
      }
      index++;
   }


   bool retired=false;
   if (!fragmentOk) {
      retired=checkAge(s_maxAge);
   }

   if (fragmentOk || (retired && partsReceived>0)) {

      if (retired) {
         // Issue warning message
         CREATE_ROS_EXCEPTION(tIssue,CoreException,CoreException::TIMEOUT,
                              "in request for fragment with L1 ID "<<m_level1Id);
         ers::warning(tIssue);
      }
      DEBUG_TEXT(DFDB_ROSCORE, 20, "FragmentRequest::execute calling main sendData for L1Id "<< m_level1Id<< std::endl);

      m_builder->closeFragment(m_eventFragment);

      unsigned int status=m_eventFragment->status();
      // Pass it on
      DataOut* mainOut=DataOut::main();
      if (mainOut != 0) {
         mainOut->sendData(m_eventFragment->buffer(), m_destination, m_transactionId, status);
      }
      TS_RECORD(TS_H1, 2370);

      if (m_doMonitoring) {
         emon::SelectionCriteria* criteria=DataOut::selectionCriteria();
         bool selected=true;
         if (criteria!=0) {
            selected=m_eventFragment->selectionCriteriaMatch(criteria);
         }
         //Monitor it 
         if (selected) {
            DataOut *sampOut=DataOut::sampled();
            if (sampOut != 0) {
               DEBUG_TEXT(DFDB_ROSCORE, 20, "FragmentRequest::execute calling sampled sendData for L1Id "<< m_level1Id<< std::endl);
               sampOut->sendData(m_eventFragment->buffer(), m_destination);
            }
         }
      }
   }


   // delete the RosFragment now it's been sent on its way
   s_mutex->lock();
   DEBUG_TEXT(DFDB_ROSCORE, 20, "FragmentRequest::execute deleting m_eventFragment for L1Id "<< m_level1Id<< std::endl);

   delete m_eventFragment;
   m_eventFragment = 0;
   s_mutex->unlock();

   TS_RECORD(TS_H1, 2390);

   if (m_doRelease) {
      // Release the original fragments (in the EIM & memory pool)
      // mutexes ? 									 FIXME
      for (std::vector<DataChannel*>::iterator dc=m_dataChannels->begin();
           dc!=m_dataChannels->end(); dc++) {
         (*dc)->releaseFragment(m_level1Id) ;
      }
   }

   DEBUG_TEXT(DFDB_ROSCORE, 20, "FragmentRequest::execute finished with L1Id "<< m_level1Id<< std::endl);
   if (fragmentOk || retired) {
      if (m_releaseQueue!=0) {
         m_releaseQueue->push(m_level1Id);
      }
      if (retired) {
         DEBUG_TEXT(DFDB_ROSCORE, 20, "FragmentRequest::execute timeout for L1Id "<< m_level1Id<< std::endl);
         return(REQUEST_TIMEOUT);
      }
      return(REQUEST_OK);
   }
   else {
      TS_RECORD(TS_H1, 2391);
      return(REQUEST_NOTFOUND);
   }
}


std::string FragmentRequest::what()
{
   std::string description="FragmentRequest for event id ??";
   return (description);
}

std::ostream& FragmentRequest::put(std::ostream& stream) const
{
   stream << "Fragment Request for event " << m_level1Id ;
   return stream;
}
