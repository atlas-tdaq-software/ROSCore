#include "ROSCore/DataOut.h"
#include "DFSubSystemItem/Config.h"

using namespace ROS;

DataOut * DataOut::s_mainDataOut=0;
DataOut * DataOut::s_sampledDataOut=0;
DataOut * DataOut::s_debugDataOut=0;
emon::SelectionCriteria* DataOut::s_criteria=0;

DataOut::DataOut(DataOut::Type type) : m_scalingFactor(1) {
  if (type==MAIN && s_mainDataOut==0) {
    s_mainDataOut=this;
    m_type=MAIN;
  }
  else if (type==MAIN) {
    m_type=OTHER;
  }
  else if (type==SAMPLED) {
    s_sampledDataOut=this;
    m_type=SAMPLED;
  }
  else if (type==DEBUGGING) {
    s_debugDataOut=this;
    m_type=DEBUGGING;
  }
  else {
     m_type=OTHER;
  }
}

DataOut::~DataOut() noexcept {
   if (m_type==MAIN) {
      s_mainDataOut=0;
   }
   else if (m_type==SAMPLED) {
      s_sampledDataOut=0;
   }
   else if (m_type==DEBUGGING) {
      s_debugDataOut=0;
   }
}

