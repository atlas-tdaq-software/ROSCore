//$Id$
/************************************************************************/
/*                                                                      */
/* File             : UserActionScheduler.cpp				*/
/*                                                                      */
/* Authors          : B.Gorini, M.Joos, J.Petersen CERN                 */
/*                                                                      */
/* a singleton class for catching expired time slices			*/
/* and scheduling user action functions					*/
/*									*/
/********* C 2008 - ROS/RCD *********************************************/

#include <algorithm>
#include <sys/types.h>
#include <iostream>
#include <mutex>
#include <string>
#include <vector>

#include "DFThreads/DFThread.h"
#include "ROSCore/ROSHandler.h"
#include "ROSCore/ScheduledUserAction.h"
#include "ROSInfo/UserActionSchedulerInfo.h"

#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSCore/UserActionScheduler.h"
#include "ROSCore/CoreException.h"

#include "rcc_time_stamp/tstamp.h"
#include "DFSubSystemItem/Config.h"

class ISInfo;
namespace daq { namespace rc { class TransitionCmd; } }


//Unless stated otherwise all methods in this class are called from IOManager

using namespace ROS;

std::mutex UserActionScheduler::s_mutex;
UserActionScheduler *UserActionScheduler::s_uniqueInstance = 0;


/**************************************************/
UserActionScheduler *UserActionScheduler::Instance() 
/**************************************************/
{
   std::lock_guard<std::mutex> lock(s_mutex);
  
  if (s_uniqueInstance == 0) 
    s_uniqueInstance = new UserActionScheduler;

  ts_open(1, TS_DUMMY);

  return s_uniqueInstance;
}


/*********************************************************************/
UserActionScheduler::UserActionScheduler(void) : ROSHandler("UserActionScheduler"), m_threadStarted(false)
/*********************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "UserActionScheduler::constructor: Entered (Empty function)");
}


/*****************************************/
UserActionScheduler::~UserActionScheduler() noexcept
/*****************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "UserActionScheduler::destructor: Entered");
  s_uniqueInstance = 0;
  //we do not clean up everything as we will exit anyway
  DEBUG_TEXT(DFDB_ROSFM, 15, "UserActionScheduler::destructor: Done");
}


/*******************************************/
void UserActionScheduler::prepareForRun(const daq::rc::TransitionCmd& /*cmd*/)
/*******************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "UserActionScheduler::prepareForRun: entered");

  m_statistics.callsToUserAction = 0;

  DEBUG_TEXT(DFDB_ROSFM, 7, "UserActionScheduler::prepareForRun: # handlers = " << m_scheduledUserActions.size());
  // if there are registered handlers then start the thread
  if (m_scheduledUserActions.size() > 0) {
    for (u_int i = 0; i < m_scheduledUserActions.size(); i++)
      m_scheduledUserActions[i]->startTimer();

    DEBUG_TEXT(DFDB_ROSFM, 7, "UserActionScheduler::prepareForRun: going to start thread ");
    startExecution();
    m_threadStarted = true;
    DEBUG_TEXT(DFDB_ROSFM, 7, "UserActionScheduler::prepareForRun: Timer Catcher started");
  }
}


/************************************/
void UserActionScheduler::stopGathering(const daq::rc::TransitionCmd& /*cmd*/)
/************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "UserActionScheduler::stopEB: Entered");

  if (m_threadStarted) 
  {
    //Stop the thread
    DEBUG_TEXT(DFDB_ROSFM, 7, "UserActionScheduler::stopEB: stopping Timer catcher thread");
    stopExecution();

    try 
    {
      waitForCondition(DFThread::TERMINATED, 5);     
    }
    catch (DFThread::Timeout&) 
    {
      std::cout << " TimeOut on stopping UserActionScheduler thread " << std::endl;  // We have created a zombie thread
      CREATE_ROS_EXCEPTION(ex1, CoreException, THREAD_WAIT_TIMEOUT, "stopping UserActionScheduler ");
      throw(ex1);
    }

    m_threadStarted = false;
    std::cout << " UserActionScheduler::stopEB: UserActionScheduler thread stopped" << std::endl;
  }

  DEBUG_TEXT(DFDB_ROSFM, 15, "UserActionScheduler::stopEB: Done");
}


/**************************************/
ISInfo *UserActionScheduler::getISInfo()
/**************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "UserActionScheduler::getIsInfo: called");
  return &m_statistics;
}


/***********************************/
void UserActionScheduler::clearInfo()
/***********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "UserActionScheduler::clearInfo: Entered");
  m_statistics.callsToUserAction = 0;
  DEBUG_TEXT(DFDB_ROSFM, 15, "UserActionScheduler::clearInfo: Done");
}


//Called from the constructor of ScheduledUserAction;
/*********************************************************************************/
void UserActionScheduler::registerHandler(ScheduledUserAction *scheduledUserAction)
/*********************************************************************************/
{
  m_scheduledUserActions.push_back(scheduledUserAction);
  DEBUG_TEXT(DFDB_ROSFM, 20, "UserActionScheduler::registerHandler: handler = " << scheduledUserAction);
}


//Called from the destructor of ScheduledUserAction;
/***********************************************************************************/
void UserActionScheduler::unregisterHandler(ScheduledUserAction *scheduledUserAction)
/***********************************************************************************/
{
  std::vector<ScheduledUserAction*>::iterator it;

  it = find(m_scheduledUserActions.begin(), m_scheduledUserActions.end(), scheduledUserAction);
  DEBUG_TEXT(DFDB_ROSFM, 20, "UserActionScheduler::unregisterHandler: handler = " << *it);
  m_scheduledUserActions.erase(it);
}


//Called indirectly from prepareForRun()
/*********************************/
void UserActionScheduler::run(void)
/*********************************/
{
  u_int dummy, time_left, min_time;
  
  DEBUG_TEXT(DFDB_ROSFM, 15, "UserActionScheduler::run: Entered for " << m_scheduledUserActions.size() << " tasks");

  while(1) 
  {
    //1. Look for the task that has the shortest time left 
    min_time = 0xffffffff;
    for (u_int i = 0; i < m_scheduledUserActions.size(); i++) 
    {
      time_left = m_scheduledUserActions[i]->getAge();
      DEBUG_TEXT(DFDB_ROSFM, 15, "UserActionScheduler::run: Time left for task " << i << " = " << time_left << " ms");
      if (time_left < min_time)
        min_time = time_left;
    }
    DEBUG_TEXT(DFDB_ROSFM, 15, "UserActionScheduler::run: Time left to next task = " << min_time << " ms");
    
    //2. Wait until the next task has to be executed
    if (min_time)
      ts_wait(min_time * 1000, &dummy);  //We may sleep or yield in ts_wait

    cancellationPoint();
    
    //3. Execute the task(s)
    for (u_int i = 0; i < m_scheduledUserActions.size(); i++) 
    {
      if (m_scheduledUserActions[i]->checkAge()) 
      {
        DEBUG_TEXT(DFDB_ROSFM, 15, "UserActionScheduler::run: Executing task " << i);
        m_statistics.callsToUserAction++;
        m_scheduledUserActions[i]->reactTo();
      }
    }
  }
}


//Called by DFThreads when the thread gets stopped
/*********************************/
void UserActionScheduler::cleanup()
/*********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "UserActionScheduler::cleanup: Empty method");
}
