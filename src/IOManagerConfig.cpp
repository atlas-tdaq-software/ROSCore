#include "ROSCore/IOManagerConfig.h"

using namespace ROS;

std::string IOManagerConfig::s_globalOptions="";

void IOManagerConfig::globalOptions(std::string options) {
   s_globalOptions=options;
}

const std::string& IOManagerConfig::globalOptions() {
   return s_globalOptions;
}
