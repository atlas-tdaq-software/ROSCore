#include "ROSCore/TriggerInputQueue.h"
#include <tbb/concurrent_queue.h>  // for concurrent_bounded_queue

using namespace ROS;

TriggerInputQueue* TriggerInputQueue::s_instance=0;
unsigned int TriggerInputQueue::s_numInstance=0;
std::mutex TriggerInputQueue::s_mutex;

TriggerInputQueue* TriggerInputQueue::instance() {
   std::lock_guard<std::mutex> lock(s_mutex);
   if (s_instance==0) {
      s_instance=new TriggerInputQueue();
   }
   s_numInstance++;
   return s_instance;
}

void TriggerInputQueue::destroy() {
   std::lock_guard<std::mutex> lock(s_mutex);
   s_numInstance--;
   if (s_numInstance==0) {
      delete s_instance;
      s_instance=0;
   }
}
TriggerInputQueue::TriggerInputQueue() {
}

void TriggerInputQueue::reset() {
   m_queue.clear();
}
void TriggerInputQueue::abort() {
   m_queue.abort();
}
void TriggerInputQueue::setSize(unsigned int size) {
   m_queue.set_capacity(size);
}
