// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone, University College London
//
//  $Log$
//  Revision 1.1  2008/10/31 16:34:30  gcrone
//  New generic handler interface
//
//
// //////////////////////////////////////////////////////////////////////

#include "ROSCore/ROSHandler.h"
#include "DFSubSystemItem/Config.h"

using namespace ROS;

std::list<ROSHandler*> ROSHandler::s_handlers;

ROSHandler::ROSHandler(std::string name) : m_name(name) {
   s_handlers.push_back(this);
}

ROSHandler::~ROSHandler() noexcept {
   for (iterator hiter=s_handlers.begin(); hiter!=s_handlers.end(); hiter++) {
      if (*hiter==this){
         s_handlers.erase(hiter);
         break;
      }
   }
}

std::string ROSHandler::name(){
   return m_name;
}

ROSHandler::iterator ROSHandler::begin() {
   return s_handlers.begin();
}
ROSHandler::iterator ROSHandler::end() {
   return s_handlers.end();
}

void ROSHandler::clearInfo() {
}
