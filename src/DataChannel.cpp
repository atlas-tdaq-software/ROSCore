/*
  ATLAS ROS Software

  Class: DataChannel
  Authors: ATLAS ROS group 	
*/
#include <map>
#include <ostream>
#include <vector>

#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSCore/DataChannel.h"
#include "ROSCore/CoreException.h"
#include "DFSubSystemItem/Config.h"
#include "DFThreads/DFCountedPointer.h"
#include "ers/ers.h"
class ISInfo;

using namespace ROS;

//list of channels belonging to the specific instance
std::vector < DataChannel *> DataChannel::s_dataChannels;

//global map of all data channels
std::map < unsigned int, DataChannel * > DataChannel::s_dataChannelsMap ;

unsigned int DataChannel::s_oldestValidId=0;

void DataChannel::oldestValid(unsigned int oldestId) {
   s_oldestValidId=oldestId;
}


//Constructor
DataChannel::DataChannel(unsigned int id, unsigned int configId, unsigned long physicalAddress) 
   : m_id(id), m_configId(configId), m_physicalAddress(physicalAddress)
{
  //Check that the channel does not exist already
  if (s_dataChannelsMap.count(id)==0) {
    //Add it to the map 
    s_dataChannelsMap[id]=this;
    //Add it also to the plain vector
    s_dataChannels.push_back(this);
  }
  //Otherwise there is a problem in the configuration
  else {
    CREATE_ROS_EXCEPTION(ex1,CoreException,DUPLICATEID," channel  ID = 0x" << std::hex << m_id << std::dec);
    throw(ex1);
  }
}

//Destructor
DataChannel::~DataChannel() {
  //Look if the channel is registered in the global map
  if (s_dataChannelsMap.count(m_id)>0) {
    //Remove it from the map
    s_dataChannelsMap.erase(m_id);
    //Remove it from the vector
    for (std::vector<DataChannel *>::iterator it=s_dataChannels.begin(); 
	 it!=s_dataChannels.end(); it++) {
       if ((*it)->id() == m_id) {
          s_dataChannels.erase(it);
          break;
       }
    }
  }
  //Otherwise there has been a problem
  else {
    CREATE_ROS_EXCEPTION(ex1,CoreException,UNDEFINEDID," trying to delete channel with ID 0x" <<
                                                          std::hex << m_id << std::dec);
    ers::error(ex1);
  }  
}

  unsigned int DataChannel::id() {
    return m_id;
  }

  unsigned int DataChannel::configId() {
    return m_configId;
  }

unsigned long DataChannel::physicalAddress() {
   return m_physicalAddress;
}

  int DataChannel::numberOfChannels() {
    return s_dataChannels.size();
  }

  int DataChannel::collectGarbage(unsigned int /*oldestId*/) { //oldestId commented out to remove compiler warning
    return(0);
  }

  // Dummy getInfo() while we are phasing it out.
  DFCountedPointer < Config > DataChannel::getInfo() {
     DFCountedPointer<Config> empty=Config::New();
     return empty;
  }

  void DataChannel::probe() {
  }

  ISInfo* DataChannel::getISInfo() {
//     return m_isInfo;
     return 0;
  }

  DataChannel * DataChannel::channel(unsigned int id) {
    //Look if there is a channel with the requested id
    if (s_dataChannelsMap.count(id)>0) {
      return s_dataChannelsMap[id];
    }
    else {
      std::ostringstream msg;
      msg << " asked for channel ID " << id ;
      throw CoreException(CoreException::UNDEFINEDID,msg.str());
    }
  }

  std::vector < DataChannel * > * DataChannel::channels() {
    return &s_dataChannels;
  }


