//$Id$
/*
  ATLAS ROS Software

  Class: PCMemoryDataChannel
  Authors: Markus Joos, Jorgen Petersen
 	
*/
#include <iostream>
#include <vector>

#include <stdint.h>


#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSEventFragment/ROBFragment.h"
#include "ROSEventFragment/FullFragment.h"
#include "ROSCore/CoreException.h"    
#include "ROSCore/PCMemoryDataChannel.h"
#include "DFSubSystemItem/Config.h"                   // for Config
#include "DFThreads/DFCountedPointer.h"               // for DFCountedPointer
#include "DFThreads/DFFastMutex.h"                    // for DFFastMutex
#include "ROSCore/DataChannel.h"                      // for DataChannel
#include "ROSEventFragment/EventFragment.h"           // for EventFragment
#include "ROSEventFragment/EventFragmentException.h"  // for EventFragmentEx...
#include "ROSEventInputManager/EventDescriptor.h"     // for evDesc_t
#include "ROSEventInputManager/EventInputManager.h"   // for EventInputManager
#include "ROSInfo/PCMemoryDataChannelInfo.h"          // for PCMemoryDataCha...
#include "ROSMemoryPool/MemoryPage.h"                 // for MemoryPage
#include "ROSMemoryPool/MemoryPool.h"                 // for MemoryPool
#include "ers/ers.h"                                  // for warning
class ISInfo;

using namespace ROS;

/***************************************************************************************/
PCMemoryDataChannel::PCMemoryDataChannel(unsigned int id,
					 unsigned int configId,
					 unsigned long rolPhysicalAddress,
                                         DFCountedPointer<Config> configuration,
					 PCMemoryDataChannelInfo* info) :
  DataChannel(id, configId, rolPhysicalAddress)
/***************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "PCMemoryDataChannel constructor called");

  if (info == 0) {
    CREATE_ROS_EXCEPTION(ex1, CoreException, INFOZERO, "");
    throw(ex1);
  }

  m_statistics = info;	// -> UserDataChannelInfo object
  DEBUG_TEXT(DFDB_ROSFM, 20, "PCMemoryDataChannel constructor :  m_statistics @ " << m_statistics);

  char mutexName[]="REQUESTMUTEX";
  m_mutex    = DFFastMutex::Create(mutexName);
  m_fragType = configuration->getInt("inputFragmentType");

  // Create an Event Input Manager + its internal memory pool
  m_sourceIdentifier  = configuration->getInt("channelId",configId);
  m_pageSize          = configuration->getInt("memoryPoolPageSize",configId);
  m_noPages           = configuration->getInt("memoryPoolNumPages",configId);
  m_poolType          = configuration->getInt("poolType",configId);
  // compute # hash lists as smallest power of 2 less than the # pages
  for (int n = 0; n < 32; n++)
  {
    if ((m_noPages / (1<<n)) == 0)
    {
      m_noHashLists = 1<<(n-1);
      break;
    }
  }

  // reserve pages in pool; this number should be = # request handlers but since we don't know it here:
  // reserve 4 pages  FIXME sometime  //MJ should be cleaned up once the new requeuing is implemented
  m_eventInputManager = new EventInputManager(m_noPages, m_pageSize, m_noHashLists, m_poolType, EIM_NUMBEROFRESERVEDPAGES);
  m_memoryPool        = m_eventInputManager->getMemoryPool();

  std::cout << std::endl;
  std::cout << " PCMemoryDataChannel  Configuration parameters: " << std::endl;
  std::cout << " # pages in memory pool = " << std::dec << m_noPages << std::endl;
  std::cout << " page size              = " << m_pageSize << std::endl;
  std::cout << " # hash lists           = " << m_noHashLists << std::endl;
  std::cout << " # reserved pages       = " << EIM_NUMBEROFRESERVEDPAGES << std::endl;
  std::cout << " ROB ID                 = " << m_sourceIdentifier << std::endl;
  std::cout << " pool type              = " << m_poolType << std::endl;
  std::cout << " rolPhysicalAddress     = " << rolPhysicalAddress << std::endl;
  std::cout << " inputFragmentType      = " << m_fragType << std::endl;
  DEBUG_TEXT(DFDB_ROSFM, 15, "PCMemoryDataChannel constructor done\n");
}

/*************************************************/
PCMemoryDataChannel::~PCMemoryDataChannel()
/*************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "PCMemoryDataChannel::destructor  called");
  m_mutex->destroy();

  delete m_eventInputManager;   // deletes also the Memory Pool

  DEBUG_TEXT(DFDB_ROSFM, 15, "PCMemoryDataChannel::destructor  done");
}


//Called by IOManager.cpp
/****************************************************************/
ISInfo* PCMemoryDataChannel::getISInfo()
/****************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "PCMemoryDataChannel::getIsInfo: called");
  return m_statistics;
  DEBUG_TEXT(DFDB_ROSFM, 15, "PCMemoryDataChannel::getIsInfo: done");
}


//Called by IOManager.cpp
/***********************************/
void PCMemoryDataChannel::probe()
/***********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "PCMemoryDataChannel::probe: called");
  m_statistics->freePages = m_memoryPool->numberOfFreePages();   //Update this counter every 5 seconds. The other counters are updated in real time
  DEBUG_TEXT(DFDB_ROSFM, 15, "PCMemoryDataChannel::probe: done");
}

//Called by IOManager.cpp
/***********************************/
void PCMemoryDataChannel::clearInfo()
/***********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "PCMemoryDataChannel::clearInfo: called");
  m_statistics->fragmentsServed = 0;
  m_statistics->fragmentsMissed = 0;
  m_statistics->fragmentsLost = 0;
  m_statistics->freePages = 0;

  DEBUG_TEXT(DFDB_ROSFM, 15, "PCMemoryDataChannel::clearInfo: done");
}


//Called from request handler 
/****************************************************/
int PCMemoryDataChannel::requestFragment(int level1Id)
/****************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "PCMemoryDataChannel::requestFragment: fragment with L1ID " << level1Id << " requested");
  return(level1Id);
}


//Called from request handler
/*****************************************************/
void PCMemoryDataChannel::releaseFragment(int level1Id)
/*****************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "PCMemoryDataChannel::releaseFragment: called");
  DEBUG_TEXT(DFDB_ROSFM, 20, "PCMemoryDataChannel::releaseFragment: # free pages before delete " <<  m_memoryPool->numberOfFreePages());

  m_mutex->lock();
  // removes the ED & the page
  m_eventInputManager->deleteById(level1Id);  
  m_mutex->unlock();

  DEBUG_TEXT(DFDB_ROSFM, 20, "PCMemoryDataChannel::releaseFragment: # free pages after delete " << m_memoryPool->numberOfFreePages());
  DEBUG_TEXT(DFDB_ROSFM, 10, "PCMemoryDataChannel::releaseFragment: fragment with L1ID " << level1Id << " released\n");
  DEBUG_TEXT(DFDB_ROSFM, 15, "PCMemoryDataChannel::releaseFragment: done");
}


//Called from request handler
/***************************************************************************************/
void PCMemoryDataChannel::releaseFragment(const std::vector < unsigned int > * level1Ids)
/***************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "PCMemoryDataChannel::releaseFragment: called");
  m_mutex->lock();
  m_eventInputManager->deleteById(level1Ids);  //"not found" cannot happen if we use (as we should always do) a data driven trigger
  m_mutex->unlock();

  DEBUG_TEXT(DFDB_ROSFM, 10, "PCMemoryDataChannel::releaseFragment: executing ReleaseRequest for events " << level1Ids->front() << ".." << level1Ids->back());
  DEBUG_TEXT(DFDB_ROSFM, 15, "PCMemoryDataChannel::releaseFragment: done");
}


//Called from request handler
/**************************************************************/
ROS::EventFragment* PCMemoryDataChannel::getFragment(int ticket)
/**************************************************************/
{
  evDesc_t *ed;
  MemoryPage *mem_page;
  EventFragment *fragment=0;

  DEBUG_TEXT(DFDB_ROSFM, 10, "PCMemoryDataChannel::getFragment: fragment with L1ID " << ticket << " requested");

  // protect EIM and fast allocate with same mutex
  m_mutex->lock();

  //Find the requested event
  ed = m_eventInputManager->getById(ticket);

  if (m_fragType == 0)
  {
    DEBUG_TEXT(DFDB_ROSFM, 10, "PCMemoryDataChannel::getFragment: processing ROB fragment");
    if((intptr_t)ed == EventInputManager::EIM_MAYCOME || (intptr_t)ed == EventInputManager::EIM_NEVERTOCOME)
    {
      try
      {
	fragment = new ROBFragment(m_memoryPool, ticket, m_sourceIdentifier, 0); //create an empty ROB fragment
	Buffer *mem_buffer = fragment->buffer();                                 //get the Buffer of the ROB fragment
	Buffer::page_iterator mem_page_i = mem_buffer->begin();
	MemoryPage *mem_page = const_cast<MemoryPage *>(*mem_page_i);            //get the memory page of the buffer
	mem_page->lock();
	evDesc_t *ed = m_eventInputManager->getEventDescriptor(mem_page);        //get a pointer to the event descriptor
	ed->L1id = ticket;                                                       //set the L1ID
	m_eventInputManager->createEvent(ed);                                    //Insert the event into the Event Input Manager

	if ((intptr_t)ed == EventInputManager::EIM_MAYCOME)
	{
	  m_statistics->fragmentsMissed++;
	  fragment->setStatus(EventFragment::STATUS_MAYCOME);
	  DEBUG_TEXT(DFDB_ROSFM, 10, "PCMemoryDataChannel::getFragment: Fragment for L1ID " << ticket << " has not yet arrived");
	}

	if ((intptr_t)ed == EventInputManager::EIM_NEVERTOCOME)
	{
	  m_statistics->fragmentsLost++;
	  fragment->setStatus(EventFragment::STATUS_LOST);
	  DEBUG_TEXT(DFDB_ROSFM, 10, "PCMemoryDataChannel::getFragment: Fragment for L1ID " << ticket << " does not exist");
          CREATE_ROS_EXCEPTION(ex1, CoreException, PCMEMCHAN_LOST, "\n L1ID = " << ticket << ", ROL physical addr = " << physicalAddress());
          ers::warning(ex1);
	}
      }
      catch (EventFragmentException& e)
      {
	DEBUG_TEXT(DFDB_ROSFM, 5, e);
	m_mutex->unlock();
	throw e;
      }
      m_mutex->unlock();
      return(fragment);
    }
  }
  
  if (m_fragType == 1)
  {
    DEBUG_TEXT(DFDB_ROSFM, 10, "PCMemoryDataChannel::getFragment: processing full fragment");
    if ((intptr_t)ed == EventInputManager::EIM_MAYCOME || (intptr_t)ed == EventInputManager::EIM_NEVERTOCOME) 
    {
      DEBUG_TEXT(DFDB_ROSFM, 5, "PCMemoryDataChannel::getFragment: Fragment for L1ID " << ticket << " is not available");
      CREATE_ROS_EXCEPTION(ex1, CoreException, NODATA, "L1ID = " << ticket << ", ROL physical addr = " << physicalAddress());
      throw(ex1);
    }
  }
  
  m_statistics->fragmentsServed++;
  DEBUG_TEXT(DFDB_ROSFM, 20, "PCMemoryDataChannel::getFragment: getById returns " << HEX(ed));

  mem_page = ed->myPage;

  DEBUG_TEXT(DFDB_ROSFM, 20, "PCMemoryDataChannel::getFragment: Dumping fields of this event descriptor:");
  DEBUG_TEXT(DFDB_ROSFM, 20, "PCMemoryDataChannel::getFragment: Address of memory page = " << std::hex << mem_page << std::dec);
  DEBUG_TEXT(DFDB_ROSFM, 20, "PCMemoryDataChannel::getFragment: Fragment size (bytes)  = " << ed->RODFragmentSize);
  DEBUG_TEXT(DFDB_ROSFM, 20, "PCMemoryDataChannel::getFragment: Fragment status        = " << ed->RODFragmentStatus);

  if (m_fragType == 0)
  {
    //Create a ROB Fragment in the Buffer and initialize it
    fragment = new ROBFragment(mem_page, ed->RODFragmentSize, 0, m_sourceIdentifier);
    DEBUG_TEXT(DFDB_ROSFM, 20, "PCMemoryDataChannel::getFragment: ROB fragment is at " << HEX(fragment));
  }
  
  if (m_fragType == 1)
  { 
    //Create a Full Fragment in the Buffer 
    fragment = new FullFragment(mem_page);
    DEBUG_TEXT(DFDB_ROSFM, 20, "PCMemoryDataChannel::getFragment: Full fragment is at " << HEX(fragment));
  } 
 
  m_mutex->unlock();

  DEBUG_TEXT(DFDB_ROSFM, 10, "PCMemoryDataChannel::getFragment: PCMemoryDataChannel:: got fragment with L1ID " << ticket);
  DEBUG_TEXT(DFDB_ROSFM, 15, "PCMemoryDataChannel::getFragment: done");
  return(fragment);
}
