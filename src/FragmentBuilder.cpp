// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone, University College London
//
//  $Log$
//  Revision 1.2  2008/04/03 11:18:55  gcrone
//  Add closeFragment method
//
//  Revision 1.1  2008/02/04 15:19:30  gcrone
//  Add new FragmentRequest/Builder classes
//
//
// //////////////////////////////////////////////////////////////////////

#include "ROSCore/FragmentBuilder.h"

using namespace ROS;


MemoryPool* FragmentBuilder::s_pool = 0;

unsigned int FragmentBuilder::s_runNumber = 0;

unsigned int FragmentBuilder::s_rosId = 0;

unsigned int FragmentBuilder::s_sdId = 0;


void FragmentBuilder::configure(MemoryPool* pool,
                                unsigned int runNumber,
                                unsigned int rosId,
                                unsigned int subdetId) {
   s_pool=pool;
   s_runNumber=runNumber;
   s_rosId=rosId;
   s_sdId=0;    //MJ: do we need a proper value here? 
}

void FragmentBuilder::closeFragment(EventFragment* /*fragment*/) {
}
