// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone, University College London
//
//  $Log$
//  Revision 1.7  2008/06/09 10:55:25  gcrone
//  Remove special meaning of 0 for checkAge()
//
//  Revision 1.6  2008/06/04 16:32:55  gcrone
//  Move checkAge and s_maxAge into Request base class and add s_delay and timeToExecute so that we can delay executionof a request
//
//  Revision 1.5  2004/01/28 16:06:50  gorini
//  Cleanup of data requests. No fucntional change
//
//  Revision 1.4  2004/01/28 15:39:13  gorini
//  Added monitoring to TestBeamRequest. Major cleanup of Request classes.
//
//  Revision 1.3  2003/03/11 16:36:06  pyannis
//  Changes to compile with gcc 3.2 too...
//
//  Revision 1.2  2002/11/08 15:31:03  gcrone
//  New error/exception handling
//
//  Revision 1.1.1.1  2002/10/07 09:50:07  pyannis
//  Initial version of DataFlow ROS packages tree...
//
//  Revision 1.1.1.1  2002/08/23 13:36:54  gorini
//  First import
//
//  Revision 1.1  2002/08/01 14:15:55  gcrone
//  First 'working' version using ROS namespace and dynamically loaded libraries
//
//
// //////////////////////////////////////////////////////////////////////
#include <iostream>
#include <chrono>
#include <vector>

#include "ROSCore/Request.h"
#include "DFDebug/DFDebug.h"

namespace ROS { class DataChannel; }

using namespace ROS;

unsigned int Request::s_delay=0;
unsigned int Request::s_maxAge=0;

std::ostream& operator <<(std::ostream& stream, const Request& req)
{
   return (req.put(stream));
}
Request::Request(std::vector<DataChannel*> *dataChannels)
   : m_dataChannels(dataChannels) {
   m_birthTime=std::chrono::high_resolution_clock::now();
}

void Request::setDelay(unsigned int delay) {
   s_delay=delay;
}

void Request::setMaxAge(unsigned int maxAge) {
  s_maxAge=maxAge; 
}


bool Request::timeToExecute() {
   return checkAge(s_delay);
}

bool Request::checkAge()
{
   return checkAge(s_maxAge);
}

bool Request::checkAge(unsigned int ageLimit)
{
   // Definition of the return codes:
   // false = request is not yet too old
   // true = request is too old

   if (ageLimit==0) {
      return true;
   }

   auto elapsed=std::chrono::high_resolution_clock::now()-m_birthTime;
   unsigned int myAge=std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
   DEBUG_TEXT(DFDB_ROSCORE, 20, "Request::checkAge: age is "<<myAge << ", limit is " << ageLimit);
   if (myAge > ageLimit) {
      return(true);
   }
   else {
      return(false);
   }
}
