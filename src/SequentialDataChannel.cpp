//$Id$
/************************************************************************/
/*									*/
/* File             : SequentialDataChannel.cpp				*/
/*									*/
/* Authors          : B.Gorini, M.Joos, J.Petersen CERN			*/
/*									*/
/********* C 2004 - ROS/RCD  ********************************************/

#include <sys/types.h>                               // for u_int
#include <memory>                                    // for allocator
#include <ostream>                                   // for operator<<, basi...
#include <stdint.h> 

#include "DFSubSystemItem/Config.h"                  // for Config
#include "DFThreads/DFCountedPointer.h"              // for DFCountedPointer
#include "DFThreads/DFFastMutex.h"                   // for DFFastMutex

#include "DFDebug/DFDebug.h"
#include "ROSEventFragment/RODFragment.h"
#include "ROSEventFragment/ROBFragment.h"

#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSCore/SequentialInputHandler.h"
#include "ROSCore/SequentialDataChannel.h"
#include "ROSCore/PCMemoryDataChannel.h"             // for PCMemoryDataChannel
#include "ROSCore/CoreException.h"

#include "ROSEventFragment/EventFragment.h"          // for EventFragment
#include "ROSEventInputManager/EventDescriptor.h"    // for evDesc_t
#include "ROSEventInputManager/EventInputManager.h"  // for EventInputManager
#include "ROSInfo/SequentialDataChannelInfo.h"       // for SequentialDataCh...
#include "ROSMemoryPool/MemoryPage.h"                // for MemoryPage
#include "ROSMemoryPool/MemoryPool.h"                // for MemoryPool
#include "ers/ers.h"                                 // for warning

class ISInfo;

using namespace ROS;

/**************************************************************************************************/
SequentialDataChannel::SequentialDataChannel(unsigned int id,
					     unsigned int configId,
					     unsigned long rolPhysicalAddress,
					     bool usePolling,
					     DFCountedPointer<Config> configuration,
					     SequentialDataChannelInfo* info) :
  PCMemoryDataChannel(id, configId, rolPhysicalAddress, configuration, info)
/**************************************************************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialDataChannel::constructor: Entered");

  if (info == 0) {				// if default or called explicitly by user with zero
    CREATE_ROS_EXCEPTION(ex1, CoreException, INFOZERO, "");
    throw(ex1);
  }

  m_statistics = info;
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialDataChannel::constructor: m_statistics @ " << m_statistics);

  m_sequentialInputHandler = SequentialInputHandler::Instance();

  m_sequentialInputHandler->setup(configuration);

  m_sequentialInputHandler->registerChannel(this);
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialDataChannel::constructor: register channel " << HEX(this) << " in SequentialInputHandler");

  char mutexName[]="REQUESTMUTEX";
  m_fastAllocateMutex = DFFastMutex::Create(mutexName);
  m_dataInterruptMutex = DFFastMutex::Create();               //No name because this mutex is only used in this class

  m_usePolling = usePolling;
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialDataChannel::constructor: usePolling =  " << m_usePolling);

  m_fragType = configuration->getInt("inputFragmentType");
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialDataChannel::constructor: fragment type =  " << m_fragType);

  m_rolPhysicalAddress = rolPhysicalAddress;
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialDataChannel::constructor: rolPhysicalAddress =  " << m_rolPhysicalAddress);

  // IS info
  m_statistics->lastL1IdInput = c_initL1id;
  m_statistics->wrongMarker = 0;
  m_statistics->wrongSize = 0;
  m_statistics->multipleFragments = 0;
  m_statistics->pollRODs = 0;
  m_statistics->pollIRFlags = 0;
  m_statistics->noSpace = 0;

  m_dataInterruptFlag = 0;	
  m_fragmentsScheduled = 0;

  m_rod_L1id_prev = c_initL1id;

  m_eventInputManager = getEIM();
  m_memoryPool = m_eventInputManager->getMemoryPool();
  m_robPageSize = getPageSize();  //This is a method of the PCMemoryDataChannel
  DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialDataChannel::setup: memoryPoolPageSize = " << m_robPageSize);

}

/********************************************************************/
SequentialDataChannel::~SequentialDataChannel()
/********************************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialDataChannel::destructor: Entered");

  m_fastAllocateMutex->destroy();

  m_dataInterruptMutex->destroy();

  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialDataChannel::destructor: unregister channel " << HEX(this) << " in SequentialInputHandler");
  m_sequentialInputHandler->unregisterChannel(this);

  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialDataChannel::destructor: Exited");
}


//Called by IOManager.cpp
/****************************************************************/
ISInfo *SequentialDataChannel::getISInfo()
/****************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialDataChannel::getIsInfo: called");
  return m_statistics;
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialDataChannel::getIsInfo: done");
}


//Called by SequentialInputHandler::run
/************************************************************/
bool SequentialDataChannel::readyToReadout(void)
/************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialDataChannel::readyToReadout: # fragments ready = "  << m_fragmentsScheduled);

  bool retValue = false;

  if (m_fragmentsScheduled > 0) {
    m_statistics->multipleFragments++;
    retValue = true;			// more fragments  && space available
  }
  else {				// check if space && new fragments
    // leave mutex unprotected: READ ONLY (thread safe ..)
    if (m_memoryPool->isPageAvailableNonReserved()) {
      DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialDataChannel::readyToReadout: space available ");
      DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialDataChannel::readyToReadout: # fragmentsScheduled " << m_fragmentsScheduled);
      if (m_usePolling) {
        m_fragmentsScheduled+=poll();	// user returns result of polling, over VMEbus (or equivalent ..)
        DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialDataChannel::readyToReadout: # fragments ready = " << m_fragmentsScheduled);
        if (m_fragmentsScheduled > 0) {
          m_statistics->pollRODs++;
          retValue = true;
        }
      }
      else {
        if (pollDataInterrupt()) {
          m_fragmentsScheduled++;
          DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialDataChannel::readyToReadout interrupt: # fragments ready = " << m_fragmentsScheduled);
          m_statistics->pollIRFlags++;
          retValue = true;
        }
      }
    }
    else
      m_statistics->noSpace++;
  }
  return retValue;
}

//Called by SequentialInputHandler::prepareForRun
/*****************************************************************************/
void SequentialDataChannel::clear(void)
/*****************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialDataChannel::clearChannel: entered");
  
  m_rod_L1id_prev = c_initL1id;
  m_dataInterruptFlag = 0;
  m_fragmentsScheduled = 0;

  m_eventInputManager->reset();
}


//This method has to be called by the specific user code if it is based on interrupts
/*****************************************************************************/
void SequentialDataChannel::signalFragmentAvailable(void)
/*****************************************************************************/
{
  m_dataInterruptMutex->lock();    //We are competing with pollDataInterrupt below which is executed as part of the Input Handler thread
  m_dataInterruptFlag++;           
  m_dataInterruptMutex->unlock();
}

//Called by readyToReadout(void). Logically equivalent to the poll() in user space
/*******************************************************/
bool SequentialDataChannel::pollDataInterrupt(void)
/*******************************************************/
{
  bool retValue = false;

  m_dataInterruptMutex->lock();
  if (m_dataInterruptFlag > 0) {
    m_dataInterruptFlag = 0;
    retValue = true;
  }
  m_dataInterruptMutex->unlock();

  return retValue;
}

//Called by SequentialInputHandler::run
/*****************************************************/
unsigned int SequentialDataChannel::inputFragment(void)
/*****************************************************/
{
  unsigned int nBytes;

  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialDataChannel::inputFragment: Inputting data via ROL ");

  m_fastAllocateMutex->lock();

  //Get a memory page to store the data
  MemoryPage* mem_page = m_memoryPool->getPage();  // getPage throws an exception if no pages available
  DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialDataChannel::inputFragment: mem_page = " << mem_page <<
                             "  pageNo = " << mem_page->pageNumber());

  //Reserve the full page size (at this point we do not know how many bytes will be transferred)
  unsigned int* buffer = (unsigned int *)(mem_page->reserve(m_robPageSize));
  if (buffer == 0) {
    CREATE_ROS_EXCEPTION(ex1,CoreException,ALL_INVALIDPAGE,
                         "MemoryPool returned an already locked page or a page with wrong size. Capacity of the page = "
                         << mem_page->capacity() << " instead of " << m_robPageSize);
    m_fastAllocateMutex->unlock();
    throw(ex1);
  }

  m_fastAllocateMutex->unlock();

  if (m_fragType == 0) 
  {   // ROD fragment : the default
    //Now offset the buffer pointer to leave space for the ROB header that will be filled later
    buffer = (unsigned int *)(((char *)buffer) + sizeof(ROBFragment::ROBHeader));
    unsigned long pci_addr = mem_page->physicalAddress() + sizeof(ROBFragment::ROBHeader);

    //Receive fragment data
    nBytes = this->getNextFragment(buffer, m_robPageSize - sizeof(ROBFragment::ROBHeader), pci_addr);
    DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialDataChannel::inputFragment: L1id in buffer = " <<  buffer[EventFragment::rod_l1id_offset]);

    m_fastAllocateMutex->lock();
    //Now release the unused memory from the memory page
    mem_page->release(m_robPageSize-sizeof(ROBFragment::ROBHeader) - nBytes);
    //Check the marker of the ROD fragment
    unsigned int *markerptr = (unsigned int *) ((intptr_t)mem_page->address() + sizeof(ROBFragment::ROBHeader));
    if (*markerptr != 0xee1234ee)  
//    if (*markerptr != 0x1234)  // debug hook
    {
	m_statistics->wrongMarker++;
        CREATE_ROS_EXCEPTION(e,CoreException,ROD_MARKER,"\nROD Marker = " << HEX(*markerptr) << ", ROL = " << m_rolPhysicalAddress);
        ers::warning(e);
    }

    m_rod_L1id_prev = buffer[EventFragment::rod_l1id_offset];

    //Compute address of the ROD trailer
    unsigned int *trailerptr = (unsigned int *) ((intptr_t)markerptr + nBytes - sizeof(RODFragment::RODTrailer));
    DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialDataChannel::inputFragment: markerptr = " << markerptr << "   trailerptr = " << trailerptr);

    //Check if the number of words received is consistent with the information in the ROD TRAILER
    u_int rodfragsize = sizeof(RODFragment::RODHeader) + sizeof(RODFragment::RODTrailer) + trailerptr[0] * 4 + trailerptr[1] * 4;
    DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialDataChannel::inputFragment:  Expected fragment size = " << rodfragsize << "   real fragment size = " << nBytes);
    if (rodfragsize != nBytes)
//      if (rodfragsize != 1)     // debug hook
    {
      DEBUG_TEXT(DFDB_ROSFM, 5, "SequentialDataChannel::inputFragment: Size mismatch:  Expected fragment size = " << rodfragsize << "   real fragment size = " << nBytes);
      m_statistics->wrongSize++;
      CREATE_ROS_EXCEPTION(e, CoreException, ALL_ILLSIZE2, "\n L1ID = " << m_rod_L1id_prev << ", received  size = " << nBytes << ", expected size = " << rodfragsize);
      ers::warning(e);
    }

  }	// ROD Fragment
  else 
  { // Full fragment :  Event Building
    nBytes = this->getNextFragment(buffer, m_robPageSize);

    m_fastAllocateMutex->lock();
    //Now release the unused memory from the memory page
    mem_page->release(m_robPageSize - nBytes);
    
    //Check if the data block starts with a Full fragment
    unsigned int *markerptr = (unsigned int *) ((intptr_t)mem_page->address());
    DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialDataChannel::FULL header marker is " << HEX(*markerptr));				 
    if (*markerptr != EventFragment::s_fullMarker)
    {
      m_statistics->wrongMarker++;
      CREATE_ROS_EXCEPTION(e, CoreException, FULL_MARKER, "\nReceived Full Marker = " << HEX(*markerptr) << ", ROL = " << m_rolPhysicalAddress);
      ers::warning(e);
    }

    m_rod_L1id_prev = buffer[EventFragment::full_l1id_offset];
    DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialDataChannel::inputFragment: m_rod_L1id_prev = " << m_rod_L1id_prev);				 
  }

  //Get the event descriptor
  evDesc_t *ed = m_eventInputManager->getEventDescriptor(mem_page);

  //Initialize the event descriptor
  ed->L1id = m_rod_L1id_prev;
  ed->RODFragmentSize = nBytes / 4;  //Unit = words
  DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialDataChannel::inputFragment: L1id = " << ed->L1id << " FragmentSize = " << ed->RODFragmentSize);

  //Insert the event into the Event Input Manager
  m_eventInputManager->createEvent(ed);

  m_fastAllocateMutex->unlock();

  m_fragmentsScheduled--;	// fragment done
  DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialDataChannel::inputFragment: # fragments scheduled = " << m_fragmentsScheduled);

  return(ed->L1id);

}

// called by IOManager.cpp
/*****************************************/
void SequentialDataChannel::probe()
/*****************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialDataChannel::probe: called");

  PCMemoryDataChannel::probe();

  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialDataChannel::probe: done");
}

// called by IOManager.cpp
/****************************************/
void SequentialDataChannel::clearInfo()
/****************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 5, "SequentialDataChannel::clearInfo: called");

  PCMemoryDataChannel::clearInfo();

  m_statistics->lastL1IdInput = c_initL1id;
  m_statistics->wrongMarker = 0;
  m_statistics->wrongSize = 0;
  m_statistics->multipleFragments = 0;
  m_statistics->pollRODs = 0;
  m_statistics->pollIRFlags = 0;
  m_statistics->noSpace = 0;

  m_statistics->lastL1IdInput = m_rod_L1id_prev; 

  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialDataChannel::clearInfo: done");
}
