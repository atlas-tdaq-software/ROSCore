//$Id$
/************************************************************************/
/*                                                                      */
/* File             : SequentialInputHandler.cpp                        */
/*                                                                      */
/* Authors          : B.Gorini, M.Joos, J.Petersen CERN                 */
/*                                                                      */
/* a singleton class that contains					*/
/* methods for handling the sequential input of ROD fragments           */
/* from a number of links						*/
/* in RCD: via VMEbus							*/
/* These methods are used to input fragments driven by either polling   */
/* or interrupts							*/
/*									*/
/********* C 2008 - ROS/RCD *********************************************/

#include <sys/types.h>                           // for u_int
#include <algorithm>                             // for find
#include <iostream>                              // for endl, basic_ostream:...
#include <map>                                   // for map, operator==, _Rb...
#include <string>                                // for allocator, string
#include <utility>                               // for pair
#include <vector>                                // for vector<>::iterator
#include <iomanip>

#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSCore/SequentialInputHandler.h"
#include "ROSCore/CoreException.h"  
#include "DFSubSystemItem/Config.h"              // for Config
#include "DFThreads/DFCountedPointer.h"          // for DFCountedPointer
#include "DFThreads/DFFastMutex.h"               // for DFFastMutex
#include "DFThreads/DFThread.h"                  // for DFThread, DFThread::...
#include "ROSCore/ROSHandler.h"                  // for ROSHandler
#include "ROSCore/SequentialDataChannel.h"       // for SequentialDataChannel
#include "ROSCore/TriggerInputQueue.h"           // for TriggerInputQueue
#include "ROSInfo/SequentialInputHandlerInfo.h"  // for SequentialInputHandl...
class ISInfo;
namespace daq { namespace rc { class TransitionCmd; } }

using namespace ROS;

// Static variables
DFFastMutex *SequentialInputHandler::s_mutex = DFFastMutex::Create();
SequentialInputHandler *SequentialInputHandler::s_uniqueInstance = 0;


// Singleton pattern: only one instance is created
/********************************************************/
SequentialInputHandler *SequentialInputHandler::Instance() 
/********************************************************/
{
  s_mutex->lock();
  if (s_uniqueInstance == 0) {
    s_uniqueInstance = new SequentialInputHandler;
  }  
  s_mutex->unlock();
  
  return s_uniqueInstance;
}

/**************************************************************************************/
SequentialInputHandler::SequentialInputHandler(void) : ROSHandler("SequentialInputHandler"), m_threadStarted(false)
/**************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::constructor: Entered");
  m_numberOfDataChannels = 0;
  m_statistics.l1idsToQueue = 0;
  m_statistics.yield = 0;

  m_runActive=false;

  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::constructor: Done");
}



/**************************************************************************************/
void SequentialInputHandler::setup(DFCountedPointer<Config> configuration)
/**************************************************************************************/
{
   DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::setup: Entered");

   m_triggerQueueFlag = configuration->getBool("triggerQueue");

   m_singleFragmentMode = configuration->getBool("SingleFragmentMode");
   DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialInputHandler::setup: singleFragmentMode = " << m_singleFragmentMode);

   m_triggerQueueSize = configuration->getInt("triggerQueueSize");
   DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialInputHandler::setup: triggerQueueSize = " << m_triggerQueueSize);

}


//Called by: IOManager
/***********************************************/
SequentialInputHandler::~SequentialInputHandler() noexcept
/***********************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::destructor: Entered");
  s_uniqueInstance=0;
}

//Called by: IOManager::configure
/******************************************/
void SequentialInputHandler::configure(const daq::rc::TransitionCmd& /*s*/)
/******************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::configure: Entered");

  // if DDT, create trigger Q DDT even if sequential IH is never started i.e. # data channels = 0.
  // If DDT (ROBIN) UserActionScheduler, then Q is created
  // in parallel with the one from the DDT USerActionScheduler
  if (m_triggerQueueFlag) {
     m_inputToTriggerQueue = TriggerInputQueue::instance();
     m_inputToTriggerQueue->setSize(m_triggerQueueSize);
     DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialInputHandler::configure: , Q = " << m_inputToTriggerQueue << " size = " <<  m_triggerQueueSize);
  }

  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::configure: Done");
}

//Called by: IOManager::unconfigure
/********************************************/
void SequentialInputHandler::unconfigure(const daq::rc::TransitionCmd& /*s*/)
/********************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::unconfigure: Entered");

  if (m_triggerQueueFlag) {
    m_inputToTriggerQueue->destroy();
  }
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::unconfigure: Done");
}


//Called by: IOManager::prepareForRun
/**********************************************/
void SequentialInputHandler::prepareForRun(const daq::rc::TransitionCmd& /*s*/)
/**********************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::prepareForRun: # data channels = " << m_numberOfDataChannels);

  m_statistics.l1idsToQueue = 0;
  m_statistics.yield = 0;
  m_threadStarted=false;
 
  if (m_numberOfDataChannels>0) {
    for(int chan = 0; chan < m_numberOfDataChannels; chan++) {
      m_dataChannels[chan]->clear();
    }

    m_outStandingEvents.clear();

    m_mapSizeHist.reserve(c_mapSizeBins);
    for (int i = 0; i < c_mapSizeBins; i++) {
      m_mapSizeHist.push_back(0);
    }

    DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::prepareForRun: going to start thread ");

    m_runActive=true;
    startExecution();
    m_threadStarted=true;
    DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::prepareForRun: input started");
  }
}


//Called by: IOManager::stopEB
/***************************************/
void SequentialInputHandler::stopGathering(const daq::rc::TransitionCmd& /*s*/)
/***************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::stopGathering: Entered");

  m_runActive=false;
  if (m_triggerQueueFlag) {
     m_inputToTriggerQueue->abort();
  }

  //Stop the thread
  if (m_threadStarted) {
    DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::stopGathering: stopping input thread");
    stopExecution();
    waitForCondition(DFThread::TERMINATED, 5);  //5 second time out to kill the thread
    m_threadStarted = false;
    std::cout << " SequentialInputHandler::stopGathering: input thread stopped" << std::endl;
  }
  
  if (m_triggerQueueFlag) {
     m_inputToTriggerQueue->reset();
  }

  if (m_numberOfDataChannels>0) {
    std::cout << std::endl;
    std::cout << " histogram of the size of the map for outstanding events" << std::endl;
    int nMAPgroup = 5;                                     // just for formatting
    for (int i=0; i<(c_mapSizeBins/nMAPgroup); i++) {
      int bin_contents = 0;
      for (int j=0; j<nMAPgroup; j++) {
        bin_contents+= m_mapSizeHist[nMAPgroup*i+j];
      }
      std::cout << std::setw(7) << std::dec << nMAPgroup*i << std::setw(12) << bin_contents << std::endl;
    }
    std::cout << std::endl;

    m_mapSizeHist.clear();	
  }

  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::stopGathering: Done");
}


//Called by: IOManager::probe
/*****************************************/
ISInfo* SequentialInputHandler::getISInfo()
/*****************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::getIsInfo: called");
  return &m_statistics;
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::getIsInfo: done");
}

 
//Called by: IOManager::prepareForRun
/**************************************/
void SequentialInputHandler::clearInfo()
/**************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::clearInfo  Function entered");
  m_statistics.l1idsToQueue = 0;
  m_statistics.yield = 0;
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::clearInfo  Function done");
}


//Called by run (see below)  
/****************************************************************************/
void SequentialInputHandler::pushL1id(unsigned int L1id, unsigned int channel)
/****************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::pushL1id: Entered");
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::pushL1id: L1id = " << L1id << "channel = " << channel);

  if (m_triggerQueueFlag)   // only in self-triggering mode  //Today no other triggers are used
  {
    bool complete = areFragmentsThere(L1id);
    DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialInputHandler::run complete = " << complete);

    if (complete)
    {
      DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialInputHandler::run: push L1id " << L1id << " onto Q");
      m_inputToTriggerQueue->push(L1id);
      if (m_singleFragmentMode) {
        DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialInputHandler::run: push channel " << channel << " onto Q");
        m_inputToTriggerQueue->push(channel);
      }
      m_statistics.l1idsToQueue++;
    }
  }
}


//Called by pushL1id (see above) 
/***************************************************************/
bool SequentialInputHandler::areFragmentsThere(unsigned int l1id)
/***************************************************************/
{
  std::map <unsigned int, int>::iterator it; 

  DEBUG_TEXT(DFDB_ROSFM, 10, "SequentialInputHandler::areFragmentsThere. On entry:  l1id =  "
                            << l1id << " Size of outstanding map = " << m_outStandingEvents.size());

  //If there is a single data channel, every new incoming fragment shall generate a trigger
  if (m_numberOfDataChannels==1)
    return true;

  //IF in singleFragment mode every new incoming fragment shall generate a trigger
  if (m_singleFragmentMode)
    return true;

  // update map size histogram
  int mapsize = m_outStandingEvents.size();
  if (mapsize < (c_mapSizeBins - 1)) {
    m_mapSizeHist[mapsize]++;
  }
  else {                            // overflow
    m_mapSizeHist[c_mapSizeBins - 1]++;
  }

  it = m_outStandingEvents.find(l1id);
  if (it == m_outStandingEvents.end()) {	// add l1id to map, suggest at the end
    m_outStandingEvents.insert(m_outStandingEvents.end(), std::pair<unsigned int, int>(l1id, 1)); 
    DEBUG_TEXT(DFDB_ROSFM, 10, "areFragmentsThere: added  l1id " << l1id << " to map");
    return false;
  }
  else {	// l1id found
    DEBUG_TEXT(DFDB_ROSFM, 10, "areFragmentsThere: found  l1id " << l1id << " # received = " << it->second);
    if (it->second == (m_numberOfDataChannels-1)) {	// complete 
      m_outStandingEvents.erase(it);
      DEBUG_TEXT(DFDB_ROSFM, 10, "areFragmentsThere: l1id complete " << " l1id erased and map size = " << m_outStandingEvents.size());
      return true;
    }
    else {	// incomplete
      (it->second)++;
      return false;
    }
  }
}



//Called by the constructor of SequentialDataChannel
/**************************************************************************/
void SequentialInputHandler::registerChannel(SequentialDataChannel *channel)
/**************************************************************************/
{
  m_dataChannels.push_back(channel);
  m_numberOfDataChannels++;
  m_rod_L1id_prev.push_back(0);

  DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialInputHandler::registerChannel: channel = " << channel << " is added to channel vector");

}

//Called by the destructor of SequentialDataChannel
/****************************************************************************/
void SequentialInputHandler::unregisterChannel(SequentialDataChannel *channel)
/****************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialInputHandler::unregisterChannel: entered");

  std::vector<SequentialDataChannel *>::iterator it;

  it = find(m_dataChannels.begin(), m_dataChannels.end(), channel);
  if (it != m_dataChannels.end()) {
    m_dataChannels.erase(it);
    m_numberOfDataChannels--;
  }
  else {
    CREATE_ROS_EXCEPTION(e, CoreException, SEQ_UNREGISTERNOTFOUND, "");
    throw e;
  }

  DEBUG_TEXT(DFDB_ROSFM, 20, "SequentialInputHandler::unregisterChannel: channel = " <<
                             channel << " is removed from channel vector. Number of data channels is now "
                             << m_numberOfDataChannels);

  m_rod_L1id_prev.pop_back();

}

/************************************/
void SequentialInputHandler::run(void)
/************************************/
{

  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::run: Entered");

  bool busy;

  while(m_runActive) {
    busy = true;
    while(busy && m_runActive) {
      busy = false;
      for(int chan = 0; chan < m_numberOfDataChannels; chan++) {
         cancellationPoint();
        bool isReady = m_dataChannels[chan]->readyToReadout();
        if (isReady) {
          busy = true;
          DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::run: Inputting data via ROL " << chan);
          unsigned int FragmentL1id =  m_dataChannels[chan]->inputFragment();

          pushL1id(FragmentL1id,chan);
        }
      }
    }

    // We have scanned all ROLs but none has data or all pages are used in the memory pool of the
    // respective ROL. There is nothing to do. Therefore:
    DFThread::yieldOrCancel();
    m_statistics.yield++;
  }

}

//Called by DFThreads when it stops the run()-thread 
/************************************/
void SequentialInputHandler::cleanup()
/************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SequentialInputHandler::cleanup: Empty method");
}
