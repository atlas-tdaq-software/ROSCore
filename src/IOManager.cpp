/// $Id$
// //////////////////////////////////////////////////////////////////////
//  IOManager the core intelligence of the ROS
//
//  Author:  G.J.Crone
//
//  $Log$
//  Revision 1.165  2008/11/21 14:52:09  gcrone
//  Tidy up the loop over DataOut creation.
//
//  Revision 1.164  2008/11/11 17:52:23  gcrone
//  Use new ROSHandler interface
//
//
//
// //////////////////////////////////////////////////////////////////////

#include <atomic>

#include <chrono>
#include <iostream>
#include <list>
#include <memory>
#include <set>
#include <string>
#include <thread>
#include <utility>
#include <vector>

#include <tbb/concurrent_queue.h>

#include "rcc_time_stamp/tstamp.h"

#include "ROSCore/IOManager.h"
#include "ROSCore/Request.h"
#include "ROSCore/DataRequest.h"
#include "ROSCore/FragmentRequest.h"
#include "ROSCore/FragmentBuilder.h"
#include "ROSCore/RequestHandler.h"
#include "ROSCore/CoreException.h"
#include "ROSCore/DataOut.h"
#include "ROSCore/TriggerIn.h"
#include "ROSCore/ROSHandler.h"
#include "ROSCore/DataChannel.h"
#include "ROSCore/ReadoutModule.h"
#include "ROSCore/IOManagerConfig.h"               // for IOManagerConfig
#include "ROSCore/ScheduledUserAction.h"           // for ScheduledUserAction

#include "ROSUtilities/PluginFactory.h"

#include "ROSBufferManagement/Buffer.h"
#include "ROSMemoryPool/MemoryPool.h"
#include "ROSMemoryPool/MemoryPool_malloc.h"
#include "ROSMemoryPool/MemoryPool_CMEM.h"
#include "ROSMemoryPool/MemoryPage.h"              // for MemoryPage

#include "DFSubSystemItem/Config.h"                // for Config, Config::T_...
#include "DFSubSystemItem/SSIConfiguration.h"      // for SSIConfiguration
#include "DFSubSystemItem/ConfigException.h"
#include "DFDebug/DFDebug.h"
#include "DFDebug/GlobalDebugSettings.h"           // for GlobalDebugSettings

#include "DFThreads/DFCountedPointer.h"            // for DFCountedPointer
#include "DFThreads/DFFastMutex.h"                 // for DFFastMutex
#include "DFThreads/DFThread.h"                    // for DFThread, DFThread...

#include "is/exceptions.h"

#include "ROSUtilities/ROSErrorReporting.h"

#include "DF_IS_Info/rosNamed.h"

#include "ROSInfo/CoreInfo.h"


#include "RunControl/FSM/FSMCommands.h"
#include "RunControl/Common/Controllable.h"        // for Controllable, FSM_...
#include "RunControl/Common/RunControlCommands.h"  // for UserCmd, SubTransi...

#include "ers/ers.h"                               // for warning, fatal

#include "ipc/partition.h"                         // for IPCPartition
#include "is/info.h"                               // for ISInfo (ptr only)
#include "is/infodictionary.h"                     // for ISInfoDictionary
#include "monsvc/ConfigurationRules.h"             // for ConfigurationRule
#include "monsvc/PublishingController.h"           // for PublishingController

class Configuration;
namespace daq { namespace core { class Partition; } }

using namespace ROS;


void iomExceptionHandler(std::exception& e) {
   std::cout << "Caught an unhandled exception \n";
   ENCAPSULATE_ROS_EXCEPTION(e2, CoreException, UNCLASSIFIED, e, "thread stopped");
   ers::fatal(e2);
}

IOManager::ScheduledMonitoringAction::ScheduledMonitoringAction(IOManager* iom, std::string name, unsigned int interval) :
   ScheduledUserAction(interval), m_iom(iom), m_name(name) {
}

void IOManager::ScheduledMonitoringAction::reactTo() {
   m_iom->monitoringAction(m_name);
}

void IOManager::monitoringAction(const std::string& name)
{
   DEBUG_TEXT(DFDB_ROSCORE, 10 ,"IOManager::MonitoringAction() name: " << name); 

   if (name=="IOManagerInternal") {
      coreMonitor();
   }
   else if (m_userCommandPageSize != 0) {
      MemoryPage* memPage=m_userCommandPool->getPage();
      unsigned long physicalAddress = memPage->physicalAddress();
      //Reserve the full page size (at this point we do not know how many bytes will be transferred)
      unsigned char* dataPtr=(unsigned char*)(memPage->reserve(m_userCommandPageSize));
      if (dataPtr == 0) {
         std::cout << "MemoryPool returned an already locked page or a page with wrong size: "
                   << "the capacity of the page appears to be " << memPage->capacity()
                   << " instead of " << m_userCommandPageSize << std::endl ;
      }
      unsigned int freeSize=m_userCommandPageSize;

      // loop over readout modules 
      for (std::vector<ReadoutModule*>::iterator readoutModule =  m_readoutModules.begin();
           readoutModule != m_readoutModules.end(); readoutModule++) {
         unsigned int bytesUsed=
            (*readoutModule)->getUserCommandStatistics(name,
                                                       dataPtr, freeSize,
                                                       physicalAddress);
         dataPtr+=bytesUsed;
         freeSize-=bytesUsed;
         physicalAddress+=bytesUsed;
      }
      memPage->release(freeSize);
      Buffer* buffer=new Buffer(memPage);
      if (freeSize != m_userCommandPageSize) {
         // Send this page to monitoring stream
         DataOut* sampler=DataOut::sampled();
         if (sampler!=0) {
            sampler->sendData(buffer, m_userCommandDestination);
         }
      }
      delete buffer;
      memPage->free();
   }
}



/***********************************************************/
IOManager::IOManager(std::string& name) : daq::rc::Controllable(),
                                          m_name(name),
                                          m_publishingController(0)
/***********************************************************/
{
   std::cout<< "IOManager constructor - name " << m_name << std::endl;

   // Creation of the queue for requests is postponed to the load transaction
   m_requestQueue=0;

   // similarly for configuration
   m_config=0;

   m_pool=0;
   m_userCommandPool=0;
   m_userCommandPageSize=0;
   m_userCommandDestination=0;

   m_busyCount=0;
   m_queueCount=0;
   m_maxBusyCounts=0;
   m_busyIndex=0;
   m_busyPerSecond=0;

   m_requestsQueued=0;
   m_requestsDequeued=0;
   m_requestsRequeued=0;
   m_numberOfRequestsLast=0;
   m_requestTimeOut=0;
   m_communicationTimeout=0;
   m_releaseDelay=0;

   m_lastHandled=0;
   m_lastPopEmpty=0;
   m_lastPushEmpty=0;

   char requestMutexName[]="REQUESTMUTEX";
   m_requestMutex = DFFastMutex::Create(requestMutexName) ;

   m_trace = false;
   m_interactiveMode = false;

   // Now we can override the value set by DFErrorCatcher
   DFThread::set_exception_handler(iomExceptionHandler);

   m_runActive=false;
   m_configured=false;
   m_preConfigured=false;
}


/*************************/
IOManager::~IOManager(void) noexcept
/*************************/
{
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::destructor");

   if (m_runActive) {
      std::cout << "IOManager::~IOManager() Run active, issuing stopGathering\n";
      daq::rc::TransitionCmd command(daq::rc::FSM_COMMAND::STOPGATHERING);
      stopGathering(command);
      disconnect(command);
      unconfigure(command);
   }


   for (std::vector<TriggerIn*>::reverse_iterator iter=m_triggerIn.rbegin() ; iter!=m_triggerIn.rend(); iter++) {
      (*iter)->stopExecution();
   }
   if (m_requestQueue) {
      m_requestQueue->abort();
      //Scheduling request handlers for termination
      for (std::vector<RequestHandler*>::reverse_iterator 
              requestHandler=m_requestHandlers.rbegin();
           requestHandler!=m_requestHandlers.rend();
           requestHandler++) {
         (*requestHandler)->stop();
      }
   }

   // stop Sequential Handler & Interrupt Catcher after Modules
   for (ROSHandler::iterator iter=ROSHandler::begin(); iter!=ROSHandler::end(); iter++) {
      (*iter)->stopExecution();
   }


   DEBUG_TEXT(DFDB_ROSCORE, 15, "IOManager::destructor cleaning up ReadoutModule");
   while (!m_readoutModules.empty()) {
      ReadoutModule* rm = m_readoutModules.back();
      m_readoutModules.pop_back();
      delete rm;
   }

   DEBUG_TEXT(DFDB_ROSCORE, 15, "IOManager::destructor cleaning up triggerIn");
   while (!m_triggerIn.empty()) {
      TriggerIn* tin=m_triggerIn.back();
      m_triggerIn.pop_back();
      delete tin;
   }


   DEBUG_TEXT(DFDB_ROSCORE, 15, "IOManager::destructor cleaning up dataOut");
   while (!m_dataOut.empty()) {
      DataOut* dataOut=m_dataOut.back();
//      dataOut->unconfigure();
      m_dataOut.pop_back();
      delete dataOut;
   }

   if(m_requestQueue) {
      delete m_requestQueue;
   }

   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::destructor complete");
}

/*********************************/
std::string IOManager::getName() {
/*********************************/
   return m_name;
}

/*********************************************/
void IOManager::queueRequest(Request* request) {
/*********************************************/
   if (m_runActive) {
      m_requestQueue->push(request);
      m_requestsQueued++;
   }
   else {
      // Create an exception and issue warning?
      ERS_LOG("Ignoring Request while run not active");
   }
}

/********************************************************/
void IOManager::setConfigPlugin(std::string& pluginName) {
/********************************************************/
   m_configPluginName=pluginName;

   m_configFactory.load(m_configPluginName);
   std::cout << "IOManager::configure creating config plugin for "
             << m_name <<std::endl;
   m_config = m_configFactory.create(m_configPluginName.c_str(),
                                     (void*) (m_name.c_str()));
}


/************************************************/
void IOManager::setInteractive(bool interactive) {
/************************************************/
   m_interactiveMode=interactive;
}


/********************************************************/
std::vector<ReadoutModule*>* IOManager::readoutModules() {
/********************************************************/
   return &m_readoutModules;
}

/******************************/
void IOManager::preconfigure(){
/******************************/
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::preconfigure");

   if (m_preConfigured) {
      DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::preconfigure already done, skipping");
      return;
   }

   SSIConfiguration * myConf = m_config->update();
   if(myConf == 0) {
      CREATE_ROS_EXCEPTION(tException, CoreException, UNDEFINED_CONFIGURATION, "");
      throw (tException);
   }
   m_coreParameters = myConf->getConfig("Core")[0];

   Configuration* confDB=m_config->getConfiguration();
   const daq::core::Partition* partition=m_config->getPartition();

   if (confDB==0) {
      confDB=m_coreParameters->getPointer<Configuration>("configurationDB");
   }
   if (partition==0) {
      partition=m_coreParameters->getPointer<daq::core::Partition>("partitionPointer");
   }

   //Set the debug levels
   if (!m_trace) {
     DF::GlobalDebugSettings::setup(m_coreParameters->getInt("TraceLevel"),
                                    m_coreParameters->getInt("TracePackage"));
   }

   if (!m_interactiveMode) {
      // Open connection to IS
      std::string partitionName=m_coreParameters->getString("partition");
      m_ipcPartition=IPCPartition(partitionName.c_str());
      std::string serverName=m_coreParameters->getString("ISServerName");
      m_isInfoName=serverName+".ROS."+m_name;

      m_publishingController=new monsvc::PublishingController(m_ipcPartition, m_name);

      int fullStatsInterval=m_coreParameters->getInt("FullStatisticsInterval");
      
      // monsvs does not like intervals set to zero
      // In this case we intend the publication to be disabled
      // hence we simply skip the publication rule definition
      if (fullStatsInterval) {
	std::ostringstream sStream;
	sStream << "Histogramming:.*/=>oh:(" << fullStatsInterval
		<< ",1,Histogramming," << m_name << ")";
	m_publishingController->add_configuration_rule(
	   *monsvc::ConfigurationRule::from(sStream.str()));
      }
   }

   // online precision time measurement for rates
   ts_open(1,TS_DUMMY);
  
   // get the request time out
   m_requestTimeOut = m_coreParameters->getUInt("RequestTimeOut");
  
   m_releaseDelay = m_coreParameters->getInt("ReleaseDelay");

   // get the communication time out
   m_communicationTimeout = m_coreParameters->getInt("communicationTimeout");

   // Create the queue for requests ; 
   m_requestQueue=new tbb::concurrent_bounded_queue<Request*>;

   unsigned int queueSize = m_coreParameters->getInt("QueueSize");
   if (queueSize!=0) {
      m_requestQueue->set_capacity(queueSize);
   }

   // Create the memory pool for buffers
   int npages=m_coreParameters->getInt("memoryPoolNumPages");
   int pageSize=m_coreParameters->getInt("memoryPoolPageSize");
   m_pool=new MemoryPool_malloc(npages,pageSize);

   // Create the memory pool for userCommand statistics
   m_userCommandPageSize=m_coreParameters->getInt("UserCommandPoolPageSize");
   if (m_userCommandPageSize != 0) {
      int poolType=m_coreParameters->getInt("UserCommandPoolType");
      if (poolType!=MemoryPool::ROSMEMORYPOOL_CMEM_BPA && poolType!=MemoryPool::ROSMEMORYPOOL_CMEM_GFP) {
         CREATE_ROS_EXCEPTION(tException, CoreException, ILLEGAL_POOL_TYPE,
                              " for user command statistics");
         throw (tException);
      }
      m_userCommandPool=new MemoryPool_CMEM(2, m_userCommandPageSize, poolType);
   }
   m_userCommandDestination=0;

   // get the ROS logicalId
   m_rosId = m_coreParameters->getInt("rosId");




   // Create any required scheduled monitoring actions
   int numberOfMonitoringActions=m_coreParameters->getInt("numberOfScheduledMonitoringActions");
   for (int action=0; action<numberOfMonitoringActions; action++) {
      std::string actionName=m_coreParameters->getString("monitoringScheduleNames", action);
      unsigned int actionInterval=m_coreParameters->getUInt("monitoringScheduleIntervals", action);

      if (actionName=="IOManagerInternal") {
         m_busyPerSecond=1000/actionInterval;
         m_maxBusyCounts=10*m_busyPerSecond; // storage for 10 seconds
         m_busyCount=new int[m_maxBusyCounts];
         m_queueCount=new int[m_maxBusyCounts];
         m_busyInterval=new float[m_maxBusyCounts];
         std::cout << "busy count interval=" << actionInterval << ",  nbins=" << m_maxBusyCounts << std::endl;
         for (int bin=0;bin<m_maxBusyCounts; bin++) {
            m_busyCount[bin]=0;
            m_queueCount[bin]=0;
            m_busyInterval[bin]=0;
         }
         m_busyTimeLast=std::chrono::system_clock::now();
         m_scheduledMonitoringActions.push_back(new ScheduledMonitoringAction(this, actionName, actionInterval));
      }
      else if (m_userCommandPageSize != 0) {
         m_scheduledMonitoringActions.push_back(new ScheduledMonitoringAction(this, actionName, actionInterval));
      }
      else {
         CREATE_ROS_EXCEPTION(ex, CoreException, NO_POOL, " for monitoring action '" << actionName << "'");
         ers::warning(ex);
      }
   }


   // Create ReadoutModules
   int numberOfReadoutModules =
      m_coreParameters->getInt("numberOfReadoutModules");

   for (int count=0; count<numberOfReadoutModules; count++) {
      std::string readoutModuleType=
         m_coreParameters->getString("readoutModuleType", count);

      m_readoutModuleFactory.load(readoutModuleType);

      DEBUG_TEXT(DFDB_ROSCORE, 10 ,"IOManager::load() instantiating ReadoutModule" << count
                 << " as a " << readoutModuleType); 
      ReadoutModule* module=
               m_readoutModuleFactory.create(readoutModuleType);
      m_readoutModules.push_back(module);
   }

   // Initialize ReadoutModules
   std::vector<DFCountedPointer<Config> > readoutModuleConfig=
      myConf->getConfig("ReadoutModule");


   // Assume that fragment managers are listed in the right order in
   // the Config vector
   if (m_readoutModules.size() == readoutModuleConfig.size()) {
      for (unsigned int index=0; index<m_readoutModules.size(); index++) {
         std::string moduleId;
         try {
            moduleId=readoutModuleConfig[index]->getString("UID");
         }
         catch (ConfigException& error2) {
         }
         m_moduleIds.push_back(moduleId);
         try {
            m_readoutModules[index]->setConfiguration(confDB,partition,
                                                      readoutModuleConfig[index]->getString("UID"));
            m_readoutModules[index]->setup(readoutModuleConfig[index]);
         }
         catch (ConfigException& error) {
            ENCAPSULATE_ROS_EXCEPTION(e1, CoreException, BAD_CONFIGURATION, error,
                                      "Setting up ReadoutModule " << moduleId);
            e1.set_severity(ers::Fatal);
            throw e1;
         }
      }
   }
   else {
      CREATE_ROS_EXCEPTION(tException, CoreException,WRONG_NUMBER_OF_FM_CONFIGS, "");
      throw (tException);
   }


   // Create DataOuts
   int numberOfDataOuts =  m_coreParameters->getInt("numberOfDataOuts");


   if (numberOfDataOuts > 0) {
      std::vector<DFCountedPointer<Config> > dataOutConfig=myConf->getConfig("DataOut");

      for (int ido=0; ido<numberOfDataOuts; ido++) {
         std::string dataOutType = m_coreParameters->getString("dataOutType", ido);
         std::string dataOutClass = m_coreParameters->getString("dataOutClass", ido);
         m_dataOutFactory.load(dataOutClass);
         DataOut::Type doutType;
         if (dataOutType=="MAIN") {
            doutType=DataOut::MAIN;
         }
         else if (dataOutType=="SAMPLED") {
            doutType=DataOut::SAMPLED;
         }
         else if (dataOutType=="DEBUGGING") {
            doutType=DataOut::DEBUGGING;
         }
       
         DataOut* dataOut = m_dataOutFactory.create(dataOutClass, &doutType);
         m_dataOut.push_back(dataOut);
         try {
            dataOutConfig[ido]->set("interactive", m_interactiveMode);
            dataOut->setConfiguration(confDB,partition,
                                      dataOutConfig[ido]->getString("UID"));
            dataOut->setup(dataOutConfig[ido]);
         }
         catch (ConfigException& error) {
            ENCAPSULATE_ROS_EXCEPTION(e1, CoreException, BAD_CONFIGURATION, error,
                                      "configuring " << dataOutType << " DataOut ");
            e1.set_severity(ers::Fatal);
            throw e1;
         }
         if (doutType==DataOut::SAMPLED) {
            //  Set the scaling factor for the Monitoring
            dataOut->setScalingFactor(m_coreParameters->getInt("MonitoringScalingFactor"));
         }
      }
   }


   // Create TriggerIn
   if (m_coreParameters->isKey("triggerInType",0)) {
      vector<string> triggerInType = m_coreParameters->getVector<string>("triggerInType");
      for (vector<string>::iterator iter=triggerInType.begin(); iter!=triggerInType.end(); iter++) {
         m_triggerInFactory.load(*iter);
         m_triggerIn.push_back(m_triggerInFactory.create(*iter));
      }

      // Configure TriggerIn
      m_triggerInConfig = myConf->getConfig("TriggerIn");

      if (m_triggerInConfig.size() == 0) {
         std::cerr << "No triggerIn configuration available\n";
      }
      else {
         for (unsigned int iTrig=0; iTrig<m_triggerIn.size(); iTrig++) {

            try {
               m_triggerIn[iTrig]->setConfiguration(confDB,partition,
                                                    m_triggerInConfig[iTrig]->getString("UID"));
               m_triggerIn[iTrig]->setup(this, m_triggerInConfig[iTrig]);
            }
            catch (ConfigException& error) {
               ENCAPSULATE_ROS_EXCEPTION(e1, CoreException, BAD_CONFIGURATION, error,
                                         "configuring TriggerIn " << iTrig << " (" << triggerInType[iTrig] << ") ");
               e1.set_severity(ers::Fatal);
               throw e1;
            }
         }
      }
   }

   if (m_triggerIn.size() > 0) {
      int nRequestHandlers =
         m_coreParameters->getInt("NumberOfRequestHandlers");
      // Create RequestHandler threads
      for (int thread=0 ;thread<nRequestHandlers; thread++) {
         // Just pass the same queue to all handlers for now
         m_requestHandlers.push_back(new RequestHandler(m_requestQueue));
      }
   }


   delete myConf;

   ts_clock(&m_tsStart);
   m_tsStopLast = m_tsStart;
   m_lastPublish=std::chrono::system_clock::now();
   m_preConfigured=true;
}

//
// methods inherited from RC::Controllable
//

/*********************************************/
void IOManager::configure(const daq::rc::TransitionCmd& command)
/*********************************************/
{
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::configure");


   preconfigure();

   bool threadedConfigure=m_coreParameters->getBool("ThreadedConfigure");
   std::vector<std::thread*> configThreads;
   for (unsigned int index=0; index<m_readoutModules.size(); index++) {
      auto module=m_readoutModules[index];
      std::string moduleId=m_moduleIds[index];

      if (threadedConfigure) {
         DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::configure - creating configuration thread");
         std::thread* moduleThread=
            new std::thread(
               [module,moduleId,command]{try {
                     module->configure(command);
                     //std::cout << "Module " << moduleId << " configured\n";
                  }
                  catch (ConfigException& error) {
                     ENCAPSULATE_ROS_EXCEPTION(re1, CoreException, BAD_CONFIGURATION, error,
                                               "configuring ReadoutModule " << moduleId);
                     re1.set_severity(ers::Fatal);
                     throw re1;
                  }
               });
         configThreads.push_back(moduleThread);
      }
      else {
         try {
            module->configure(command);
         }
         catch (ConfigException& error) {
            ENCAPSULATE_ROS_EXCEPTION(e1, CoreException, BAD_CONFIGURATION, error,
                                      "configuring ReadoutModule " << moduleId);
            e1.set_severity(ers::Fatal);
            throw e1;
         }
      }
   }
   // Wait for the ReadoutModules to finish configuration
   for (auto threadIter : configThreads) {
      DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::configure - waiting for ConfigurationThread");
      threadIter->join();
      delete threadIter;
   }


   for (std::vector<DataOut*>::iterator iter=m_dataOut.begin() ; iter!=m_dataOut.end(); iter++) {
      try {
         (*iter)->configure(command);
      }
      catch (ConfigException& error) {
         ENCAPSULATE_ROS_EXCEPTION(e1, CoreException, BAD_CONFIGURATION, error,
                                   "Connecting DataOut");
         e1.set_severity(ers::Fatal);
         throw e1;
      }
   }

   //Add information about number of channels and their Ids to the
   //TriggerIn Config class 
   //This is needed by the EmulatedTriggerIn
   std::vector<DataChannel *> *channels = DataChannel::channels();
   for (unsigned int iTrig=0; iTrig<m_triggerIn.size(); iTrig++) {
      m_triggerInConfig[iTrig]->set("numberOfChannels",channels->size());
      for (unsigned int loop=0; loop<channels->size(); loop++) {
         std::ostringstream str;
         m_triggerInConfig[iTrig]->set("channelId",(*channels)[loop]->id(), loop);
      }
      try {
         m_triggerIn[iTrig]->configure(command);
      }
      catch (ConfigException& error) {
         ENCAPSULATE_ROS_EXCEPTION(e1, CoreException, BAD_CONFIGURATION, error,
                                   "Connecting TriggerIn");
         e1.set_severity(ers::Fatal);
         throw e1;
      }
   }


   // configure any interrupt handlers etc. AFTER the Modules
   for (ROSHandler::iterator iter=ROSHandler::begin(); iter!=ROSHandler::end(); iter++) {
      (*iter)->configure(command);
   }

   m_configured=true;
}

/** Make any network connections necessary
*/
/**************************************************/
void IOManager::connect(const daq::rc::TransitionCmd& command)
/**************************************************/
{
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::connect");
   // Notify the ReadoutModules
   for (unsigned int index=0; index<m_readoutModules.size(); index++) {
      m_readoutModules[index]->connect(command);
   }

   // and DataOuts
   for (std::vector<DataOut*>::iterator iter=m_dataOut.begin() ; iter!=m_dataOut.end(); iter++) {
      try {
         (*iter)->connect(command);
      }
      catch (ConfigException& error) {
         ENCAPSULATE_ROS_EXCEPTION(e1, CoreException, BAD_CONFIGURATION, error,
                                   "Connecting DataOut");
         e1.set_severity(ers::Fatal);
         throw e1;
      }
   }

   for (std::vector<TriggerIn*>::iterator iter=m_triggerIn.begin() ; iter!=m_triggerIn.end(); iter++) {
      try {
         (*iter)->connect(command);
      }
      catch (ConfigException& error) {
         ENCAPSULATE_ROS_EXCEPTION(e1, CoreException, BAD_CONFIGURATION, error,
                                   "Connecting TriggerIn");
         e1.set_severity(ers::Fatal);
         throw e1;
      }
   }

   // after Modules are configured think about the input threads ..
   for (ROSHandler::iterator iter=ROSHandler::begin(); iter!=ROSHandler::end(); iter++) {
      (*iter)->connect(command);
   }
}

/** Start all the other threads.
 */
/**********************************************/
void IOManager::prepareForRun(const daq::rc::TransitionCmd& command)
/**********************************************/
{
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::prepareForRun");

   m_requestQueue->clear();     // clean the requests Q

   m_pool->reset();

   m_requestsQueued=0;
   m_requestsDequeued=0;
   m_requestsRequeued=0;
   m_numberOfRequestsLast=0;

   m_lastHandled=0;
   m_lastPopEmpty=0;
   m_lastPushEmpty=0;

   SSIConfiguration* runConfig = m_config->updateRun();
   std::vector<DFCountedPointer<Config> > runParams = 
      runConfig->getConfig("RunParameters");
   delete runConfig;

   m_runNumber=runParams[0]->getInt("runNumber");
   DEBUG_TEXT(DFDB_ROSCORE, 10 ,"IOManager::prepareForRun: m_runNumber = " << m_runNumber << " read from config object"); 

   //Set all Global parameters needed by Data Requests
   DataRequest::configure(m_pool,m_requestMutex,
			  m_runNumber,m_rosId,m_requestTimeOut);
   //Similarly for new FragmentRequest interface
   FragmentRequest::configure(m_requestMutex, m_requestTimeOut);
   FragmentBuilder::configure(m_pool, m_runNumber, m_rosId, 0);

   // To be changed!!!
   Request::setDelay(m_releaseDelay);

   m_runActive=true;

   //Then start all plugins
   for (vector<DataOut*>::iterator iter=m_dataOut.begin() ; iter!=m_dataOut.end(); iter++) {
      (*iter)->setRunConfiguration(runParams[0]);
      (*iter)->prepareForRun(command);
   }

   //Start input handler threads 
   for (ROSHandler::iterator iter=ROSHandler::begin(); iter!=ROSHandler::end(); iter++) {
      (*iter)->prepareForRun(command);
   }


   // start ReadoutModules before request handlers 
   // (SWRobin: includes starting the SWRobin thread)
   for (std::vector<ReadoutModule*>::iterator readoutModule =  m_readoutModules.begin();
	readoutModule != m_readoutModules.end(); readoutModule++) {
      // Reset counters
     (*readoutModule)->clearInfo();
     (*readoutModule)->setRunConfiguration(runParams[0]);
     (*readoutModule)->prepareForRun(command);
   }
   
   //Reset also statistics of individual channels
   for (std::vector<DataChannel *>::iterator dci=DataChannel::channels()->begin();
	dci != DataChannel::channels()->end(); dci++) {
     (*dci)->clearInfo();
   }

   // Start the Request Handler threads
   for (std::vector<RequestHandler*>::iterator requestHandler =m_requestHandlers.begin();
        requestHandler!=m_requestHandlers.end(); requestHandler++) {
      // Reset counters
      (*requestHandler)->clearStatistics();
      (*requestHandler)->startExecution();
   }

   // Clear stats in interrupt catchers here!
   for (ROSHandler::iterator iter=ROSHandler::begin(); iter!=ROSHandler::end(); iter++) {
      (*iter)->clearInfo();
   }

   for (std::vector<TriggerIn*>::iterator iter=m_triggerIn.begin() ; iter!=m_triggerIn.end(); iter++) {
      (*iter)->prepareForRun(command);
   }

   if (m_publishingController!=0) {
      m_publishingController->start_publishing();
   }

   ts_clock(&m_tsStart);
   m_tsStopLast = m_tsStart;
}


/********************************************************************/
void IOManager::stopROIB(const daq::rc::TransitionCmd& command) {
/********************************************************************/
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::stopROIB");

   // Tell all TriggerIns
   for (std::vector<TriggerIn*>::reverse_iterator iter=m_triggerIn.rbegin() ; iter!=m_triggerIn.rend(); iter++) {
      (*iter)->stopROIB(command);
   }
   // Tell ReadoutModules
   for (std::vector<ReadoutModule*>::reverse_iterator rm =  m_readoutModules.rbegin();
	rm != m_readoutModules.rend(); rm++) {
      (*rm)->stopROIB(command);
   }
   // Tell DataOuts
   for (std::vector<DataOut*>::iterator iter=m_dataOut.begin() ; iter!=m_dataOut.end(); iter++) {
      (*iter)->stopROIB(command);
   }
}


/********************************************************************/
void IOManager::stopDC(const daq::rc::TransitionCmd& command) {
/********************************************************************/
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::stopROIB");

   // Tell all TriggerIns
   for (std::vector<TriggerIn*>::reverse_iterator iter=m_triggerIn.rbegin() ; iter!=m_triggerIn.rend(); iter++) {
      (*iter)->stopDC(command);
   }
   // Tell ReadoutModules
   for (std::vector<ReadoutModule*>::reverse_iterator rm =  m_readoutModules.rbegin();
	rm != m_readoutModules.rend(); rm++) {
      (*rm)->stopDC(command);
   }
   // Tell DataOuts
   for (std::vector<DataOut*>::iterator iter=m_dataOut.begin() ; iter!=m_dataOut.end(); iter++) {
      (*iter)->stopDC(command);
   }
}


/********************************************************************/
void IOManager::stopHLT(const daq::rc::TransitionCmd& command) {
/********************************************************************/
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::stopHLT");

   // Tell all TriggerIns
   for (std::vector<TriggerIn*>::reverse_iterator iter=m_triggerIn.rbegin() ; iter!=m_triggerIn.rend(); iter++) {
      (*iter)->stopHLT(command);
   }
   // Tell ReadoutModules
   for (std::vector<ReadoutModule*>::reverse_iterator rm =  m_readoutModules.rbegin();
	rm != m_readoutModules.rend(); rm++) {
      (*rm)->stopHLT(command);
   }
   // Tell DataOuts
   for (std::vector<DataOut*>::iterator iter=m_dataOut.begin() ; iter!=m_dataOut.end(); iter++) {
      (*iter)->stopHLT(command);
   }
}


/********************************************************************/
void IOManager::stopRecording(const daq::rc::TransitionCmd& command) {
/********************************************************************/
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::stopRecording");

   // Tell all TriggerIns
   for (std::vector<TriggerIn*>::reverse_iterator iter=m_triggerIn.rbegin() ; iter!=m_triggerIn.rend(); iter++) {
      (*iter)->stopRecording(command);
   }
   // Tell ReadoutModules
   for (std::vector<ReadoutModule*>::reverse_iterator rm =  m_readoutModules.rbegin();
	rm != m_readoutModules.rend(); rm++) {
      (*rm)->stopRecording(command);
   }
   // Tell DataOuts
   for (std::vector<DataOut*>::iterator iter=m_dataOut.begin() ; iter!=m_dataOut.end(); iter++) {
      (*iter)->stopRecording(command);
   }

}

/********************************************************************/
void IOManager::stopGathering(const daq::rc::TransitionCmd& command) {
/********************************************************************/
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::stopGathering");

   // Tell all TriggerIn threads to stop
   for (std::vector<TriggerIn*>::reverse_iterator iter=m_triggerIn.rbegin() ; iter!=m_triggerIn.rend(); iter++) {
      (*iter)->stopGathering(command);
   }
   // Then wait 'til they have
   for (std::vector<TriggerIn*>::iterator iter=m_triggerIn.begin() ; iter!=m_triggerIn.end(); iter++) {
      if ((*iter)->getCondition() != DFThread::UNSTARTED) {
         try {
            (*iter)->waitForCondition(DFThread::TERMINATED, m_communicationTimeout);
            std::cout << "IOManager: TriggerIn stopped\n";
         }
         catch(DFThread::Timeout&) {
            CREATE_ROS_EXCEPTION(e, CoreException, THREAD_WAIT_TIMEOUT, "waiting for TriggerIn to stop. "
                                 << ": TriggerIn is still in condition: "
                                 << (*iter)->getCondition());
            ers::warning(e);
         } 
      }
   }


   //Scheduling request handlers for termination
   for (std::vector<RequestHandler*>::reverse_iterator 
	  requestHandler=m_requestHandlers.rbegin();
        requestHandler!=m_requestHandlers.rend();
        requestHandler++) {
     (*requestHandler)->stop();
   }
   m_runActive=false;
   m_requestQueue->abort();
   for (std::vector<RequestHandler*>::reverse_iterator 
	  requestHandler=m_requestHandlers.rbegin();
        requestHandler!=m_requestHandlers.rend();
        requestHandler++) {
     (*requestHandler)->join();
   }

 
   // Stop ReadoutModules
   for (std::vector<ReadoutModule*>::reverse_iterator rm =  m_readoutModules.rbegin();
	rm != m_readoutModules.rend(); rm++) {
      (*rm)->stopGathering(command);
   }

   // stop Sequential Handler & Interrupt Catcher after Modules
   for (ROSHandler::iterator iter=ROSHandler::begin(); iter!=ROSHandler::end(); iter++) {
      (*iter)->stopGathering(command);
   }

   this->publish();
   this->publishFullStats();

   for (std::vector<DataOut*>::iterator iter=m_dataOut.begin() ; iter!=m_dataOut.end(); iter++) {
      (*iter)->stopGathering(command);
   }

  //std::cout << "Saving /tmp/ROS_debug" << std::endl;
  //ts_save(TS_H5, "/tmp/ROS_debug");
  //ts_close(TS_H5);

   if (m_publishingController!=0) {
      m_publishingController->stop_publishing();
   }
//    else {
//       std::ostringstream sStream;
//       sStream << "/tmp/" << m_name << ".root";
// //      std::cout << "Opening root file: " << sStream.str() << std::endl;
//       TFile file(sStream.str().c_str(), "RECREATE");
//       std::shared_ptr<monsvc::FilePublisher>publisher=
//          std::make_shared<monsvc::FilePublisher>(monsvc::FilePublisher(&file));
//       monsvc::NameFilter filter;
//       monsvc::PublishingController::publish_now(publisher,filter);
//    }
   return;
}

void IOManager::stopArchiving(const daq::rc::TransitionCmd& command) {
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::stopArchiving");

   // Tell all TriggerIns
   for (std::vector<TriggerIn*>::reverse_iterator iter=m_triggerIn.rbegin() ; iter!=m_triggerIn.rend(); iter++) {
      (*iter)->stopArchiving(command);
   }
   // Tell ReadoutModules
   for (std::vector<ReadoutModule*>::reverse_iterator rm =  m_readoutModules.rbegin();
	rm != m_readoutModules.rend(); rm++) {
      (*rm)->stopArchiving(command);
   }
   // Tell DataOuts
   for (std::vector<DataOut*>::iterator iter=m_dataOut.begin() ; iter!=m_dataOut.end(); iter++) {
      (*iter)->stopArchiving(command);
   }
}

void IOManager::subTransition(const daq::rc::SubTransitionCmd& command){
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::subTransition " << command.mainTransitionCmd() << " -- " << command.subTransition());

   if (command.mainTransitionCmd()=="CONFIGURE") {
      preconfigure();
   }
   else if (command.mainTransitionCmd()=="START") {
      SSIConfiguration* runConfig = m_config->updateRun();
      std::vector<DFCountedPointer<Config> > runParams = 
         runConfig->getConfig("RunParameters");
      delete runConfig;
      for (auto iter : m_triggerIn) {
         iter->setRunConfiguration(runParams[0]);
      }
      for (auto iter : m_readoutModules) {
         iter->setRunConfiguration(runParams[0]);
      }
   }

   // Tell all TriggerIns
   for (std::vector<TriggerIn*>::reverse_iterator iter=m_triggerIn.rbegin() ; iter!=m_triggerIn.rend(); iter++) {
      (*iter)->subTransition(command);
   }
   // Tell ReadoutModules
   for (std::vector<ReadoutModule*>::reverse_iterator rm =  m_readoutModules.rbegin();
	rm != m_readoutModules.rend(); rm++) {
      (*rm)->subTransition(command);
   }
   // Tell DataOuts
   for (std::vector<DataOut*>::iterator iter=m_dataOut.begin() ; iter!=m_dataOut.end(); iter++) {
      (*iter)->subTransition(command);
   }
}

/****************************************************/
void IOManager::disconnect(const daq::rc::TransitionCmd& command)
/****************************************************/
{
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::disconnect");

   DEBUG_TEXT(DFDB_ROSCORE, 15, "IOManager::disconnect disconnecting dataOut");
   for (std::vector<DataOut*>::iterator iter=m_dataOut.begin() ; iter!=m_dataOut.end(); iter++) {
      (*iter)->disconnect(command);
   }

   DEBUG_TEXT(DFDB_ROSCORE, 15, "IOManager::disconnect disconnecting triggerIn");
   for (std::vector<TriggerIn*>::reverse_iterator iter=m_triggerIn.rbegin() ; iter!=m_triggerIn.rend(); iter++) {
      (*iter)->disconnect(command);
   }

   for (std::vector<ReadoutModule*>::reverse_iterator rm = m_readoutModules.rbegin();
        rm != m_readoutModules.rend(); rm++) {
      DEBUG_TEXT(DFDB_ROSCORE, 15, "IOManager::disconnect disconnecting ReadoutModules");
      (*rm)->disconnect(command);
   }

   DEBUG_TEXT(DFDB_ROSCORE, 15, "IOManager::disconnect disconnecting generic Handlers");
   for (ROSHandler::iterator iter=ROSHandler::begin(); iter!=ROSHandler::end(); iter++) {
      (*iter)->disconnect(command);
   }

}


/********************************************************************/
void IOManager::onExit(const daq::rc::FSM_STATE state) noexcept {
/********************************************************************/
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::onExit");

   // Tell all TriggerIns
   for (std::vector<TriggerIn*>::reverse_iterator iter=m_triggerIn.rbegin() ; iter!=m_triggerIn.rend(); iter++) {
      (*iter)->onExit(state);
   }
   // Tell ReadoutModules
   for (std::vector<ReadoutModule*>::reverse_iterator rm =  m_readoutModules.rbegin();
	rm != m_readoutModules.rend(); rm++) {
      (*rm)->onExit(state);
   }
   // Tell DataOuts
   for (std::vector<DataOut*>::iterator iter=m_dataOut.begin() ; iter!=m_dataOut.end(); iter++) {
      (*iter)->onExit(state);
   }
}



/** Destroy subordinate parts
 */
/***************************************************/
void IOManager::unconfigure(const daq::rc::TransitionCmd& command)
/***************************************************/
{
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::unconfigure");

   if (m_publishingController!=0) {
      delete m_publishingController;
      m_publishingController=0;
   }

   while (!m_requestHandlers.empty()) {
      RequestHandler* requestHandler = m_requestHandlers.back();
      m_requestHandlers.pop_back();
      delete requestHandler;
   }

  // unconfigure any Interrupt Catchers etc. before the modules
   for (ROSHandler::iterator iter=ROSHandler::begin(); iter!=ROSHandler::end(); iter++) {
      (*iter)->unconfigure(command);
   }

   while (!m_readoutModules.empty()) {
      ReadoutModule* rm = m_readoutModules.back();
      rm->unconfigure(command);
      m_readoutModules.pop_back();
      delete rm;
   }

   while (!m_scheduledMonitoringActions.empty()) {
      ScheduledMonitoringAction* sma=m_scheduledMonitoringActions.back();
      m_scheduledMonitoringActions.pop_back();
      delete sma;
   }


   while (!m_dataOut.empty()) {
      DataOut* dataOut=m_dataOut.back();
      dataOut->unconfigure(command);
      m_dataOut.pop_back();
      delete dataOut;
   }

   while (!m_triggerIn.empty()) {
      TriggerIn* tin=m_triggerIn.back();
      tin->unconfigure(command);
      m_triggerIn.pop_back();
      delete tin;
   }

   if(m_requestQueue) {
      delete m_requestQueue;
      m_requestQueue=0;
   }

   // Free memory pool
   if (m_pool!=0) {
     delete m_pool;	
     m_pool=0;
   }

   if (m_userCommandPool!=0) {
      delete m_userCommandPool;
      m_userCommandPool=0;
   }

   m_trace = false;

   if (!m_interactiveMode) {
      ISInfoDictionary dictionary(m_ipcPartition);
      // Remove my named info
      try {
         dictionary.remove(m_isInfoName);
      }
      catch (daq::is::Exception& exc) {
         // Ignore error at this stage.  It may be that we have never published
         // anything since configure or somebody removed the item behind our back.
      }

      // And the "expert" info items
      for (std::set<std::string>::iterator iter=m_isItems.begin(); iter!=m_isItems.end(); iter++) {
         try {
            dictionary.remove((*iter).c_str());
         }
         catch (daq::is::Exception& exc) {
            // Ignore error at this stage.  It may be that somebody removed the item behind our back
         }
      }
      m_isItems.clear();
   }

   ts_close(TS_DUMMY);

   m_configured=false;
   m_preConfigured=false;
}

/** Specific user commands 
 */
/*********************************************/
void IOManager::user(const daq::rc::UserCmd& command) {
/*********************************************/
   std::string commandName=command.commandName();
   std::vector<std::string> arguments=command.commandParameters();

   // Is it one that we deal with ourselves?
   if (command.commandName() == "lldebug") {
      if (arguments.size()==2) {
         std::istringstream pstr(arguments[0]);
         std::istringstream lstr(arguments[1]);
         int package;
         int level;
         pstr >> package;
         lstr >> level;
         std::cout << "Changing debug settings to package " << package
              << ",  level " << level << std::endl ;
         DF::GlobalDebugSettings::setup(level, package);
         m_trace = true;
      }
      else {
         std::cerr << "Bad/missing arguments to lldebug user command\n";
      }
   }
   else {
      // loop over readout modules 
      for (std::vector<ReadoutModule*>::iterator readoutModule =  m_readoutModules.begin();
           readoutModule != m_readoutModules.end(); readoutModule++) {
         (*readoutModule)->user(command);
      }

      monitoringAction(commandName);

      // loop over triggers
      for (std::vector<TriggerIn*>::iterator iter=m_triggerIn.begin() ; iter!=m_triggerIn.end(); iter++) {
         (*iter)->user(command);
      }

      // loop over outputs
      for (std::vector<DataOut*>::iterator iter=m_dataOut.begin() ; iter!=m_dataOut.end(); iter++) {
         (*iter)->user(command);
      }

   }
}


void IOManager::disable(const std::vector<std::string>& argVec) {
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::disable");

   DEBUG_TEXT(DFDB_ROSCORE, 15, "IOManager::disable disabling dataOut");
   for (std::vector<DataOut*>::iterator iter=m_dataOut.begin() ; iter!=m_dataOut.end(); iter++) {
      (*iter)->disable(argVec);
   }

   DEBUG_TEXT(DFDB_ROSCORE, 15, "IOManager::disable disabling triggerIn");
   for (std::vector<TriggerIn*>::reverse_iterator iter=m_triggerIn.rbegin() ; iter!=m_triggerIn.rend(); iter++) {
      (*iter)->disable(argVec);
   }

   for (std::vector<ReadoutModule*>::reverse_iterator rm = m_readoutModules.rbegin();
        rm != m_readoutModules.rend(); rm++) {
      DEBUG_TEXT(DFDB_ROSCORE, 15, "IOManager::disable disabling ReadoutModules");
      (*rm)->disable(argVec);
   }

   DEBUG_TEXT(DFDB_ROSCORE, 15, "IOManager::disable disabling generic Handlers");
   for (ROSHandler::iterator iter=ROSHandler::begin(); iter!=ROSHandler::end(); iter++) {
      (*iter)->disable(argVec);
   }

}

void IOManager::enable(const std::vector<std::string>& argVec) {
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::enable");

   DEBUG_TEXT(DFDB_ROSCORE, 15, "IOManager::enable enabling dataOut");
   for (std::vector<DataOut*>::iterator iter=m_dataOut.begin() ; iter!=m_dataOut.end(); iter++) {
      (*iter)->enable(argVec);
   }

   DEBUG_TEXT(DFDB_ROSCORE, 15, "IOManager::enable enabling triggerIn");
   for (std::vector<TriggerIn*>::iterator iter=m_triggerIn.begin() ; iter!=m_triggerIn.end(); iter++) {
      (*iter)->enable(argVec);
   }

   for (std::vector<ReadoutModule*>::iterator rm = m_readoutModules.begin();
        rm != m_readoutModules.end(); rm++) {
      DEBUG_TEXT(DFDB_ROSCORE, 15, "IOManager::enable enabling ReadoutModules");
      (*rm)->enable(argVec);
   }

   DEBUG_TEXT(DFDB_ROSCORE, 15, "IOManager::enable enabling generic Handlers");
   for (ROSHandler::iterator iter=ROSHandler::begin(); iter!=ROSHandler::end(); iter++) {
      (*iter)->enable(argVec);
   }

}


/********************************************************************/
void IOManager::resynch(const daq::rc::ResynchCmd& command) {
/********************************************************************/
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::resynch");

   // Tell all TriggerIns
   for (std::vector<TriggerIn*>::reverse_iterator iter=m_triggerIn.rbegin() ; iter!=m_triggerIn.rend(); iter++) {
      (*iter)->resynch(command);
   }
   // Tell ReadoutModules
   for (std::vector<ReadoutModule*>::reverse_iterator rm =  m_readoutModules.rbegin();
	rm != m_readoutModules.rend(); rm++) {
      (*rm)->resynch(command);
   }
   // Tell DataOuts
   for (std::vector<DataOut*>::iterator iter=m_dataOut.begin() ; iter!=m_dataOut.end(); iter++) {
      (*iter)->resynch(command);
   }
}

/**********************************************************************/
void IOManager::publishSpecificInfo(std::string setName,
                                    ISInfo* info, int index) {
   if (m_interactiveMode) {
      std::cout << "\n" << setName;
      if (index != -1) {
         std::cout << index;
      }
      std::cout << " info:\n" << *info << std::endl;
   }
   else {
      std::ostringstream itemNameStream;
      itemNameStream << m_isInfoName << "." << setName ;
      if (index != -1) {
         itemNameStream << index;
      }

      ISInfoDictionary dictionary(m_ipcPartition);

      // Keep track of what we've put in so we can clean up later
      if(m_isItems.find(itemNameStream.str()) == m_isItems.end()) {
         m_isItems.insert(itemNameStream.str());
         try {
            dictionary.insert(itemNameStream.str(), *info);
         }
         catch (daq::is::InfoAlreadyExist& exc) {
            // Ignore error at this stage.  It may be that the IS server
            // has stuff left over from a previous run which was shut down 
            // without going through unconfigure.  Try to update this item
            // instead.
            try {
               dictionary.update(itemNameStream.str(), *info);
            }
            catch (daq::is::Exception& exc) {
               // Issue a warning and carry on
               ers::warning(exc);
            }
         }
         catch (daq::is::Exception& exc) {
            // Issue a warning and carry on
            ers::warning(exc);
         }
      }
      else {
         try {
            dictionary.update(itemNameStream.str(), *info);
         }
         catch (daq::is::InfoNotFound& exc1) {
            try {
               // Looks like somebody deleted our item from another program.
               // Better just create it again
               dictionary.insert(itemNameStream.str(), *info);
            }
            catch (daq::is::Exception& exc2) {
               // Issue a warning and carry on
               ers::warning(exc2);
            }
         }
         catch (daq::is::Exception& exc3) {
            // Issue a warning and carry on
            ers::warning(exc3);
         }
      }
   }
}

/**********************************************************************/
void IOManager::coreMonitor() {
   m_busyIndex++;
   m_busyIndex=m_busyIndex%m_maxBusyCounts;
   //std::cout << "m_busyIndex=" << m_busyIndex;
   m_busyCount[m_busyIndex]=0;
   for (std::vector<RequestHandler*>::iterator requestHandler=m_requestHandlers.begin();
        requestHandler!=m_requestHandlers.end();
        requestHandler++) {
      if ((*requestHandler)->busy()) {
         m_busyCount[m_busyIndex]++;
      }
   }
   //   std::cout << ",  count=" << m_busyCount[m_busyIndex] << std::endl;
   m_queueCount[m_busyIndex]=m_requestQueue->size();

   auto timeNow=std::chrono::system_clock::now();
   auto interval=timeNow-m_busyTimeLast;
   m_busyInterval[m_busyIndex]=(float)std::chrono::duration_cast<std::chrono::microseconds>(interval).count()/1e6;
   m_busyTimeLast=timeNow;
}


/** Get operational statistics etc.
*/
/************************************************/
void IOManager::publish() {
/************************************************/
   if (!m_configured) {
      return;
   }

   auto now=std::chrono::system_clock::now();
   auto elapsed=now-m_lastPublish;
   m_lastPublish=now;

   int deltaTimeSec=std::chrono::duration_cast<std::chrono::seconds> (elapsed).count();
   if (deltaTimeSec==0) {
      return;
   }
   int deltaTimeMs=std::chrono::duration_cast<std::chrono::milliseconds> (elapsed).count();

   int numberOfLevel1=0;

   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::publish");



   int numTrig=0;
   for (std::vector<TriggerIn*>::iterator iter=m_triggerIn.begin() ; iter!=m_triggerIn.end(); iter++) {
      ISInfo* isInfo=(*iter)->getISInfo();
      if (isInfo!=0) {
         publishSpecificInfo("TriggerIn", isInfo,numTrig);
      }
      numberOfLevel1=(*iter)->triggerCount();

      numTrig++;
   }


   int numOuts=0;
   for (std::vector<DataOut*>::iterator iter=m_dataOut.begin() ; iter!=m_dataOut.end(); iter++) {
      ISInfo* isInfo=(*iter)->getISInfo();
      if (isInfo!=0) {
         publishSpecificInfo("DataOut", isInfo, numOuts);
      }
      numOuts++;
   }

   // Loop over all subordinates gathering their info
   int numMod=0;
   for (std::vector<ReadoutModule*>::iterator rm=m_readoutModules.begin();
        rm!=m_readoutModules.end(); rm++) {

      // Directly call the ReadoutModule's probe method to allow users to implement their logic
      (*rm)->publish();

      ISInfo* isInfo=(*rm)->getISInfo();
      if (isInfo!=0) {
         publishSpecificInfo("ReadoutModule", isInfo, numMod);
      }
      numMod++;
   }

   int numChan=0;
   for (std::vector<DataChannel*>::iterator ich=DataChannel::channels()->begin();
       ich!=DataChannel::channels()->end(); ich++) {

      // Directly call the DataChannel's probe method to allow users to implement their logic
      (*ich)->probe();

      ISInfo* isInfo=(*ich)->getISInfo();
      if (isInfo!=0) {
         publishSpecificInfo("DataChannel", isInfo, numChan);
      }
      numChan++;
   }

   int totalRequestsHandled = 0;
   int totalRequestsRequeued = 0;
   int totalRequestsTimedOut = 0;

   CoreInfo coreInfo;

   int nHandlers=m_requestHandlers.size();
   if (m_maxBusyCounts!=0 && nHandlers!=0) {
      float busyPercent=0;
      float queueAverage=0;
      float totBusy=0;
      float totQueue=0;
      int firstBin=m_busyIndex;
      //      std::cout << "maxbusyCounts=" << m_maxBusyCounts << ", busyIndex=" << firstBin<<std::endl;
      int index=firstBin;


      int bins=0;
      auto timeNow=std::chrono::system_clock::now();
      auto interval=timeNow-m_busyTimeLast;
      float elapsed=(float)std::chrono::duration_cast<std::chrono::microseconds>(interval).count()/1e6;
      for (int seconds=1; seconds<=10; seconds++) {
         //         std::cout << "Second " << seconds << ", busy counts: ";
         while (elapsed<seconds && bins<m_maxBusyCounts) {
            //            std::cout << " [" << index << "]=" << m_busyCount[index];
            totBusy+=m_busyCount[index];
            totQueue+=m_queueCount[index];
            elapsed+=m_busyInterval[index];
            index--;
            if (index<0) {
               index+=m_maxBusyCounts;
            }
            bins++;
         }
         //         std::cout << std::endl;
         if (bins>0) {
            busyPercent=100*totBusy/(nHandlers*bins);
            queueAverage=totQueue/bins;
            //std::cout << elapsed << " second average busy " << busyPercent << "%, queue depth " << queueAverage << std::endl;
         }

         coreInfo.loadAverage.push_back(busyPercent);
         coreInfo.queueAverage.push_back(queueAverage);
      }
   }

   for (std::vector<RequestHandler*>::iterator requestHandler=m_requestHandlers.begin();
        requestHandler!=m_requestHandlers.end();
        requestHandler++) {
      auto rhStats=(*requestHandler)->getStatistics();
      coreInfo.requestsHandled.push_back(rhStats->m_requestsHandled);
      totalRequestsHandled+=rhStats->m_requestsHandled;

      coreInfo.requestsRetried.push_back(rhStats->m_requestsFailed);
      totalRequestsRequeued+=rhStats->m_requestsFailed;

      coreInfo.requestsTimedOut.push_back(rhStats->m_requestsTimedOut);
      totalRequestsTimedOut+=rhStats->m_requestsTimedOut;
   }

   int deltanumberOfRequests = m_requestsQueued - m_numberOfRequestsLast;
   m_numberOfRequestsLast = m_requestsQueued;
   int requestRateHz = (int)(deltanumberOfRequests/deltaTimeSec);


//   int deltaRequestsHandled = totalRequestsHandled - m_lastHandled;
   m_lastHandled = totalRequestsHandled;

   if (nHandlers!=0) {
      if (! m_interactiveMode) {
         DF_IS_Info::rosNamed rosInfo(m_ipcPartition, m_isInfoName);
         rosInfo.requestRateHz = requestRateHz;
         rosInfo.requestsQueued = m_requestsQueued;

         rosInfo.requestsDequeued = totalRequestsHandled;

         rosInfo.deltaTimeMs = deltaTimeMs;
         rosInfo.numberOfLevel1 = numberOfLevel1;
         rosInfo.numberOfQueueElements = m_requestQueue->size();

         try {
            rosInfo.checkin();
         }
         catch (daq::is::Exception& exc) {
            // Issue a warning and carry on
            ers::warning(exc);
         }
      }
      else {
         std::cout << "\n=== core rosInfo " << std::endl;
         std::cout << "Request rate Hz\t\t" << requestRateHz << std::endl;
         std::cout << "Requests queued\t\t" << m_requestsQueued << std::endl;

         std::cout << "Requests dequeued\t" << totalRequestsHandled << std::endl;

         std::cout << "Delta time ms\t\t" << deltaTimeMs << std::endl;
         std::cout << "Number of Level1\t" << numberOfLevel1 << std::endl;
         std::cout << "Number of queue elements\t" << m_requestQueue->size() << std::endl;
      }

      publishSpecificInfo("Core", &coreInfo);
   }

   for (ROSHandler::iterator iter=ROSHandler::begin(); iter!=ROSHandler::end(); iter++) {
      ISInfo* handlerInfo=(*iter)->getISInfo();
      if (handlerInfo!=0) {
         publishSpecificInfo((*iter)->name(), handlerInfo);
      }
   }
}

/*************************************************************/
void IOManager::publishFullStats() {
/*************************************************************/
   DEBUG_TEXT(DFDB_ROSCORE, 10, "IOManager::publishFullStats");

  //This method allows users modules to publish histograms
  //at lower rate than the mormal statistics (which are published at publish()
  for (std::vector<ReadoutModule*>::iterator rm=m_readoutModules.begin();
      rm!=m_readoutModules.end(); rm++) {
    (*rm)->publishFullStats();
  }  
}
