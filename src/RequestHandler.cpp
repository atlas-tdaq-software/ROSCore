// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone
//
//  $Log$
//  Revision 1.36  2008/03/26 16:47:50  gorini
//  Removed misleading FIXME comment
//
//  Revision 1.35  2008/03/26 16:21:58  gcrone
//  Use yieldOrCancel instead of plain yield when requeuing.
//
//  Revision 1.34  2008/03/26 13:33:35  gorini
//  Completed implementation of the requeuing mechanism
//
//  Revision 1.33  2008/02/05 08:29:43  gcrone
//  Add Handker Id to debug messages
//
//  Revision 1.32  2007/04/25 17:42:59  gcrone
//  Check that m_request is 0 when the thread starts
//
//  Revision 1.31  2007/04/19 14:14:05  gcrone
//  Restructure run method with try/catch. New private deleteRequest() method.
//
//  Revision 1.30  2007/04/05 13:11:05  gcrone
//  Remove redundant/obsolete includes
//
//  Revision 1.29  2007/01/12 14:01:01  gcrone
//  Revert to previous version without thread cancellation blocking and flush() method
//
//  Revision 1.27  2006/08/18 10:38:23  gcrone
//  Get rid of compiler warning by creating char[] variable for mutex
//  name.
//
//  Revision 1.26  2005/07/26 21:15:26  gorini
//  Added IOMPlugin interface that replace Controllable as base class for the Plugins
//
//  Revision 1.25  2004/08/18 17:21:59  gorini
//  Patch to fix the dead-lock that can occurr when trying to requeue requests. BEWARE this fix is only suitable for configurations without L2 (like testbeam or testbeds)
//
//  Revision 1.24  2004/04/16 13:21:50  gcrone
//  Removed redundant include of DFErrorCatcher.h
//
//  Revision 1.23  2004/04/07 15:40:20  jorgen
//   remove catching of exceptions in RequestHandler
//
//  Revision 1.22  2004/03/24 14:07:26  gorini
//  Merged NewArchitecture branch.
//
//  Revision 1.21.2.1  2004/03/19 10:11:55  gorini
//  Made DataRequest more generic so that also RCDDataRequests can inherit from it.
//  buildROSFragment method has become purely virtual and is now called buildFragment.
//  Added ROSDataRequest that provides a specific implementation of buildFragment for the ROS requests.
//
//  Revision 1.21  2004/03/04 16:51:41  joos
//  DFDB_QUEUE added
//
//  Revision 1.20  2004/02/04 12:24:28  gcrone
//  Added clearInfo() method
//
//  Revision 1.19  2003/12/03 15:08:50  gcrone
//  Config constructors now protected, use static method New to enforce use of DFCountedPointer
//
//  Revision 1.18  2003/11/21 11:36:14  akazarov
//  back to v1r5p0
//
//  Revision 1.16  2003/07/11 13:18:02  gorini
//  Added std:: for gcc32 compilation
//
//  Revision 1.15  2003/07/10 17:19:31  gorini
//  Implemented automatic exception notification to te ErrorCatcher, through a specific BaseException::NotificationHandler.
//
//  Revision 1.14  2003/05/22 14:58:03  gorini
//  1) The RequestHandler threads will be terminated in case of FATAL error (i.e. uncaught exceptions) 2) Some errors that were FATAL before have been changed to RECOVERABLE and the RequestHandler will not stop for those.
//
//  Revision 1.13  2003/04/02 09:57:59  joos
//  Time-out added for Data Requests
//
//  Revision 1.12  2003/03/18 16:42:37  gcrone
//  Reset s_nextId to 1 when we create the first Requesthandler.
//
//  Revision 1.11  2003/02/21 14:30:15  gorini
//  Modified exception handling
//
//  Revision 1.10  2003/02/19 17:59:58  jorgen
//   added yield when requeuing; improved time stamping
//
//  Revision 1.9  2003/02/06 16:52:49  gcrone
//  Removed debug std::cout
//
//  Revision 1.8  2003/02/05 09:26:04  gcrone
//  Rearranged Request class hierarchy.  All data requests
//  L2,TestBeam... now subclassed from DataRequest which has a
//  buildFragment method.
//  Request::execute now returns bool for success/(temporary) failure. Now
//  retry data requests where Request::execute returns false;
//
//  Revision 1.7  2002/11/14 16:26:49  gcrone
//  Don't need pointer to IOManager anymore.
//
//  Revision 1.6  2002/11/08 15:31:03  gcrone
//  New error/exception handling
//
//  Revision 1.5  2002/11/03 11:46:43  glehmann
//  IOManager is a friend of DFCreator
//
//  Revision 1.4  2002/10/27 14:45:17  jorgen
//   added trace flag; more complete time stamping; catch EventFragmentException
//
//  Revision 1.3  2002/10/21 15:19:04  gcrone
//  New interface with IOManager.  We get a queue and pop requests ourselves
//
//  Revision 1.2  2002/10/10 14:31:40  atdsoft
//  Changes concerning the rcc_time_stamp package.
//
//  Revision 1.1.1.1  2002/10/07 09:50:08  pyannis
//  Initial version of DataFlow ROS packages tree...
//
//  Revision 1.2  2002/09/17 21:39:53  gcrone
//  Document code only - No functional changes
//
//  Revision 1.1.1.1  2002/08/23 13:36:54  gorini
//  First import
//
//  Revision 1.3  2002/08/16 15:17:19  jorgen
//   added time stamping (preliminary)
//
//  Revision 1.2  2002/08/01 14:15:55  gcrone
//  First 'working' version using ROS namespace and dynamically loaded libraries
//
//  Revision 1.1.1.1  2002/06/21 16:18:28  pyannis
//  Creation of the cmtros directory that will hold the new ROS software
//  managed by CMT.
//  Various packages are already installed, e.g. Core, BufferManagement, DFThreads.
//
//
// //////////////////////////////////////////////////////////////////////

#include <iostream>
#include <exception>
#include <mutex>
#include <thread>

#include <tbb/concurrent_queue.h>

#include "ROSCore/RequestHandler.h"
#include "ROSCore/Request.h"
#include "ROSCore/CoreException.h"
#include "DFDebug/DFDebug.h"
#include "rcc_time_stamp/tstamp.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ers/ers.h"


using namespace ROS;


int RequestHandler::s_nextId=1;
int RequestHandler::s_numberOfHandlers=0;

RequestHandler::RequestHandler(tbb::concurrent_bounded_queue<Request*>* requestQueue) :
   m_requestQueue(requestQueue), m_thread(0) {
   if (s_numberOfHandlers==0) {
      s_nextId=1;
   }

   // Count number of request handlers extant
   s_numberOfHandlers++;

   // Allocate an ID to this request handler
   m_statistics.m_handlerId=s_nextId++;

   // Initialise statistics
   clearStatistics();

   m_request=0;

   std::cout << "Finished creating request handler" << m_statistics.m_handlerId << "\n";
}

RequestHandler::~RequestHandler()
{
   s_numberOfHandlers--;
}

void RequestHandler::clearStatistics()
{
   m_statistics.m_requestsHandled=0;
   m_statistics.m_requestsTimedOut=0;
   m_statistics.m_requestsFailed=0;
}

void RequestHandler::deleteRequest()
{
   if (m_request != 0) {
       std::lock_guard<std::mutex> lock(m_mutex);   // Memory deallocation has to be protected as
       delete m_request;    // it is thread-unsafe!
       m_request=0;
   }
}

bool RequestHandler::busy() {
   return (m_request!=0);
}

void RequestHandler::startExecution() {
   m_active=true;
   m_thread=new std::thread(&RequestHandler::run,this);
}
void RequestHandler::stop() {
   m_active=false;
}
void RequestHandler::join() {
   m_thread->join();
   delete m_thread;
   m_thread=0;
}

/***
 The action routine of the thread.  Get the next Request from the
 request queue, execute and finally delete it.  
*/
void RequestHandler::run(void)
{
   m_active=true;
#ifdef TSTAMP
   const int stamp_offset = 10000*m_statistics.m_handlerId;
#endif

   if (m_request != 0) {
      CREATE_ROS_EXCEPTION(ex, CoreException, STALE_REQUEST, "");
      ers::warning(ex);
      m_request=0;
   }

   // Loop until the thread is stopped
   while (m_active) {
      TS_RECORD(TS_H1,stamp_offset + 2000);
      if (m_request==0) {
         DEBUG_TEXT(DFDB_QUEUE, 20, "RequestHandler(" << m_statistics.m_handlerId <<")::run: popping request Q");
         try {
            m_requestQueue->pop(m_request);
         }
         catch (tbb::user_abort& abortException) {
            std::cout << "RequestHandler run aborting...\n";
            m_active=false;
            return;
         }
      }
      else {
         DEBUG_TEXT(DFDB_QUEUE, 20, "RequestHandler(" << m_statistics.m_handlerId <<")::run: repeating oldRequest");
      }
      DEBUG_TEXT(DFDB_QUEUE, 20, "RequestHandler(" << m_statistics.m_handlerId <<")::run: request Q popped OK");


      try {
         TS_RECORD(TS_H1,stamp_offset + 2100);
         int requestOk;
         //unsigned int tsdata = m_statistics.m_handlerId * 100 + 3000000000;
         //TS_RECORD(TS_H5,tsdata);

         requestOk = m_request->execute();
         TS_RECORD(TS_H1,stamp_offset + 2200);

         if (requestOk == Request::REQUEST_OK) {
            //                  std::cout << "Executed " << *request << std::endl;
            m_statistics.m_requestsHandled++;

            deleteRequest();
            TS_RECORD(TS_H1, stamp_offset + 2300);
         }
         else if (requestOk == Request::REQUEST_TIMEOUT) {
            m_statistics.m_requestsTimedOut++;
            deleteRequest();
            TS_RECORD(TS_H1, stamp_offset + 2300);
         }                  
         else {
            TS_RECORD(TS_H1, stamp_offset + 2400);
            DEBUG_TEXT(DFDB_ROSCORE, 15, "RequestHandler(" << m_statistics.m_handlerId <<")::run: Request non completed: will be requeued.");
            m_statistics.m_requestsFailed++;
//	    m_request = m_requestQueue->swap(m_request);
            if (m_requestQueue->try_push(m_request)) {
               m_request=0;
            }

            TS_RECORD(TS_H1, stamp_offset + 2500);
         }

         TS_RECORD(TS_H1, stamp_offset + 2990);
      }
      catch (std::exception& issue) {
         ENCAPSULATE_ROS_EXCEPTION(newIssue, CoreException, UNCLASSIFIED, issue, "RequestHandler caught exception, deleting Request");
         ers::error(newIssue);
         deleteRequest();
      }
   }
}


RequestHandlerStatistics* RequestHandler::getStatistics(){
   return &m_statistics;
}
